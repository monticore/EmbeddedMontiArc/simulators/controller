/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.NavigationEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components.*;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * calculate the maximal steering angle of this section
 *
 * used sub blocks: VertexToVectorXY
 *                  SubtractVectors
 *                  NormalOfDirectionVertex2D
 *                  IntersectionPointOfLines
 *                  MinimumDistanceOfVectors
 *                  MaximumSteeringAngleForRadius
 *
 * used formula : none
 *
 * Created by Christoph Grüne on 18.01.2017.
 */
public class MaximumSteeringAngleOfSection extends FunctionBlock{

    //Input Variables
    private Double wheelbase;
    private Vertex beginning;
    private Vertex beginningInFront;
    private Vertex beginningBehind;
    private Vertex end;
    private Vertex endInFront;
    private Vertex endBehind;

    //Output Variables
    private Double steeringAngle;

    //Global Variables
    Boolean noIntersectionPoint = false;

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public MaximumSteeringAngleOfSection() {initComponents();}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        Map<String, Object> outputs = functionBlockManagement.executeAllFunctionBlocks(timeDelta);
        noIntersectionPoint = (Boolean) outputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_no_intersection_point_of_lines.toString());
        if(!noIntersectionPoint) {
            steeringAngle = (Double) outputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_steering_angle.toString());
        } else {
            steeringAngle = 0.0;
        }
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        wheelbase = (Double) inputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_wheelbase.toString());
        beginning = (Vertex) inputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning.toString());
        beginningInFront = (Vertex) inputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_in_front.toString());
        beginningBehind = (Vertex) inputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_behind.toString());
        end = (Vertex) inputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end.toString());
        endInFront = (Vertex) inputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end_in_front.toString());
        endBehind = (Vertex) inputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end_behind.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_steering_angle.toString(), steeringAngle);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[]   {
                ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_wheelbase.toString(),
                ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning.toString(),
                ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_in_front.toString(),
                ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_behind.toString(),
                ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end.toString(),
                ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end_in_front.toString(),
                ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end_behind.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/

    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {

        functionBlockManagement = new FunctionBlockManagement(this);


        FunctionBlock vectorXYZToVectorXY_1 = new VertexToVectorXY();
        functionBlockManagement.addFunctionBlock(vectorXYZToVectorXY_1);
        FunctionBlock vectorXYZToVectorXY_2 = new VertexToVectorXY();
        functionBlockManagement.addFunctionBlock(vectorXYZToVectorXY_2);
        FunctionBlock vectorXYZToVectorXY_3 = new VertexToVectorXY();
        functionBlockManagement.addFunctionBlock(vectorXYZToVectorXY_3);
        FunctionBlock vectorXYZToVectorXY_4 = new VertexToVectorXY();
        functionBlockManagement.addFunctionBlock(vectorXYZToVectorXY_4);
        FunctionBlock vectorXYZToVectorXY_5 = new VertexToVectorXY();
        functionBlockManagement.addFunctionBlock(vectorXYZToVectorXY_5);
        FunctionBlock vectorXYZToVectorXY_6 = new VertexToVectorXY();
        functionBlockManagement.addFunctionBlock(vectorXYZToVectorXY_6);


        FunctionBlock subtractVectors_1 = new SubtractVectors();
        functionBlockManagement.addFunctionBlock(subtractVectors_1);
        FunctionBlock subtractVectors_2 = new SubtractVectors();
        functionBlockManagement.addFunctionBlock(subtractVectors_2);
        FunctionBlock subtractVectors_3 = new SubtractVectors();
        functionBlockManagement.addFunctionBlock(subtractVectors_3);
        FunctionBlock subtractVectors_4 = new SubtractVectors();
        functionBlockManagement.addFunctionBlock(subtractVectors_4);


        FunctionBlock normalOfDirectionVector2D_1 = new NormalOfDirectionVector2D();
        functionBlockManagement.addFunctionBlock(normalOfDirectionVector2D_1);
        FunctionBlock normalOfDirectionVector2D_2 = new NormalOfDirectionVector2D();
        functionBlockManagement.addFunctionBlock(normalOfDirectionVector2D_2);
        FunctionBlock normalOfDirectionVector2D_3 = new NormalOfDirectionVector2D();
        functionBlockManagement.addFunctionBlock(normalOfDirectionVector2D_3);
        FunctionBlock normalOfDirectionVector2D_4 = new NormalOfDirectionVector2D();
        functionBlockManagement.addFunctionBlock(normalOfDirectionVector2D_4);


        FunctionBlock intersectionPointOfLines_1 = new IntersectionPointOfLines();
        functionBlockManagement.addFunctionBlock(intersectionPointOfLines_1);
        FunctionBlock intersectionPointOfLines_2 = new IntersectionPointOfLines();
        functionBlockManagement.addFunctionBlock(intersectionPointOfLines_2);


        FunctionBlock subtractVectors_5 = new SubtractVectors();
        functionBlockManagement.addFunctionBlock(subtractVectors_5);
        FunctionBlock subtractVectors_6 = new SubtractVectors();
        functionBlockManagement.addFunctionBlock(subtractVectors_6);


        FunctionBlock intersectionPointOfLines_3 = new IntersectionPointOfLines();
        functionBlockManagement.addFunctionBlock(intersectionPointOfLines_3);


        FunctionBlock minimumDistanceOfVectors = new MinimumDistanceOfVectors();
        functionBlockManagement.addFunctionBlock(minimumDistanceOfVectors);


        FunctionBlock maximumSteeringAngleForRadius = new MaximumSteeringAngleForRadius();
        functionBlockManagement.addFunctionBlock(maximumSteeringAngleForRadius);



        //connections to vectorXYZToVectorXY_1
        functionBlockManagement.connect(this, this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning.toString(), vectorXYZToVectorXY_1, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vertex.toString());
        //connections to vectorXYZToVectorXY_2
        functionBlockManagement.connect(this, this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_in_front.toString(), vectorXYZToVectorXY_2, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vertex.toString());
        //connections to vectorXYZToVectorXY_3
        functionBlockManagement.connect(this, this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_behind.toString(), vectorXYZToVectorXY_3, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vertex.toString());
        //connections to vectorXYZToVectorXY_4
        functionBlockManagement.connect(this, this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end.toString(), vectorXYZToVectorXY_4, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vertex.toString());
        //connections to vectorXYZToVectorXY_5
        functionBlockManagement.connect(this, this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end_in_front.toString(), vectorXYZToVectorXY_5, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vertex.toString());
        //connections to vectorXYZToVectorXY_6
        functionBlockManagement.connect(this, this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end_behind.toString(), vectorXYZToVectorXY_6, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vertex.toString());


        //connections to subtractVectors_1
        functionBlockManagement.connect(this, vectorXYZToVectorXY_1, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), subtractVectors_1, ConnectionEntry.SUBTRACT_VECTORS_minuend_vector.toString());
        functionBlockManagement.connect(this, vectorXYZToVectorXY_2, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), subtractVectors_1, ConnectionEntry.SUBTRACT_VECTORS_subtrahend_vector.toString());
        //connections to subtractVectors_2
        functionBlockManagement.connect(this, vectorXYZToVectorXY_1, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), subtractVectors_2, ConnectionEntry.SUBTRACT_VECTORS_minuend_vector.toString());
        functionBlockManagement.connect(this, vectorXYZToVectorXY_3, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), subtractVectors_2, ConnectionEntry.SUBTRACT_VECTORS_subtrahend_vector.toString());
        //connections to subtractVectors_3
        functionBlockManagement.connect(this, vectorXYZToVectorXY_4, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), subtractVectors_3, ConnectionEntry.SUBTRACT_VECTORS_minuend_vector.toString());
        functionBlockManagement.connect(this, vectorXYZToVectorXY_5, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), subtractVectors_3, ConnectionEntry.SUBTRACT_VECTORS_subtrahend_vector.toString());
        //connections to subtractVectors_4
        functionBlockManagement.connect(this, vectorXYZToVectorXY_4, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), subtractVectors_4, ConnectionEntry.SUBTRACT_VECTORS_minuend_vector.toString());
        functionBlockManagement.connect(this, vectorXYZToVectorXY_6, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), subtractVectors_4, ConnectionEntry.SUBTRACT_VECTORS_subtrahend_vector.toString());


        //connections to normalOfDirectionVector2D_1
        functionBlockManagement.connect(this, subtractVectors_1, ConnectionEntry.SUBTRACT_VECTORS_subtracted_vector.toString(), normalOfDirectionVector2D_1, ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_direction_vector.toString());
        //connections to normalOfDirectionVector2D_2
        functionBlockManagement.connect(this, subtractVectors_2, ConnectionEntry.SUBTRACT_VECTORS_subtracted_vector.toString(), normalOfDirectionVector2D_2, ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_direction_vector.toString());
        //connections to normalOfDirectionVector2D_3
        functionBlockManagement.connect(this, subtractVectors_3, ConnectionEntry.SUBTRACT_VECTORS_subtracted_vector.toString(), normalOfDirectionVector2D_3, ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_direction_vector.toString());
        //connections to normalOfDirectionVector2D_4
        functionBlockManagement.connect(this, subtractVectors_4, ConnectionEntry.SUBTRACT_VECTORS_subtracted_vector.toString(), normalOfDirectionVector2D_4, ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_direction_vector.toString());


        //connections to intersectionPointOfLines_1
        functionBlockManagement.connect(this, vectorXYZToVectorXY_2, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), intersectionPointOfLines_1, ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_1.toString());
        functionBlockManagement.connect(this, normalOfDirectionVector2D_1, ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_normal_vector.toString(), intersectionPointOfLines_1, ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_1.toString());
        functionBlockManagement.connect(this, vectorXYZToVectorXY_3, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), intersectionPointOfLines_1, ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_2.toString());
        functionBlockManagement.connect(this, normalOfDirectionVector2D_2, ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_normal_vector.toString(), intersectionPointOfLines_1, ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_2.toString());
        //connections to intersectionPointOfLines_2
        functionBlockManagement.connect(this, vectorXYZToVectorXY_5, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), intersectionPointOfLines_2, ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_1.toString());
        functionBlockManagement.connect(this, normalOfDirectionVector2D_3, ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_normal_vector.toString(), intersectionPointOfLines_2, ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_1.toString());
        functionBlockManagement.connect(this, vectorXYZToVectorXY_6, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), intersectionPointOfLines_2, ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_2.toString());
        functionBlockManagement.connect(this, normalOfDirectionVector2D_4, ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_normal_vector.toString(), intersectionPointOfLines_2, ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_2.toString());


        //connections to subtractVectors_5
        functionBlockManagement.connect(this, intersectionPointOfLines_1, ConnectionEntry.INTERSECTION_POINT_OF_LINES_intersection_point.toString(), subtractVectors_5, ConnectionEntry.SUBTRACT_VECTORS_minuend_vector.toString());
        functionBlockManagement.connect(this, vectorXYZToVectorXY_1, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), subtractVectors_5, ConnectionEntry.SUBTRACT_VECTORS_subtrahend_vector.toString());
        //connections to subtractVectors_6
        functionBlockManagement.connect(this, intersectionPointOfLines_2, ConnectionEntry.INTERSECTION_POINT_OF_LINES_intersection_point.toString(), subtractVectors_6, ConnectionEntry.SUBTRACT_VECTORS_minuend_vector.toString());
        functionBlockManagement.connect(this, vectorXYZToVectorXY_4, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), subtractVectors_6, ConnectionEntry.SUBTRACT_VECTORS_subtrahend_vector.toString());


        //connections to intersectionPointOfLines_3
        functionBlockManagement.connect(this, vectorXYZToVectorXY_1, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), intersectionPointOfLines_3, ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_1.toString());
        functionBlockManagement.connect(this, subtractVectors_5, ConnectionEntry.SUBTRACT_VECTORS_subtracted_vector.toString(), intersectionPointOfLines_3, ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_1.toString());
        functionBlockManagement.connect(this, vectorXYZToVectorXY_4, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), intersectionPointOfLines_3, ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_2.toString());
        functionBlockManagement.connect(this, subtractVectors_6, ConnectionEntry.SUBTRACT_VECTORS_subtracted_vector.toString(), intersectionPointOfLines_3, ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_2.toString());


        //connections to minimumDistanceOfVectors
        functionBlockManagement.connect(this, vectorXYZToVectorXY_1, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), minimumDistanceOfVectors, ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_1_1.toString());
        functionBlockManagement.connect(this, intersectionPointOfLines_3, ConnectionEntry.INTERSECTION_POINT_OF_LINES_intersection_point.toString(), minimumDistanceOfVectors, ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_1_2.toString());
        functionBlockManagement.connect(this, vectorXYZToVectorXY_4, ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), minimumDistanceOfVectors, ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_2_1.toString());
        functionBlockManagement.connect(this, intersectionPointOfLines_3, ConnectionEntry.INTERSECTION_POINT_OF_LINES_intersection_point.toString(), minimumDistanceOfVectors, ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_2_2.toString());


        //connections to maximumSteeringAngleForRadius
        functionBlockManagement.connect(this, this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_wheelbase.toString(), maximumSteeringAngleForRadius, NavigationEntry.CONSTANT_WHEELBASE.toString());
        functionBlockManagement.connect(this, minimumDistanceOfVectors, ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_minimum_distance.toString(), maximumSteeringAngleForRadius, ConnectionEntry.MAXIMUM_STEERING_ANGLE_FOR_RADIUS_radius.toString());

        //connections to this
        functionBlockManagement.connect(this, intersectionPointOfLines_3, ConnectionEntry.INTERSECTION_POINT_OF_LINES_no_intersection_point.toString(), this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_no_intersection_point_of_lines.toString());
        functionBlockManagement.connect(this, maximumSteeringAngleForRadius, ConnectionEntry.MAXIMUM_STEERING_ANGLE_FOR_RADIUS_steering_angle.toString(), this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_steering_angle.toString());
    }

    /**
     * calculates the maximal steering angle in this section
     *
     * @param beginning first point in the section of the whole path
     * @param beginningInFront one point in front of the first point in the section of the whole path (variable distance)
     * @param beginningBehind one point before the first point in the section of the whole path (variable distance)
     * @param end last point in the section of the whole path
     * @param endInFront one point in front of the last point in the section of the whole path
     * @param endBehind one point behind the last point in the section of the whole path
     * @return the maximal steering angle in radians
     */
    /*private Double calculateMaximumSteeringAngleOfSection(RealVector beginning, RealVector beginningInFront, RealVector beginningBehind, RealVector end, RealVector endInFront, RealVector endBehind) {
        RealVector centrePoint;
        RealVector beginningNormalInFront;
        RealVector beginningNormalBehind;
        RealVector endNormalInFront;
        RealVector endNormalBehind;
        RealVector beginningDirectionVector;
        RealVector endDirectionVector;

        beginning = new ArrayRealVector(new double[] {beginning.getEntry(0), beginning.getEntry(1)});
        beginningInFront = new ArrayRealVector(new double[] {beginningInFront.getEntry(0), beginningInFront.getEntry(1)});
        beginningBehind = new ArrayRealVector(new double[] {beginningBehind.getEntry(0), beginningBehind.getEntry(1)});
        end = new ArrayRealVector(new double[] {end.getEntry(0), end.getEntry(1)});
        endInFront = new ArrayRealVector(new double[] {endInFront.getEntry(0), endInFront.getEntry(1)});
        endBehind = new ArrayRealVector(new double[] {endBehind.getEntry(0), endBehind.getEntry(1)});

        beginningNormalInFront = calculateNormal(beginning.subtract(beginningInFront));
        beginningNormalBehind = calculateNormal(beginning.subtract(beginningBehind));
        endNormalInFront = calculateNormal(end.subtract(endInFront));
        endNormalBehind = calculateNormal(end.subtract(endBehind));

        //TODO use IntersectionOfLines function block

        beginningDirectionVector = calculateIntersectionPoint(beginningBehind, beginningNormalBehind, beginningInFront, beginningNormalInFront).subtract(beginning);
        endDirectionVector = calculateIntersectionPoint(endBehind, endNormalBehind, endInFront, endNormalInFront).subtract(end);

        centrePoint = calculateIntersectionPoint(beginning, beginningDirectionVector, end, endDirectionVector);

        return calculateMaximumSteeringAngle(Math.min(beginning.getDistance(centrePoint), end.getDistance(centrePoint)));
    }*/
}
