/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import org.apache.commons.math3.linear.RealVector;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Calculate the two radii respectively and select the smaller one
 *
 * used sub blocks : none
 *
 * used formula : none
 *
 * Created by Christoph Grüne on 18.01.2017.
 */
public class MinimumDistanceOfVectors extends FunctionBlock{

    //Input Variables
    private RealVector vector1_1;
    private RealVector vector1_2;
    private RealVector vector2_1;
    private RealVector vector2_2;

    //Output Variables
    private Double minimumDistance;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public MinimumDistanceOfVectors() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        minimumDistance = Math.min(vector1_1.getDistance(vector1_2), vector2_1.getDistance(vector2_2));
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        vector1_1 = (RealVector) inputs.get(ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_1_1.toString());
        vector1_2 = (RealVector) inputs.get(ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_1_2.toString());
        vector2_1 = (RealVector) inputs.get(ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_2_1.toString());
        vector2_2 = (RealVector) inputs.get(ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_2_2.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_minimum_distance.toString(), minimumDistance);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[]   {
                                            ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_1_1.toString(),
                                            ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_1_2.toString(),
                                            ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_2_1.toString(),
                                            ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_2_2.toString()
                                        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
}
