/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Convert vertex to vector
 *
 * used sub block : none
 *
 * used formula: none
 *
 * Created by Christoph Grüne on 21.01.2017.
 */
public class VertexToVectorXY extends FunctionBlock{

    //Input Variables
    private Vertex vertex;

    //Output Variables
    private RealVector vectorXY;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public VertexToVectorXY() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        vectorXY = convertVertexXYZToVectorXY();
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        vertex = (Vertex) inputs.get(ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vertex.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString(), vectorXY);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vertex.toString()};
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/

    /**
     * converts a vector.xyz to vector.xy
     *
     * @return vector.xy
     */
    private RealVector convertVertexXYZToVectorXY() {
        return new ArrayRealVector(new double[] {vertex.getPosition().getEntry(0), vertex.getPosition().getEntry(1)});
    }
}
