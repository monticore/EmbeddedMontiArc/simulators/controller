/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.trajectoryPlanningBlock.linking;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.AccessToFunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

/**
 * Created by Christoph Grüne on 13.01.2017.
 */
@Deprecated
public class AccessToVelocityEstimationTotal extends AccessToFunctionBlock {

    //input blocks
    FunctionBlock parent;

    /**
     * Further information in the abstract super class
     */
    public AccessToVelocityEstimationTotal() {
        //connectionMap = new LinkedHashMap<String, Object>(2);
    }

//    /**
//     * connects function blocks to the other function block
//     *
//     * @param steeringLogic is a SteeringLogic function block
//     */
//    public void connect(FunctionBlock parent, FunctionBlock steeringLogic) {
//        this.parent = parent;
//    }
//
//    /**
//     * Further information in the abstract super class
//     */
//    public void collectAllInputs() {
//    }
}
