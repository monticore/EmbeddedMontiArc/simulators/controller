/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.utils.Pair;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.*;

/**
 * Find the nearest path from the start vertex to the target vertex
 *
 * used sub blocks: none
 *
 * used formula: dijkstra algorithm
 *
 * Created by Christoph Grüne on 22.12.2016.
 */
public class FindPath extends FunctionBlock {

    //Input Arguments
    Graph graph;
    Vertex startVertex;
    Vertex targetVertex;

    //Output Arguments
    List<Vertex> path;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a VelocityLogic object
     */
    public FindPath() {
        initComponents();
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/
    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        path = shortestPath(graph, startVertex, targetVertex);
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        graph = (Graph) inputs.get(ConnectionEntry.FIND_PATH_graph.toString());
        startVertex = (Vertex) inputs.get(ConnectionEntry.FIND_PATH_start_vertex.toString());
        targetVertex = (Vertex) inputs.get(ConnectionEntry.FIND_PATH_target_vertex.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.FIND_PATH_path.toString(), path);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[]{
                ConnectionEntry.FIND_PATH_graph.toString(),
                ConnectionEntry.FIND_PATH_start_vertex.toString(),
                ConnectionEntry.FIND_PATH_target_vertex.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {
    }

    /**
     * calculates the shortest path from v0 to vEnd
     *
     * @param v0   the start vertex
     * @param vEnd the end vertex
     * @return the shortest path from v0 to vEnd
     */
    public List<Vertex> shortestPath(Graph graph, Vertex v0, Vertex vEnd) {

        Map<Vertex, Pair<Vertex, Double>> predecessors = dijkstra(graph, v0, vEnd);

        return calculatePath(v0, vEnd, predecessors);
    }

    /**
     * calculates the path to every vertex from vertex v0
     *
     * @param v0   the start vertex
     * @param vEnd the end vertex
     * @return the predecessors of all vertices for every vertex with v0 as start vertex
     */
    private Map<Vertex, Pair<Vertex, Double>> dijkstra(Graph graph, Vertex v0, Vertex vEnd) {
        int numberOfVertices = graph.getNumberOfVertices();
        Map<Vertex, Pair<Vertex, Double>> result = new LinkedHashMap<>();

        //Store which vertex is in S
        List<Vertex> S = new ArrayList<>(numberOfVertices);

        S.add(v0);

        //Store which vertex is in V\S
        List<Vertex> notS = new ArrayList<>(numberOfVertices);

        for (Iterator<Vertex> it = graph.getVertices().iterator(); it.hasNext(); ) {
            Vertex cur = it.next();
            if (!cur.equals(v0)) {
                notS.add(cur);
            }
        }

        //init list of predecessors and costs
        for (Iterator<Vertex> it = graph.getVertices().iterator(); it.hasNext(); ) {
            Double cost;
            Vertex cur = it.next();

            if (graph.hasEdge(v0, cur)) {
                cost = graph.getEdge(v0, cur).getWeight();
            } else {
                cost = Double.POSITIVE_INFINITY;
            }
            result.put(cur, new Pair<>(v0, cost));
        }
        result.put(v0, new Pair<>(v0, 0.0));

        //main loop
        while (S.size() < numberOfVertices) {

            //get minimum
            int iMin = 0;
            Vertex vMin = notS.get(0);
            Double costMin = result.get(vMin).getValue();

            for (int i = 1; i < notS.size(); i++) {
                if (result.get(notS.get(i)).getValue() < costMin) {
                    iMin = i;
                    vMin = notS.get(i);
                    costMin = result.get(vMin).getValue();
                }
            }

            //Add vMin to S
            S.add(vMin);
            notS.remove(iMin);

            //Update D
            Vertex curV;
            Double oldCost, costFromMin, newCost;

            for (int i = 0; i < notS.size(); i++) {
                curV = notS.get(i);
                oldCost = result.get(curV).getValue();
                if (graph.hasEdge(vMin, curV)) {
                    costFromMin = graph.getEdge(vMin, curV).getWeight();
                } else {
                    costFromMin = Double.POSITIVE_INFINITY;
                }

                //cost of path to curV via vMin
                newCost = (costMin == Double.POSITIVE_INFINITY || costFromMin == Double.POSITIVE_INFINITY) ? Double.POSITIVE_INFINITY : costMin + costFromMin;

                if (oldCost > newCost) {
                    result.put(curV, new Pair<>(vMin, newCost));
                }
            }
        }
        return result;
    }

    /**
     * calculates the path form v0 to vEnd using a predecessors map
     *
     * @param v0           the start vertex
     * @param vEnd         the end vertex
     * @param predecessors map that contains the predecessor of every vertex for the path from v0
     * @return the path
     */
    private List<Vertex> calculatePath(Vertex v0, Vertex vEnd, Map<Vertex, Pair<Vertex, Double>> predecessors) {
        LinkedList<Vertex> list = new LinkedList<>();

        if (predecessors.get(vEnd).getValue().isNaN() || predecessors.get(vEnd).getValue().isInfinite()) {
            // throw new UnreachableVertexException("The target node you have chosen is unreachable.");
            // Return empty list as defined behavior if target node is unreachable, needed for traffic optimization for example
            return list;
        }

        Vertex cur = vEnd;

        while (!predecessors.get(cur).getKey().equals(v0)) {
            list.addFirst(cur);
            cur = predecessors.get(cur).getKey();
        }
        if (cur != v0) {
            list.addFirst(cur);
        }
        list.addFirst(v0);

        return list;
    }
}
