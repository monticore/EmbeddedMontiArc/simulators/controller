/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.NavigationEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IAdjacency;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IControllerNode;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock.components.*;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * This block calculate the navigation path for the car
 *
 * used sub blocks: ExtractGraph,
 *                  LocateNearestVertexInGraph
 *                  ExtractTargetVertex
 *                  FindPath
 *                  ExtractDetailedPath
 *                  TrajectoryPlanning
 *
 * used formula : none
 * Created by Christoph Grüne on 28.02.2017.
 */
public class NavigationBlock extends FunctionBlock {

    //Input Arguments
    List<IAdjacency> adjacencyList;
    IControllerNode targetNode;
    RealVector gpsCoordinates;
    Double wheelbase;

    //Output Arguments
    List<Vertex> path;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a VelocityLogic object
     */
    public NavigationBlock() {
        initComponents();
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/
    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        Map<String, Object> outputs = functionBlockManagement.executeAllFunctionBlocks(timeDelta);
        path = (List<Vertex>) outputs.get(ConnectionEntry.NAVIGATION_BLOCK_path.toString());
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        adjacencyList = (List<IAdjacency>) inputs.get(NavigationEntry.MAP_ADJACENCY_LIST.toString());
        targetNode = (IControllerNode) inputs.get(NavigationEntry.TARGET_NODE.toString());
        gpsCoordinates = (RealVector) inputs.get(NavigationEntry.GPS_COORDINATES.toString());
        wheelbase = (Double) inputs.get(NavigationEntry.CONSTANT_WHEELBASE.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(NavigationEntry.DETAILED_PATH_WITH_MAX_STEERING_ANGLE.toString(), path);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {};
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {
        functionBlockManagement = new FunctionBlockManagement(this);

        FunctionBlock extractGraph = new ExtractGraph();
        functionBlockManagement.addFunctionBlock(extractGraph);

        FunctionBlock locateNearestVertex = new LocateNearestVertexInGraph();
        functionBlockManagement.addFunctionBlock(locateNearestVertex);

        FunctionBlock extractTargetVertex = new ExtractTargetVertex();
        functionBlockManagement.addFunctionBlock(extractTargetVertex);

        FunctionBlock findPath = new FindPath();
        functionBlockManagement.addFunctionBlock(findPath);

        FunctionBlock extractDetailedPath = new ExtractDetailedPath();
        functionBlockManagement.addFunctionBlock(extractDetailedPath);

        // Note: Commented out for now because it causes trouble and is actually unused, see comment below
        /*
        FunctionBlock trajectoryPlanningBlock = new TrajectoryPlanningBlock();
        functionBlockManagement.addFunctionBlock(trajectoryPlanningBlock);
        */

        //connections to extractGraph
        functionBlockManagement.connect(this, this, NavigationEntry.MAP_ADJACENCY_LIST.toString(), extractGraph, ConnectionEntry.EXTRACT_GRAPH_adjacency_list.toString());

        //connections to locateNearestVertex
        functionBlockManagement.connect(this, this, NavigationEntry.GPS_COORDINATES.toString(), locateNearestVertex, ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_GRAPH_gps_coordinates.toString());
        functionBlockManagement.connect(this, extractGraph, ConnectionEntry.EXTRACT_GRAPH_graph.toString(), locateNearestVertex, ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_GRAPH_graph.toString());

        //connections to extractTargetVertex
        functionBlockManagement.connect(this, this, NavigationEntry.TARGET_NODE.toString(), extractTargetVertex, ConnectionEntry.EXTRACT_TARGET_VERTEX_target_node.toString());
        functionBlockManagement.connect(this, extractGraph, ConnectionEntry.EXTRACT_GRAPH_graph.toString(), extractTargetVertex, ConnectionEntry.EXTRACT_TARGET_VERTEX_graph.toString());

        //connections to findPath
        functionBlockManagement.connect(this, extractGraph, ConnectionEntry.EXTRACT_GRAPH_graph.toString(), findPath, ConnectionEntry.FIND_PATH_graph.toString());
        functionBlockManagement.connect(this, locateNearestVertex, ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_GRAPH_nearest_vertex.toString(), findPath, ConnectionEntry.FIND_PATH_start_vertex.toString());
        functionBlockManagement.connect(this, extractTargetVertex, ConnectionEntry.EXTRACT_TARGET_VERTEX_target_vertex.toString(), findPath, ConnectionEntry.FIND_PATH_target_vertex.toString());

        //connections to extractDetailedPath
        functionBlockManagement.connect(this, findPath, ConnectionEntry.FIND_PATH_path.toString(), extractDetailedPath, ConnectionEntry.EXTRACT_DETAILED_PATH_non_detailed_path.toString());

        // Note: The following section was originally supposed to be used, but it causes issues when used multiple times
        // to recalculate the navigation (function blocks have state that is not reset correctly).
        // Since the values computed by these function blocks are not used anyways, they are therefore skipped for now.
        // The replacement code below just bypasses these function blocks and forwards the result.
        /*
        //connections to trajectoryPlanningBlock
        functionBlockManagement.connect(this, this, NavigationEntry.CONSTANT_WHEELBASE.toString(), trajectoryPlanningBlock, ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_wheelbase.toString());
        functionBlockManagement.connect(this, extractDetailedPath, ConnectionEntry.EXTRACT_DETAILED_PATH_detailed_path.toString(), trajectoryPlanningBlock, ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_path.toString());

        //connections to this
        functionBlockManagement.connect(this, trajectoryPlanningBlock, ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_detailed_path.toString(), this, ConnectionEntry.NAVIGATION_BLOCK_path.toString());
        */
        // Replacement code
        functionBlockManagement.connect(this, extractDetailedPath, ConnectionEntry.EXTRACT_DETAILED_PATH_detailed_path.toString(), this, ConnectionEntry.NAVIGATION_BLOCK_path.toString());
    }
}
