/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.trajectoryPlanningBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * After calculation of the maximal steering angle, assign it to the vertexes, which belong
 * to the section
 *
 * used sub blocks: none
 *
 * used formula: none
 *
 * Created by Christoph Grüne on 02.03.2017.
 */
public class SetMaximumSteeringAngleInVertex extends FunctionBlock {

    //Input Variables
    private List<Vertex> path;
    private Vertex beginning;
    private Vertex end;
    private Double steeringAngle;

    //Output Variables
    private List<Vertex> pathWithAngles;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public SetMaximumSteeringAngleInVertex() {
        initComponents();
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        pathWithAngles = calculatePathWithMaximumVelocities(path, beginning, end, steeringAngle);
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        path = (List<Vertex>) inputs.get(ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_path.toString());
        beginning = (Vertex) inputs.get(ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_beginning.toString());
        end = (Vertex) inputs.get(ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_end.toString());
        steeringAngle = (Double) inputs.get(ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_steering_angle.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_path_with_angles.toString(), pathWithAngles);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[]{
                ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_path.toString(),
                ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_beginning.toString(),
                ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_end.toString(),
                ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_steering_angle.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/

    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {}

    /**
     * sets maximumSteeringAngle of all vertices between beginning and end on path to steeringAngle
     *
     * @param path the path you want to manipulate
     * @param beginning the beginning vertex
     * @param end the end vertex
     * @param steeringAngle the maximal steering angle in the section between beginning and end on path
     */
    private List<Vertex> calculatePathWithMaximumVelocities(List<Vertex> path, Vertex beginning, Vertex end, Double steeringAngle) {
        Iterator<Vertex> iterator = path.listIterator(path.indexOf(beginning));
        Vertex cur = iterator.next();
        while(iterator.hasNext() && cur != end) {
            cur.setMaximumSteeringAngle(steeringAngle);
            cur = iterator.next();
        }
        if(cur == end) {
            cur.setMaximumSteeringAngle(steeringAngle);
        }
        return path;
    }
}
