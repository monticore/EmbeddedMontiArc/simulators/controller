/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IControllerNode;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.RotationConvention;
import org.apache.commons.math3.geometry.euclidean.threed.RotationOrder;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.util.CombinatoricsUtils;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Create more vertexes along the path, to make the path more specific
 *
 * used sub block: none
 *
 * used formula: simple formulas
 * Created by Christoph Grüne on 28.02.2017.
 */
public class ExtractDetailedPath extends FunctionBlock {

    //Input Arguments
    List<Vertex> nonDetailedPath;

    //Output Arguments
    List<Vertex> detailedPath;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a VelocityLogic object
     */
    public ExtractDetailedPath() {
        initComponents();
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/
    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        detailedPath = calculateDetailedPath(nonDetailedPath);
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        nonDetailedPath = (List<Vertex>) inputs.get(ConnectionEntry.EXTRACT_DETAILED_PATH_non_detailed_path.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.EXTRACT_DETAILED_PATH_detailed_path.toString(), detailedPath);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.EXTRACT_DETAILED_PATH_non_detailed_path.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {}

    /**
     * constructs a List with the IDs of vertices of the Path
     *
     * @return the List of IDs
     */
    @Deprecated
    private List<Long> nonDetailedPathID(List<Vertex> nonDetailedPath) {
        List<Long> list = nonDetailedPath.stream().map(Vertex::getOsmId).collect(Collectors.toCollection(LinkedList::new));
        return list;
    }

    /**
     * calculates a detailed path based on the path
     *
     * @return the detailedPath
     */
    @Deprecated
    private List<Vertex> detailedPath(List<Vertex> nonDetailedPath) {
        List<Vertex> nodeList = calculateDetailedPath(nonDetailedPath);
        List<Vertex> vertexList = new LinkedList<>();
        /*for (IControllerNode node : nodeList) {
            vertexList.add(convertNodeToVertex(node));
        }*/
        return vertexList;
    }

    /**
     * calculates a detailed path from a non-detailed by interpolating the vertices linearly
     * first implemented by Lukas Walbröl, modified by Christoph Grüne in order to migrate it to this functionBlock
     *
     * @param nonDetailedPath the non-detailed path to interpolate
     * @return the detailed path
     */
    private List<Vertex> calculateDetailedPath(List<Vertex> nonDetailedPath) {
        List<Vertex> tmpResult1 = new ArrayList<>();
        List<Vertex> tmpResult2 = new ArrayList<>();

        if(nonDetailedPath.size() < 2) {
            return nonDetailedPath;
        }

        for(int i = 0; i < nonDetailedPath.size() - 1; i++) {
            //long osmId1 = path.get(i);
            //long osmId2 = path.get(i + 1);
            Vertex vertex1 = nonDetailedPath.get(i);
            Vertex vertex2 = nonDetailedPath.get(i + 1);

            ArrayList<Vertex> tmpList = interpolatePath(vertex1, vertex2);
            if(i != 0) {
                tmpList.remove(0);
            }
            tmpResult1.addAll(tmpList);

        }

        // Store a list of vertex arrays with 3 vertices with sharp turns, needed for later reference
        // Sharp turns can only occur at vertices from OSM data
        // Apply to curves with 25 deg = 0.436332 rad angle
        List<Vertex[]> sharpTurnVertices = new LinkedList<>();
        double sharpCurveThreshold = 0.436332;

        for (int i = 1; i < (tmpResult1.size() - 1); ++i) {
            // It is always fine to access previous and next vertex because of for loop definition
            Vertex start = tmpResult1.get(i - 1);
            Vertex middle = tmpResult1.get(i);
            Vertex end = tmpResult1.get(i + 1);

            // Ignore vertices at intersections
            boolean angleExceeded = verticesExceedAngle(start, middle, end, sharpCurveThreshold);

            // Store data if sharp turn with OSM id
            if (angleExceeded && middle.getOsmId() > 0) {
                Vertex[] vertices = {start, middle, end};
                sharpTurnVertices.add(vertices);
            }
        }

        // Shift detailed path towards right lane for all straight line segments (e.g. exclude intersections)
        // Ignore vertices that are near a vertex with sharp curves (50 degree = 0.872665 rad)
        // Default street width is fixed to 6 meters
        double defaultStreetWidth = 6.0;
        double defaultStreetOffset = (defaultStreetWidth / 4.0);
        double laneShiftAngleThreshold = 0.8762665;

        for (int i = 1; i < (tmpResult1.size() - 1); ++i) {

            // It is always fine to access previous and next vertex because of for loop definition
            Vertex start = tmpResult1.get(i-1);
            Vertex middle = tmpResult1.get(i);
            Vertex end = tmpResult1.get(i+1);

            // Ignore vertices at intersections
            boolean angleExceeded = verticesExceedAngle(start, middle, end, laneShiftAngleThreshold);

            // Special case for i == 1, also handle first vertex
            if (i == 1) {
                if (angleExceeded) {
                    tmpResult2.add(start);
                } else {
                    tmpResult2.add(vertexLaneRelocation(Optional.of(start), Optional.of(middle), Optional.empty(), defaultStreetOffset));
                }
            }

            // Handle current vertex
            if (angleExceeded) {
                tmpResult2.add(middle);
            } else {
                tmpResult2.add(vertexLaneRelocation(Optional.of(start), Optional.of(middle), Optional.of(end), defaultStreetOffset));
            }

            // Special case for i == (tmpResult1.size() - 2), also handle last vertex
            if (i == (tmpResult1.size() - 2)) {
                if (angleExceeded) {
                    tmpResult2.add(end);
                } else {
                    tmpResult2.add(vertexLaneRelocation(Optional.empty(), Optional.of(middle), Optional.of(end), defaultStreetOffset));
                }
            }
        }

        // Copy from tmpResult2 to tmpResult1 and clear tmpResult2
        if (!tmpResult2.isEmpty()) {
            tmpResult1 = new ArrayList<>(tmpResult2);
            tmpResult2.clear();
        }

        // Smooth sharp curves in detailed path for better steering results
        // Use Bezier Curve interpolation, this requires control points of the curve
        // Apply to curves with 25 deg = 0.436332 rad angle
        double curveSmoothingAngle = 0.436332;
        int verticesCurveSmoothingRight = 8;
        int verticesCurveSmoothingLeft = 6;

        tmpResult2 = smoothCurves(tmpResult1, sharpTurnVertices, verticesCurveSmoothingRight, curveSmoothingAngle, true);
        if (!tmpResult2.isEmpty()) {
            tmpResult1 = new ArrayList<>(tmpResult2);
            tmpResult2.clear();
        }

        tmpResult2 = smoothCurves(tmpResult1, sharpTurnVertices, verticesCurveSmoothingLeft, curveSmoothingAngle, false);
        if (!tmpResult2.isEmpty()) {
            tmpResult1 = new ArrayList<>(tmpResult2);
            tmpResult2.clear();
        }

        return tmpResult1;
    }

    /**
     * Compute smoothing for a specific kind of curves
     *
     * @param inputVertices List of input vertices
     * @param sharpTurnVertices List of original vertices before lane shifting with sharp turns
     * @param verticesCurveSmoothing Amount of vertices to be checked / replaced at curves
     * @param maxAngleSmoothing Maximum angle for curves to be smoothed
     * @param checkForRightCurve True if right curves are processed, otherwise false
     * @return List of vertices with smoothed curves
     */
    private ArrayList<Vertex> smoothCurves(List<Vertex> inputVertices, List<Vertex[]> sharpTurnVertices, int verticesCurveSmoothing, double maxAngleSmoothing, boolean checkForRightCurve) {
        int skipVerticesSmoothing = 0;
        ArrayList<Vertex> result = new ArrayList<>();

        // Prevent errors, return empty result
        if (verticesCurveSmoothing < 2) {
            throw new IllegalArgumentException("smoothCurves - verticesCurveSmoothing must be greater or equal to 2");
        }

        for (int i = 0; i < (inputVertices.size() - verticesCurveSmoothing); ++i) {

            // Skip vertices because they are already processed
            if (skipVerticesSmoothing > 0) {
                skipVerticesSmoothing--;
                continue;
            }

            // By default set values to false
            boolean angleExceeded = false;
            boolean isCorrectCurveDirection = false;

            // It is always fine to access next vertices because of for loop definition
            // i is current start vertex, some are skipped, i+(verticesCurveSmoothing / 2) is curve vertex, some are skipped, i+verticesCurveSmoothing is target vertex
            Vertex[] vertices = {inputVertices.get(i), inputVertices.get(i+(verticesCurveSmoothing / 2)), inputVertices.get(i+verticesCurveSmoothing)};
            Vertex[] verticesOriginal = {};

            // Only check vertices with original OSM id
            long osmIdMiddle = inputVertices.get(i+(verticesCurveSmoothing / 2)).getOsmId();
            if (osmIdMiddle > 0) {
                // Find vertices in original sharp turn vertices
                for (Vertex[] sharpVertices : sharpTurnVertices) {
                    if (sharpVertices.length == 3) {
                        if (sharpVertices[1].getOsmId() == osmIdMiddle) {
                            if (verticesExceedAngle(sharpVertices[0], sharpVertices[1], sharpVertices[2], maxAngleSmoothing)) {
                                angleExceeded = true;
                                verticesOriginal = sharpVertices;
                            }
                        }
                    }
                }
            }

            // If angle is exceeded, perform more detailed checks
            if (angleExceeded) {
                // Compute directed angle
                RealVector start = verticesOriginal[0].getPosition();
                RealVector middle = verticesOriginal[1].getPosition();
                RealVector end = verticesOriginal[2].getPosition();
                RealVector startMiddle = middle.subtract(start);
                RealVector middleEnd = end.subtract(middle);
                double directedAngleToXAxis = -Math.atan2(startMiddle.getEntry(1), startMiddle.getEntry(0)) + 0.5 * Math.PI;

                Rotation rotation = new Rotation(RotationOrder.XYZ, RotationConvention.VECTOR_OPERATOR, 0.0, 0.0, directedAngleToXAxis);
                RealMatrix rotationMatrix = new BlockRealMatrix(rotation.getMatrix());
                RealVector middleEndRotated = rotationMatrix.operate(middleEnd);

                // Use 0.02 as check to avoid numerical issues near 0.0
                if ((middleEndRotated.getEntry(0) <= 0.02 && !checkForRightCurve) || (middleEndRotated.getEntry(0) >= 0.02 && checkForRightCurve)) {
                    isCorrectCurveDirection = true;
                }
            }

            if (angleExceeded && isCorrectCurveDirection) {
                skipVerticesSmoothing = verticesCurveSmoothing;

                RealVector positions[] = new RealVector[9];
                Vertex curveVertex[] = new Vertex[9];

                for (int j = 0; j < 9; ++j) {
                    positions[j] = bezierCurveInterpolation(3, 0.1 * (j+1), vertices);
                    curveVertex[j] = new Vertex(-1L, -1L, positions[j], 0.0);
                }

                curveVertex[4] = new Vertex(inputVertices.get(i+(verticesCurveSmoothing / 2)).getId(), inputVertices.get(i+(verticesCurveSmoothing / 2)).getOsmId(), positions[4], inputVertices.get(i+(verticesCurveSmoothing / 2)).getMaximumSteeringAngle());

                result.add(inputVertices.get(i));

                for (int j = 0; j < 9; ++j) {
                    result.add(curveVertex[j]);
                }

                result.add(inputVertices.get(i+verticesCurveSmoothing));

                // Otherwise just add current vertex and continue
            } else {
                result.add(inputVertices.get(i));
            }
        }

        // Add remaining nodes that are not included in the smoothing
        for (int i = Math.max(0, (inputVertices.size() - verticesCurveSmoothing)); i < inputVertices.size(); ++i) {
            result.add(inputVertices.get(i));
        }

        return result;
    }
    /**
     * Compute a bezier curve interpolation
     *
     * @param degree Degree for bezier curve interpolation
     * @param amount Amount between 0.0 and 1.0 defining the points of the curve as percentage
     * @param vertices Array of vertices that needs to have exactly 6 entries
     * @return RealVector on quintic bezier curve
     */
    private RealVector bezierCurveInterpolation(int degree, double amount, Vertex[] vertices) {

        // Error checks
        if (amount < 0.0 || amount > 1.0) {
            throw new IllegalArgumentException("bezierCurveInterpolation - Amount must be between 0.0 and 1.0!");
        }

        if (vertices.length != degree) {
            throw new IllegalArgumentException("bezierCurveInterpolation - Vertices array does not have as many entries as defined by degree!");
        }

        double sumOfZPositions = 0.0;
        RealVector resultPos = new ArrayRealVector(new double[] {0.0, 0.0, 0.0});

        // Compute bezier curve interpolation
        for (int i = 0; i < degree; ++i) {
            double factor = CombinatoricsUtils.binomialCoefficientDouble((degree - 1), i) * Math.pow((1.0 - amount), ((degree - 1) - i)) * Math.pow(amount, i);
            resultPos = resultPos.add(vertices[i].getPosition().mapMultiply(factor));
            sumOfZPositions = sumOfZPositions + vertices[i].getPosition().getEntry(2);
        }

        // Use average value for Z coordinate
        resultPos.setEntry(2, (sumOfZPositions / degree));
        return resultPos;
    }

    /**
     * Relocates a vertex from the trajectory to a given sideways offset
     * If start is not set, the end vertex is relocated
     * If end is not set, the start vertex is relocated
     * Otherwise middle vertex is relocated
     *
     * @param start the start vertex
     * @param middle the middle vertex
     * @param end the end vertex
     * @param offset Offset to be added to relocated vertex
     * @return Relocated vertex
     */
    private Vertex vertexLaneRelocation(Optional<Vertex> start, Optional<Vertex> middle, Optional<Vertex> end, double offset) {

        RealVector relocationVector1 = new ArrayRealVector(new double[] {0.0, 0.0, 0.0});
        RealVector relocationVector2 = new ArrayRealVector(new double[] {0.0, 0.0, 0.0});

        if (start.isPresent() && middle.isPresent()) {
            // Only interested in 2D, then needed perpendicular vector of (x, y) is (y, -x)
            relocationVector1 = middle.get().getPosition().subtract(start.get().getPosition());
            double xVal = relocationVector1.getEntry(0);
            double yVal = relocationVector1.getEntry(1);
            relocationVector1.setEntry(0, yVal);
            relocationVector1.setEntry(1, -xVal);
            relocationVector1.setEntry(2, 0.0);

            if (relocationVector1.getNorm() != 0.0) {
                relocationVector1.unitize();
            }
        }

        if (middle.isPresent() && end.isPresent()) {
            // Only interested in 2D, then needed perpendicular vector of (x, y) is (y, -x)
            relocationVector2 = end.get().getPosition().subtract(middle.get().getPosition());
            double xVal = relocationVector2.getEntry(0);
            double yVal = relocationVector2.getEntry(1);
            relocationVector2.setEntry(0, yVal);
            relocationVector2.setEntry(1, -xVal);
            relocationVector2.setEntry(2, 0.0);

            if (relocationVector2.getNorm() != 0.0) {
                relocationVector2.unitize();
            }
        }

        // If start is not set, the end vertex is relocated
        if (end.isPresent() && !start.isPresent()) {
            RealVector pos = end.get().getPosition().add(relocationVector2.mapMultiply(offset));
            return new Vertex(end.get().getId(), end.get().getOsmId(), pos, end.get().getMaximumSteeringAngle());
        }

        // If end is not set, the start vertex is relocated
        if (start.isPresent() && !end.isPresent()) {
            RealVector pos = start.get().getPosition().add(relocationVector1.mapMultiply(offset));
            return new Vertex(start.get().getId(), start.get().getOsmId(), pos, start.get().getMaximumSteeringAngle());
        }

        // Otherwise middle vertex is relocated with average
        relocationVector1 = relocationVector1.add(relocationVector2).mapMultiply(0.5);

        if (relocationVector1.getNorm() != 0.0) {
            relocationVector1.unitize();
        }

        if (middle.isPresent()) {
            RealVector pos = middle.get().getPosition().add(relocationVector1.mapMultiply(offset));
            return new Vertex(middle.get().getId(), middle.get().getOsmId(), pos, middle.get().getMaximumSteeringAngle());
        }

        return null;
    }

    /**
     * checks if curve defined by vertices exceed a given angle
     *
     * @param start the start vertex
     * @param middle the middle vertex
     * @param end the end vertex
     * @param angleThreshold Angle to be checked against
     * @return true if the curve defined by vertices exceeds angle, otherwise false
     */
    private boolean verticesExceedAngle(Vertex start, Vertex middle, Vertex end, double angleThreshold) {
        // Compute directional vectors
        RealVector startMiddle = middle.getPosition().subtract(start.getPosition());
        RealVector middleEnd = end.getPosition().subtract(middle.getPosition());

        // No angle can be computed if norm is 0.0, return false
        if (startMiddle.getNorm() == 0.0 || middleEnd.getNorm() == 0.0) {
            return false;
        }

        // Compute cosine angle for shortest angle between vectors
        double cosAngle = startMiddle.cosine(middleEnd);
        double realAngle = Math.acos(cosAngle);

        // Return true if the realAngle is bigger than threshold
        return realAngle > angleThreshold;
    }

    /**
     * interpolates a path linearly between vertex1 and vertex2
     * first implemented by Lukas Walbröl, modified by Christoph Grüne in order to migrate it to this functionBlock
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     * @return an linearly interpolated path between vertex1 and vertex2
     */
    private ArrayList<Vertex> interpolatePath(Vertex vertex1, Vertex vertex2) {
        ArrayList<Vertex> result = new ArrayList<>();
        //Point3D normDiff = difference.normalize().multiply(IControllerNode.INTERPOLATION_DISTANCE);
        RealVector normDiff = vertex2.getPosition().subtract(vertex1.getPosition()).unitVector().mapMultiply(IControllerNode.INTERPOLATION_DISTANCE);

        //result.add(new ControllerNode(p3D1.p, osmId1));
        result.add(vertex1);
        //double distance = p3D1.p.distance(p3D2.p);
        double distance = vertex1.getPosition().getDistance(vertex2.getPosition());


        double loops = distance / IControllerNode.INTERPOLATION_DISTANCE;
        if(loops - Math.floor(loops) == 0.0) {
            loops -= 0.1;
        }
        loops = Math.floor(loops);

        //Point3D tmpP = p3D1.p;
        RealVector tempVector = vertex1.getPosition();
        for(int i = 0; i < loops; i++) {
            tempVector = tempVector.add(normDiff);
            //result.add(new ControllerNode(tmpP, IControllerNode.INTERPOLATED_NODE));
            result.add(new Vertex(-1L, -1L, tempVector, 0.0));
        }

        //result.add(new ControllerNode(p3D2.p, osmId2));
        result.add(vertex2);
        return result;
    }

    /**
     * converts a IControllerNode to a vertex
     *
     * @param node the node that will be converted to a vertex
     * @return the converted vertex
     */
    @Deprecated
    private Vertex convertNodeToVertex(IControllerNode node) {
        RealVector pos = new ArrayRealVector(new Double[] {
                node.getPoint().getX(),
                node.getPoint().getY(),
                node.getPoint().getZ()
        });
        Vertex vertex = new Vertex(node.getId(), node.getOsmId(), pos, 0.0);
        return vertex;
    }
}
