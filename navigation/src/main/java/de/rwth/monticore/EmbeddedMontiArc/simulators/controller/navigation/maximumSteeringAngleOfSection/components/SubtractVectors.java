/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.*;
import org.apache.commons.math3.linear.RealVector;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Derive the direction vector
 *
 * used sub blocks: none
 *
 * used formula: none
 * Created by Christoph Grüne on 18.01.2017.
 */
public class SubtractVectors extends FunctionBlock{

    //Input Variables
    private RealVector minuendVector;
    private RealVector subtrahendVector;

    //Output Variables
    private RealVector subtractedVector;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public SubtractVectors() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        subtractedVector = minuendVector.subtract(subtrahendVector);
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        minuendVector = (RealVector) inputs.get(ConnectionEntry.SUBTRACT_VECTORS_minuend_vector.toString());
        subtrahendVector = (RealVector) inputs.get(ConnectionEntry.SUBTRACT_VECTORS_subtrahend_vector.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.SUBTRACT_VECTORS_subtracted_vector.toString(), subtractedVector);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {ConnectionEntry.SUBTRACT_VECTORS_minuend_vector.toString(), ConnectionEntry.SUBTRACT_VECTORS_subtrahend_vector.toString()};
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
}
