/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.NavigationEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Calculate the maximal steering angle with radius and the wheelbase of car
 *
 * used sub block: none
 *
 * used formula : angle = Math.asin(wheelbase / radius)
 *
 * Created by Christoph Grüne on 18.01.2017.
 */
public class MaximumSteeringAngleForRadius extends FunctionBlock{

    //Input Variables
    private Double wheelbase;
    private Double radius;

    //Output Variables
    private Double steeringAngle;

    //Global Variables

    //Used Function Blocks

    /**
     * Constructor for a InnerControlBlock object
     */
    public MaximumSteeringAngleForRadius() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        steeringAngle = calculateMaximumSteeringAngle();
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        wheelbase = (Double) inputs.get(NavigationEntry.CONSTANT_WHEELBASE.toString());
        radius = (Double) inputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_FOR_RADIUS_radius.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.MAXIMUM_STEERING_ANGLE_FOR_RADIUS_steering_angle.toString(), steeringAngle);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {NavigationEntry.CONSTANT_WHEELBASE.toString(), ConnectionEntry.MAXIMUM_STEERING_ANGLE_FOR_RADIUS_radius.toString()};
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/

    /**
     * calculates the maximum steering angle in this part
     */
    private Double calculateMaximumSteeringAngle() {
        if(radius == 0.0) {
            return 0.0;
        }
        return (Double) (Math.asin(wheelbase / radius));
    }
}
