/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.trajectoryPlanningBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.MaximumSteeringAngleOfSection;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Christoph Grüne on 13.01.2017.
 */
@Deprecated
public class VelocityEstimationSection extends FunctionBlock {

    //Input Variables
    private Double constantWheelbase;
    private Vertex beginning;
    private Vertex beginningInFront;
    private Vertex beginningBehind;
    private Vertex end;
    private Vertex endInFront;
    private Vertex endBehind;

    //Output Variables
    private Double maximumVelocityInSection;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public VelocityEstimationSection() {initComponents();}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        Map<String, Object> outputs = functionBlockManagement.executeAllFunctionBlocks(timeDelta);
        maximumVelocityInSection = (Double) outputs.get(ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_velocity.toString());
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        /*constantWheelbase = (Double) inputs.get(BusEntry.CONSTANT_WHEELBASE.toString());
        beginning = (Vertex) inputs.get(ConnectionEntry.BEGINNING_VECTOR.toString());
        beginningInFront = (Vertex) inputs.get(ConnectionEntry.BEGINNING_IN_FRONT_VECTOR.toString());
        beginningBehind = (Vertex) inputs.get(ConnectionEntry.BEGINNING_BEHIND_VECTOR.toString());
        end = (Vertex) inputs.get(ConnectionEntry.END_VECTOR.toString());
        endInFront = (Vertex) inputs.get(ConnectionEntry.END_IN_FRONT_VECTOR.toString());
        endBehind = (Vertex) inputs.get(ConnectionEntry.END_BEHIND_VECTOR.toString());
        */super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        //outputs.put(ConnectionEntry.MAXIMUM_VELOCITY_IN_SECTION.toString(), maximumVelocityInSection);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                /*BusEntry.SENSOR_CURRENT_SURFACE.toString(),
                BusEntry.CONSTANT_WHEELBASE.toString(),
                ConnectionEntry.BEGINNING_VECTOR.toString(),
                ConnectionEntry.BEGINNING_IN_FRONT_VECTOR.toString(),
                ConnectionEntry.BEGINNING_BEHIND_VECTOR.toString(),
                ConnectionEntry.END_VECTOR.toString(),
                ConnectionEntry.END_IN_FRONT_VECTOR.toString(),
                ConnectionEntry.END_BEHIND_VECTOR.toString()*/
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {

        //TODO OutputConnections?!

        functionBlockManagement = new FunctionBlockManagement(this);

        FunctionBlock maximumSteeringAngleOfSection = new MaximumSteeringAngleOfSection();
        functionBlockManagement.addFunctionBlock(maximumSteeringAngleOfSection);

        //FunctionBlock maximumVelocityBySteeringAnAngle = new MaximumVelocityBySteeringAnAngle();
        //functionBlockManagement.addFunctionBlock(maximumVelocityBySteeringAnAngle);

        //connections to maximumSteeringAngleOfSection
        /*functionBlockManagement.connect(this, this, BusEntry.CONSTANT_WHEELBASE.toString(), maximumSteeringAngleOfSection, BusEntry.CONSTANT_WHEELBASE.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.BEGINNING_VECTOR.toString(), maximumSteeringAngleOfSection, ConnectionEntry.BEGINNING_VECTOR.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.BEGINNING_IN_FRONT_VECTOR.toString(), maximumSteeringAngleOfSection, ConnectionEntry.BEGINNING_IN_FRONT_VECTOR.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.BEGINNING_BEHIND_VECTOR.toString(), maximumSteeringAngleOfSection, ConnectionEntry.BEGINNING_BEHIND_VECTOR.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.END_VECTOR.toString(), maximumSteeringAngleOfSection, ConnectionEntry.END_VECTOR.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.END_IN_FRONT_VECTOR.toString(), maximumSteeringAngleOfSection, ConnectionEntry.END_IN_FRONT_VECTOR.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.END_BEHIND_VECTOR.toString(), maximumSteeringAngleOfSection, ConnectionEntry.END_BEHIND_VECTOR.toString());
        */
    }
}
