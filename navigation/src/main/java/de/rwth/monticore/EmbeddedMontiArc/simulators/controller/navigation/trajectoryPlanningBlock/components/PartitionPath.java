/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.trajectoryPlanningBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.exceptions.IllegalPathIteratorException;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Partition the path into sections, each section represented by 6 vertexes and lengthOfSection
 * in between
 *
 * used sub blocks : none
 *
 * used formula: none
 * Created by Christoph Grüne on 20.01.2017.
 */
public class PartitionPath extends FunctionBlock {

    //Input Variables
    private Iterator<Vertex> pathIterator;
    private Vertex lastEnd;
    private Vertex lastEndInFront;
    private Vertex lastEndBehind;

    //Output Variables
    private Vertex beginning;
    private Vertex beginningInFront;
    private Vertex beginningBehind;
    private Vertex end;
    private Vertex endInFront;
    private Vertex endBehind;
    private Boolean pathEnd;

    //Global Variables
    private Double distanceOfMeasuringPoints;
    private Double lengthOfSections;

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public PartitionPath(Double distanceOfMeasuringPoints, Double lengthOfSections) {
        this.distanceOfMeasuringPoints = distanceOfMeasuringPoints;
        this.lengthOfSections = lengthOfSections;
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {

        if(!pathIterator.hasNext()) {
            throw new IllegalPathIteratorException("NavigationBlock->TrajectoryPlanningBlock->PartitionPath: An incorrect path iterator was passed.");
        }

        int state = 0;

        Double distance = 0.0;
        Vertex last = null;
        pathEnd = false;

        if(lastEnd != null && lastEndBehind != null && lastEndInFront != null) {
            beginning = lastEnd;
            beginningInFront = lastEndInFront;
            beginningBehind = lastEndBehind;

            end = lastEnd;
            endInFront = lastEndInFront;
            endBehind = lastEndBehind;

            last = lastEndBehind;

            distance = 2 * distanceOfMeasuringPoints;

            state = 2;
        }

        while (pathIterator.hasNext() && state != 6) {
            Vertex current = pathIterator.next();

            if (0 == state) {
                last = current;
                beginningInFront = current;
                beginning = current;
                beginningBehind = current;
                endInFront = current;
                end = current;
                endBehind = current;
                state = 1;
            } else if (1 == state && distance >= distanceOfMeasuringPoints) {
                beginning = current;
                state = 2;
            }else if(2 == state && distance >= 2 * distanceOfMeasuringPoints) {
                beginningBehind = current;
                state = 3;
            } else if (3 == state && distance >= lengthOfSections) {
                endInFront = current;
                state = 4;
            } else if (4 == state && distance >= lengthOfSections + distanceOfMeasuringPoints) {
                end = current;
                state = 5;
            } else if (5 == state && distance >= lengthOfSections + 2 * distanceOfMeasuringPoints) {
                endBehind = current;
                state = 6;
            }

            //necessary for the last section of the path
            if(state < 3) {
                endInFront = end;
            }
            if(state < 4) {
                end = endBehind;
            }
            if(state < 5) {
                endBehind = current;
            }

            distance += current.getPosition().getDistance(last.getPosition());
            last = current;
        }
        if(!pathIterator.hasNext()) {
            pathEnd = true;
        }
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        pathIterator = (Iterator<Vertex>) inputs.get(ConnectionEntry.PARTITION_PATH_path_iterator.toString());
        try {
            lastEnd = (Vertex) inputs.get(ConnectionEntry.PARTITION_PATH_last_end.toString());
            lastEndInFront = (Vertex) inputs.get(ConnectionEntry.PARTITION_PATH_last_end_in_front.toString());
            lastEndBehind = (Vertex) inputs.get(ConnectionEntry.PARTITION_PATH_last_end_behind.toString());
        } catch(NullPointerException e) {
            lastEnd = null;
            lastEndInFront = null;
            lastEndBehind = null;
        }
        super.setInputs(inputs,
                ConnectionEntry.PARTITION_PATH_last_end.toString(),
                ConnectionEntry.PARTITION_PATH_last_end_in_front.toString(),
                ConnectionEntry.PARTITION_PATH_last_end_behind.toString()
        );
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.PARTITION_PATH_beginning.toString(), beginning);
        outputs.put(ConnectionEntry.PARTITION_PATH_beginning_in_front.toString(), beginningInFront);
        outputs.put(ConnectionEntry.PARTITION_PATH_beginning_behind.toString(), beginningBehind);
        outputs.put(ConnectionEntry.PARTITION_PATH_end.toString(), end);
        outputs.put(ConnectionEntry.PARTITION_PATH_end_in_front.toString(), endInFront);
        outputs.put(ConnectionEntry.PARTITION_PATH_end_behind.toString(), endBehind);
        outputs.put(ConnectionEntry.PARTITION_PATH_path_end.toString(), pathEnd);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[]{
                ConnectionEntry.PARTITION_PATH_path_iterator.toString(),
                ConnectionEntry.PARTITION_PATH_last_end.toString(),
                ConnectionEntry.PARTITION_PATH_last_end_in_front.toString(),
                ConnectionEntry.PARTITION_PATH_last_end_behind.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
}
