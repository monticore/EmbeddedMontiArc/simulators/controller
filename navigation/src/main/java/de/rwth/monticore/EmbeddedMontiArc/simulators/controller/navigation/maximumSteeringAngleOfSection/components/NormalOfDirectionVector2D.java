/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Calculate the normal of the direction vector
 *
 * used sub blocks : none
 *
 * used formula: normal vector cross product direction vector equals 0.
 *
 * Created by Christoph Grüne on 21.01.2017.
 */
public class NormalOfDirectionVector2D extends FunctionBlock{

    //Input Variables
    private RealVector directionVector;

    //Output Variables
    private RealVector normalVector;

    //Global Variables


    /**
     * Constructor for a InnerControlBlock object
     */
    public NormalOfDirectionVector2D() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        normalVector = calculateNormal();
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        directionVector = (RealVector) inputs.get(ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_direction_vector.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_normal_vector.toString(), normalVector);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_direction_vector.toString()};
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/

    /**
     * calculates a normalized normal of a direction vector in the x-y-dimension
     *
     * @return the normalized normal
     */
    private RealVector calculateNormal() {
        RealVector normal = new ArrayRealVector(new double[] {1, 1});

        normal.setEntry(1, (-1) * directionVector.getEntry(0)/directionVector.getEntry(1));

        return normal;
    }
}

