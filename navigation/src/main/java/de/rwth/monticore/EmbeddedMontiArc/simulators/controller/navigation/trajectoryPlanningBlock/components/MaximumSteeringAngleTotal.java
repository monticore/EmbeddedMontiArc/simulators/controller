/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.trajectoryPlanningBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.*;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.MaximumSteeringAngleOfSection;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Calculate the maximal steering angle for each different section
 *
 * used sub blocks: partitionPath
 *                  MaximumSteeringAngleOfSection
 *                  setMaximumSteeringAngleInVertex
 *
 * used formula : none
 *
 * Created by Christoph Grüne on 13.01.2017.
 */
public class MaximumSteeringAngleTotal extends FunctionBlock {

    //Input Variables
    private Iterator<Vertex> pathIterator;
    private List<Vertex> path;
    private Double wheelbase;

    //Output Variables
    private List<Vertex> detailedPath;

    //Global Variables
    private Boolean pathEnd = false;

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public MaximumSteeringAngleTotal() {initComponents();}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        Map<String, Object> outputs = new LinkedHashMap<>();
        while (!pathEnd) {
            outputs = functionBlockManagement.executeAllFunctionBlocks(timeDelta);
            pathEnd = (Boolean) outputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_path_end.toString());
        }
        detailedPath = (List<Vertex>) outputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_detailed_path.toString());
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        pathIterator = (Iterator<Vertex>) inputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_path_iterator.toString());
        path = (List<Vertex>) inputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_path.toString());
        wheelbase = (Double) inputs.get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_wheelbase.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_detailed_path.toString(), detailedPath);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_path_iterator.toString(),
                ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_path.toString(),
                ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_wheelbase.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/

    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {

        functionBlockManagement = new FunctionBlockManagement(this);

        FunctionBlock partitionPath = new PartitionPath(FunctionBlockParameter.DISTANCE_OF_MEASURING_POINTS.getParameter(), FunctionBlockParameter.LENGTH_OF_SECTIONS.getParameter());
        functionBlockManagement.addFunctionBlock(partitionPath);

        FunctionBlock maximumSteeringAngleOfSection = new MaximumSteeringAngleOfSection();
        functionBlockManagement.addFunctionBlock(maximumSteeringAngleOfSection);

        FunctionBlock setMaximumSteeringAngleInVertex = new SetMaximumSteeringAngleInVertex();
        functionBlockManagement.addFunctionBlock(setMaximumSteeringAngleInVertex);

        //connections to partitionPath
        functionBlockManagement.connect(this, this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_path_iterator.toString(), partitionPath, ConnectionEntry.PARTITION_PATH_path_iterator.toString());
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_end.toString(), partitionPath, ConnectionEntry.PARTITION_PATH_last_end.toString(), null);
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_end_in_front.toString(), partitionPath, ConnectionEntry.PARTITION_PATH_last_end_in_front.toString(), null);
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_end_behind.toString(), partitionPath, ConnectionEntry.PARTITION_PATH_last_end_behind.toString(), null);

        //connections to maximumSteeringAngleOfSection
        functionBlockManagement.connect(this, this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_wheelbase.toString(), maximumSteeringAngleOfSection, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_wheelbase.toString());
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_beginning.toString(), maximumSteeringAngleOfSection, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning.toString());
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_beginning_in_front.toString(), maximumSteeringAngleOfSection, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_in_front.toString());
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_beginning_behind.toString(), maximumSteeringAngleOfSection, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_behind.toString());
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_end.toString(), maximumSteeringAngleOfSection, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end.toString());
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_end_in_front.toString(), maximumSteeringAngleOfSection, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end_in_front.toString());
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_end_behind.toString(), maximumSteeringAngleOfSection, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end_behind.toString());

        //connections to setMaximumSteeringAngleInVertex
        functionBlockManagement.connect(this, this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_path.toString(), setMaximumSteeringAngleInVertex, ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_path.toString());
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_beginning_in_front.toString(), setMaximumSteeringAngleInVertex, ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_beginning.toString());
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_end_behind.toString(), setMaximumSteeringAngleInVertex, ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_end.toString());
        functionBlockManagement.connect(this, maximumSteeringAngleOfSection, ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_steering_angle.toString(), setMaximumSteeringAngleInVertex, ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_steering_angle.toString());

        //connections to this
        functionBlockManagement.connect(this, partitionPath, ConnectionEntry.PARTITION_PATH_path_end.toString(), this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_path_end.toString());
        functionBlockManagement.connect(this, setMaximumSteeringAngleInVertex, ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_path_with_angles.toString(), this, ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_detailed_path.toString());
    }
}
