/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.trajectoryPlanningBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph Grüne on 25.01.2017.
 */
@Deprecated
public class PathEstimation extends FunctionBlock {

    //Input Variables
    private List<Vertex> path;
    private Double estimatedDistance;

    //Output Variables
    private List<Vertex> estimatedPath;

    //Global Variables

    //Used Function Blocks

    /**
     * Constructor for a InnerControlBlock object
     */
    public PathEstimation() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        estimatedPath = calculateEstimatedPath();
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        //path = (List<Vertex>) inputs.get(ConnectionEntry.ESTIMATED_PATH.toString());
        //estimatedDistance = (Double) inputs.get(ConnectionEntry.ESTIMATED_DISTANCE.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        //outputs.put(ConnectionEntry.ESTIMATED_PATH.toString(), estimatedPath);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                //ConnectionEntry.ESTIMATED_PATH.toString(), ConnectionEntry.ESTIMATED_DISTANCE.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * function to calculate the needed distance
     */
    private List<Vertex> calculateEstimatedPath() {
        List<Vertex> result = null;

        return result;
    }

}
