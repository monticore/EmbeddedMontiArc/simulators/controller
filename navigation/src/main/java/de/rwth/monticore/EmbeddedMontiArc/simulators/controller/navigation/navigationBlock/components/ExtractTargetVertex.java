/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IControllerNode;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.exceptions.IllegalTargetNodeException;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Find the target vertex in the graph
 *
 * used sub blocks : none
 *
 * used formula: none
 *
 * Created by Christoph Grüne on 01.03.2017.
 */
public class ExtractTargetVertex extends FunctionBlock {

    //Input Arguments
    IControllerNode targetNode;
    Graph streetMapGraph;

    //Output Arguments
    Vertex targetVertex;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a VelocityLogic object
     */
    public ExtractTargetVertex() {
        initComponents();
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/
    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        try {
            targetVertex = streetMapGraph.getVertex(extractTargetVertex(targetNode));
        } catch(IllegalArgumentException e) {
            throw new IllegalTargetNodeException("The TargetNode is not in the graph: "+ e.toString());
        }
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        targetNode = (IControllerNode) inputs.get(ConnectionEntry.EXTRACT_TARGET_VERTEX_target_node.toString());
        streetMapGraph = (Graph) inputs.get(ConnectionEntry.EXTRACT_TARGET_VERTEX_graph.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.EXTRACT_TARGET_VERTEX_target_vertex.toString(), targetVertex);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.EXTRACT_TARGET_VERTEX_target_node.toString(),
                ConnectionEntry.EXTRACT_TARGET_VERTEX_graph.toString(),
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {}

    public Vertex extractTargetVertex(IControllerNode node) {
        RealVector pos = new ArrayRealVector(new Double[] {
                node.getPoint().getX(),
                node.getPoint().getY(),
                node.getPoint().getZ()
        });
        return new Vertex(node.getId(), node.getOsmId(), pos, 0.0);
    }
}
