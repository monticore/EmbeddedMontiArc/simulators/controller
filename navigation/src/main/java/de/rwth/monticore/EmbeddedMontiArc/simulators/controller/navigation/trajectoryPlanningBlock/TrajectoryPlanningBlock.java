/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.trajectoryPlanningBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.CreateIterator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.trajectoryPlanningBlock.components.MaximumSteeringAngleTotal;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Create a path, which all vertexes in it with maximal steering angle
 *
 *
 * used sub block: createPathIterator
 *                 MaximumSteeringAngleInTotal
 *
 *
 * used formula: none
 * Created by Christoph Grüne on 13.01.2017.
 */
public class TrajectoryPlanningBlock extends FunctionBlock {

    //Input Variables
    private List<Vertex> path;
    private Double constantWheelBase;

    //Output Variables
    private List<Vertex> detailedPath;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public TrajectoryPlanningBlock() {initComponents();}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        Map<String, Object> outputs = functionBlockManagement.executeAllFunctionBlocks(timeDelta);
        detailedPath = (List<Vertex>) outputs.get(ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_detailed_path.toString());
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        path = (List<Vertex>) inputs.get(ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_path.toString());
        constantWheelBase = (Double) inputs.get(ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_wheelbase.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_detailed_path.toString(), detailedPath);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_path.toString(),
                ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_wheelbase.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {

        functionBlockManagement = new FunctionBlockManagement(this);

        FunctionBlock createPathIterator = new CreateIterator();
        functionBlockManagement.addFunctionBlock(createPathIterator);

        FunctionBlock maximumSteeringAngleTotal = new MaximumSteeringAngleTotal();
        functionBlockManagement.addFunctionBlock(maximumSteeringAngleTotal);

        //connections to createPathIterator
        functionBlockManagement.connect(this, this, ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_path.toString(), createPathIterator, ConnectionEntry.CREATE_ITERATOR_list.toString());

        //connections to maximumSteeringAngleTotal
        functionBlockManagement.connect(this, createPathIterator, ConnectionEntry.CREATE_ITERATOR_iterator.toString(), maximumSteeringAngleTotal, ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_path_iterator.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_path.toString(), maximumSteeringAngleTotal, ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_path.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_wheelbase.toString(), maximumSteeringAngleTotal, ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_wheelbase.toString());

        //connections to this
        functionBlockManagement.connect(this, maximumSteeringAngleTotal, ConnectionEntry.MAXIMUM_STEERING_ANGLE_TOTAL_detailed_path.toString(), this, ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_detailed_path.toString());
    }
}
