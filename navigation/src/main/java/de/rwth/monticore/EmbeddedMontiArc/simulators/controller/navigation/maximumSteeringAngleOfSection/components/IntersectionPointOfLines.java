/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import org.apache.commons.math3.linear.*;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Calculate the intersection point of two direction vectors with the "beginning vector" respectively
 *
 * used sub blocks: none
 *
 * used formula : LUDecomposition decomposition
 *
 * Created by Christoph Grüne on 13.01.2017.
 */
public class IntersectionPointOfLines extends FunctionBlock {

    //Input Variables
    private RealVector positionVector1;
    private RealVector directionVector1;
    private RealVector positionVector2;
    private RealVector directionVector2;

    //Output Variables
    private RealVector intersectionPoint;
    private Boolean noIntersectionPoint;

    //Global Variables

    //Used Function Blocks

    /**
     * Constructor for a InnerControlBlock object
     */
    public IntersectionPointOfLines() {initComponents();}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        intersectionPoint = calculateIntersectionPoint();
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        positionVector1 = (RealVector) inputs.get(ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_1.toString());
        directionVector1 = (RealVector) inputs.get(ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_1.toString());
        positionVector2 = (RealVector) inputs.get(ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_2.toString());
        directionVector2 = (RealVector) inputs.get(ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_2.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.INTERSECTION_POINT_OF_LINES_intersection_point.toString(), intersectionPoint);
        outputs.put(ConnectionEntry.INTERSECTION_POINT_OF_LINES_no_intersection_point.toString(), noIntersectionPoint);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_1.toString(),
                ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_1.toString(),
                ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_2.toString(),
                ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_2.toString()};
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {

    }

    /**
     * calculates the intersection point of two lines of the form: positionVec + a * directionVec
     *
     * @return intersection point
     */
    private RealVector calculateIntersectionPoint() {
        RealMatrix coefficients;
        RealVector constants;
        RealVector solution;

        noIntersectionPoint = false;

        coefficients = new Array2DRowRealMatrix(2, 2);
        coefficients.setEntry(0, 0, (-1) * directionVector1.getEntry(0));
        coefficients.setEntry(0, 1, directionVector2.getEntry(0));
        coefficients.setEntry(1, 0, (-1) * directionVector1.getEntry(1));
        coefficients.setEntry(1, 1, directionVector2.getEntry(1));

        LUDecomposition decomposition = new LUDecomposition(coefficients);

        constants = new ArrayRealVector(new double[] {positionVector1.getEntry(0) - positionVector2.getEntry(0), positionVector1.getEntry(1) - positionVector2.getEntry(1)});

        DecompositionSolver solver = decomposition.getSolver();
        try {
            if(Double.isInfinite(directionVector1.getEntry(0)) || Double.isNaN(directionVector1.getEntry(0))
                    || Double.isInfinite(directionVector2.getEntry(0)) || Double.isNaN(directionVector2.getEntry(0))
                    || Double.isInfinite(directionVector1.getEntry(1)) || Double.isNaN(directionVector1.getEntry(1))
                    || Double.isInfinite(directionVector2.getEntry(1)) || Double.isNaN(directionVector2.getEntry(1))
                    ) {
                throw new SingularMatrixException();
            }
            solution = solver.solve(constants);
        } catch(SingularMatrixException e) {
            noIntersectionPoint = true;
            solution = new ArrayRealVector(2);
            solution.setEntry(0, Double.NaN);
            solution.setEntry(1, Double.NaN);
        }

        return positionVector1.add(directionVector1.mapMultiply(solution.getEntry(0)));
    }
}
