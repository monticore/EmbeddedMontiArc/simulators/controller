/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.trajectoryPlanningBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.PathGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockParameter;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph Grüne on 24.01.2017.
 */
public class FunctionBlockTestPartitionPath extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestPartitionPath(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestPartitionPath.class);
    }


    public void testPartitionPath_Cicle() {

        PathGenerator pathGenerator = new PathGenerator();

        Double distanceOfMeasuringPoints = FunctionBlockParameter.DISTANCE_OF_MEASURING_POINTS.getParameter();
        Double lengthOfSections = FunctionBlockParameter.LENGTH_OF_SECTIONS.getParameter();

        double result;
        for (double x = -20.0; x < 20.0; x += 2.5) {
            for (double y = -20.0; y < 20.0; y += 2.5) {
                for (double radius = 15.0; radius < 1000.0; radius *= 5.0) {
                    List<Vertex> path = pathGenerator.generateCircle(radius, x, y, 0);
                    executeTestCase(path, lengthOfSections, distanceOfMeasuringPoints);
                }
            }
        }

        assertTrue(true);
    }

    public void testPartitionPath_StraightLine() {

        PathGenerator pathGenerator = new PathGenerator();

        Double distanceToMeasuringPoints = 1.5;
        Double lengthOfSections = 10.0;

        for (double x = -20.0; x < 20.0; x += 0.1) {
            for (double y = -20.0; y < 20.0; y += 0.1) {
                for (double z = -20.0; z < 20.0; z += 5.0) {
                    List<Vertex> path = pathGenerator.generateStraightLine(100.0, x, y, new ArrayRealVector(new double[]{x * y * z, x * y}), 0);
                    executeTestCase(path, lengthOfSections, distanceToMeasuringPoints);
                }
            }
        }

        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/

    private void executeTestCase(List<Vertex> path, Double lengthOfSections, Double distanceOfMeasuringPoints) {

        FunctionBlock partitionPath = new PartitionPath(FunctionBlockParameter.DISTANCE_OF_MEASURING_POINTS.getParameter(), FunctionBlockParameter.LENGTH_OF_SECTIONS.getParameter());

        RealVector beginning = new ArrayRealVector(new double[]{100.0, 0.0, 0.0});
        RealVector beginningInFront = new ArrayRealVector(new double[]{100.0, 0.0, 0.0});
        RealVector beginningBehind = new ArrayRealVector(new double[]{100.0, 0.0, 0.0});
        RealVector end = new ArrayRealVector(new double[]{100.0, 0.0, 0.0});
        RealVector endInFront = new ArrayRealVector(new double[]{100.0, 0.0, 0.0});
        RealVector endBehind = new ArrayRealVector(new double[]{100.0, 0.0, 0.0});

        RealVector res_beginning;
        RealVector res_beginningInFront;
        RealVector res_beginningBehind;
        RealVector res_end;
        RealVector res_endInFront;
        RealVector res_endBehind;

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        Iterator<Vertex> pathIterator = path.iterator();
        input.put(ConnectionEntry.PARTITION_PATH_path_iterator.toString(), pathIterator);

        beginningInFront = new ArrayRealVector(new double[]{0.0, 0.0, 0.0});

        RealVector last = beginningInFront;
        double distance = 0.0;
        int state = 0;

        int counter = 0;

        for (Vertex current : path) {
            if (0 == state) {
                last = current.getPosition();
                beginningInFront = current.getPosition();
                state = 1;
            } else if (1 == state && distance >= distanceOfMeasuringPoints) {
                beginning = current.getPosition();
                state = 2;
            } else if (2 == state && distance >= 2 * distanceOfMeasuringPoints) {
                beginningBehind = current.getPosition();
                state = 3;
            } else if (3 == state && distance >= lengthOfSections) {
                endInFront = current.getPosition();
                state = 4;
            } else if (4 == state && distance >= lengthOfSections + distanceOfMeasuringPoints) {
                end = current.getPosition();
                state = 5;
            } else if (5 == state && distance >= lengthOfSections + 2 * distanceOfMeasuringPoints) {
                endBehind = current.getPosition();
                if(counter == 1) {
                    input.put(ConnectionEntry.PARTITION_PATH_last_end.toString(), new Vertex(0L, 0L, end, 0.0));
                    input.put(ConnectionEntry.PARTITION_PATH_last_end_in_front.toString(), new Vertex(0L, 0L, endInFront, 0.0));
                    input.put(ConnectionEntry.PARTITION_PATH_last_end_behind.toString(), new Vertex(0L, 0L, endBehind, 0.0));
                } else {
                    counter = 1;
                }

                state = 2;

                boolean res = executeBlockAndTest(partitionPath, input, beginning, beginningInFront, beginningBehind, end, endInFront, endBehind);
                assertFalse(!res);
            }

            distance = 2 * distanceOfMeasuringPoints;

            distance += current.getPosition().getDistance(last);
            last = current.getPosition();
        }
    }

    private boolean executeBlockAndTest(FunctionBlock partitionPath,
                                        Map<String, Object> input,
                                        RealVector beginning,
                                        RealVector beginningInFront,
                                        RealVector beginningBehind,
                                        RealVector end,
                                        RealVector endInFront,
                                        RealVector endBehind
    ) {
        partitionPath.setInputs(input);
        partitionPath.execute(1);
        RealVector res_beginning = ((Vertex) partitionPath.getOutputs().get(ConnectionEntry.PARTITION_PATH_beginning.toString())).getPosition();
        RealVector res_beginningInFront = ((Vertex) partitionPath.getOutputs().get(ConnectionEntry.PARTITION_PATH_beginning_in_front.toString())).getPosition();
        RealVector res_beginningBehind = ((Vertex) partitionPath.getOutputs().get(ConnectionEntry.PARTITION_PATH_beginning_behind.toString())).getPosition();
        RealVector res_end = ((Vertex) partitionPath.getOutputs().get(ConnectionEntry.PARTITION_PATH_end.toString())).getPosition();
        RealVector res_endInFront = ((Vertex) partitionPath.getOutputs().get(ConnectionEntry.PARTITION_PATH_end_in_front.toString())).getPosition();
        RealVector res_endBehind = ((Vertex) partitionPath.getOutputs().get(ConnectionEntry.PARTITION_PATH_end_behind.toString())).getPosition();

        double epsilon = 0.00001;
        if (res_beginning.getEntry(0) < beginning.getEntry(0) - epsilon || res_beginning.getEntry(0) > beginning.getEntry(0) + epsilon
                || res_beginning.getEntry(1) < beginning.getEntry(1) - epsilon || res_beginning.getEntry(1) > beginning.getEntry(1) + epsilon
                || res_beginning.getEntry(2) < beginning.getEntry(2) - epsilon || res_beginning.getEntry(2) > beginning.getEntry(2) + epsilon
                ) {
            return false;
        }
        if (res_beginningInFront.getEntry(0) < beginningInFront.getEntry(0) - epsilon || res_beginningInFront.getEntry(0) > beginningInFront.getEntry(0) + epsilon
                || res_beginningInFront.getEntry(1) < beginningInFront.getEntry(1) - epsilon || res_beginningInFront.getEntry(1) > beginningInFront.getEntry(1) + epsilon
                || res_beginningInFront.getEntry(2) < beginningInFront.getEntry(2) - epsilon || res_beginningInFront.getEntry(2) > beginningInFront.getEntry(2) + epsilon
                ) {
            return false;
        }
        if (res_beginningBehind.getEntry(0) < beginningBehind.getEntry(0) - epsilon || res_beginningBehind.getEntry(0) > beginningBehind.getEntry(0) + epsilon
                || res_beginningBehind.getEntry(1) < beginningBehind.getEntry(1) - epsilon || res_beginningBehind.getEntry(1) > beginningBehind.getEntry(1) + epsilon
                || res_beginningBehind.getEntry(2) < beginningBehind.getEntry(2) - epsilon || res_beginningBehind.getEntry(2) > beginningBehind.getEntry(2) + epsilon
                ) {
            return false;
        }
        if (res_end.getEntry(0) < end.getEntry(0) - epsilon || res_end.getEntry(0) > end.getEntry(0) + epsilon
                || res_end.getEntry(1) < end.getEntry(1) - epsilon || res_end.getEntry(1) > end.getEntry(1) + epsilon
                || res_end.getEntry(2) < end.getEntry(2) - epsilon || res_end.getEntry(2) > end.getEntry(2) + epsilon
                ) {
            return false;
        }
        if (res_endInFront.getEntry(0) < endInFront.getEntry(0) - epsilon || res_endInFront.getEntry(0) > endInFront.getEntry(0) + epsilon
                || res_endInFront.getEntry(1) < endInFront.getEntry(1) - epsilon || res_endInFront.getEntry(1) > endInFront.getEntry(1) + epsilon
                || res_endInFront.getEntry(2) < endInFront.getEntry(2) - epsilon || res_endInFront.getEntry(2) > endInFront.getEntry(2) + epsilon
                ) {
            return false;
        }
        if (res_endBehind.getEntry(0) < endBehind.getEntry(0) - epsilon || res_endBehind.getEntry(0) > endBehind.getEntry(0) + epsilon
                || res_endBehind.getEntry(1) < endBehind.getEntry(1) - epsilon || res_endBehind.getEntry(1) > endBehind.getEntry(1) + epsilon
                || res_endBehind.getEntry(2) < endBehind.getEntry(2) - epsilon || res_endBehind.getEntry(1) > endBehind.getEntry(1) + epsilon
                ) {
            return false;
        }

        return true;
    }
}

