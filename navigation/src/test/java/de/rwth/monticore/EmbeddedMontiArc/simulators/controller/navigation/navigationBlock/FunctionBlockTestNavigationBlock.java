/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.GraphGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.NavigationEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IAdjacency;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IControllerNode;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph Grüne on 28.02.2017.
 */
public class FunctionBlockTestNavigationBlock extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestNavigationBlock(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestNavigationBlock.class);
    }

    public void testNavigationBlock_1() {
        executeTest(1);
    }

    public void testNavigationBlock_2() {
        executeTest(2);
    }

    /*****************************
     * helper functions          *
     *****************************/
    private void executeTest(int exampleIndex) {
        GraphGenerator graphGenerator = new GraphGenerator();
        List<IAdjacency> adjacencies = graphGenerator.getIAdjacencyList(exampleIndex);
        Graph graph = graphGenerator.getGraph(exampleIndex);

        for (Vertex vStart : graph.getVertices()) {
            for (Vertex vEnd : graph.getVertices()) {
                List<Vertex> detailedPathWithMaximalSteeringAngle = graphGenerator.getDetailedPathWithMaximalSteeringAngle(exampleIndex, vStart, vEnd);

                FunctionBlock navigationBlock = new NavigationBlock();

                Map<String, Object> inputs = new LinkedHashMap<>();
                inputs.put(NavigationEntry.MAP_ADJACENCY_LIST.toString(), adjacencies);
                inputs.put(NavigationEntry.GPS_COORDINATES.toString(), vStart.getPosition());
                IControllerNode node = null;
                for (IAdjacency iAdjacency : adjacencies) {
                    if (iAdjacency.getNode1().getId() == vEnd.getId().longValue()) {
                        node = iAdjacency.getNode1();
                    } else if (iAdjacency.getNode2().getId() == vEnd.getId().longValue()) {
                        node = iAdjacency.getNode2();
                    }
                }
                inputs.put(NavigationEntry.TARGET_NODE.toString(), node);
                inputs.put(BusEntry.CONSTANT_WHEELBASE.toString(), 4.0);


                navigationBlock.setInputs(inputs);
                navigationBlock.execute(1);
                List<Vertex> result = (List<Vertex>) navigationBlock.getOutputs().get(NavigationEntry.DETAILED_PATH_WITH_MAX_STEERING_ANGLE.toString());

                if (result.size() != detailedPathWithMaximalSteeringAngle.size()) {
                    assertFalse(true);
                }
                if (result.size() != detailedPathWithMaximalSteeringAngle.size()) {
                    assertFalse(true);
                }
                for (int i = 0; i < result.size(); ++i) {
                    if (!result.get(i).getId().equals(detailedPathWithMaximalSteeringAngle.get(i).getId())) {
                        assertFalse(true);
                    }
                    if (!result.get(i).getOsmId().equals(detailedPathWithMaximalSteeringAngle.get(i).getOsmId())) {
                        assertFalse(true);
                    }
                    if (!result.get(i).getPosition().equals(detailedPathWithMaximalSteeringAngle.get(i).getPosition())) {
                        assertFalse(true);
                    }
                    if(detailedPathWithMaximalSteeringAngle.size() >= 6) {
                        if (!(result.get(i).getMaximumSteeringAngle() < detailedPathWithMaximalSteeringAngle.get(i).getMaximumSteeringAngle() - 0.000001)
                                && (result.get(i).getMaximumSteeringAngle() > detailedPathWithMaximalSteeringAngle.get(i).getMaximumSteeringAngle() + 0.000001)) {
                            assertFalse(true);
                        }
                    } else {
                        if (result.get(i).getMaximumSteeringAngle().doubleValue() != 0.0) {
                            assertFalse(true);
                        }
                    }
                }
            }
        }

        assertTrue(true);
    }
}
