/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * test for the function block SubtractVectors
 *
 * Created by Christoph Grüne on 25.01.2017.
 */
public class FunctionBlockTestSubtractVectors extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestSubtractVectors(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestSubtractVectors.class);
    }


    /**
     * tests if all inputs (see below) are handled correctly:
     *      (x1, x2) = (57464654.464, 454897.7898)
     *      (y1, y2) = (1.5, 1.8)
     */
    public void testMaximumSteeringAngleForRadius() {

        FunctionBlock subtractVectors = new SubtractVectors();

        RealVector result, compareResult;

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        RealVector minuendVector = new ArrayRealVector(new double[] {57464654.464, 454897.7898});
        RealVector subtrahendVector = new ArrayRealVector(new double[] {1.5, 1.8});


        input.put(ConnectionEntry.SUBTRACT_VECTORS_minuend_vector.toString(), minuendVector);
        input.put(ConnectionEntry.SUBTRACT_VECTORS_subtrahend_vector.toString(), subtrahendVector);

        subtractVectors.setInputs(input);
        subtractVectors.execute(1);
        result = (RealVector) subtractVectors.getOutputs().get(ConnectionEntry.SUBTRACT_VECTORS_subtracted_vector.toString());

        compareResult = minuendVector.subtract(subtrahendVector);

        if (result.getDimension() != compareResult.getDimension() || result.getEntry(0) != compareResult.getEntry(0) || result.getEntry(1) != compareResult.getEntry(1)) {
            assertFalse(true);
        }
        assertTrue(true);
    }


    /*****************************
     * helper functions          *
     *****************************/

}

