/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.GraphGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Christoph Grüne on 05.03.2017.
 */
public class FunctionBlockTestLocateNearestVertexInGraph extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestLocateNearestVertexInGraph(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestLocateNearestVertexInGraph.class);
    }

    public void testLocateNearestVertex_1() {
        executeTest(1);
    }

    public void testLocateNearestVertex_2() {
        executeTest(2);
    }


    /*****************************
     * helper functions          *
     *****************************/
    private void executeTest(int exampleIndex) {
        GraphGenerator graphGenerator = new GraphGenerator();
        Graph graph = graphGenerator.getGraph(exampleIndex);

        FunctionBlock locateNearestVertex = new LocateNearestVertexInGraph();

        for(double x = -20.0; x < 20.0; x += 0.1) {
            for(double y = -20.0; y < 20.0; y += 0.1) {
                RealVector gps = new ArrayRealVector(new Double[] {x, y, 0.0});

                Map<String, Object> inputs = new LinkedHashMap<>();
                inputs.put(ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_GRAPH_gps_coordinates.toString(), gps);
                inputs.put(ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_GRAPH_graph.toString(), graph);

                Vertex nearestVertex = graph.getVertices().get(0);
                for(Vertex vertex : graph.getVertices()) {
                    if(vertex.getPosition().getDistance(gps) < nearestVertex.getPosition().getDistance(gps)) {
                        nearestVertex = vertex;
                    }
                }

                locateNearestVertex.setInputs(inputs);
                locateNearestVertex.execute(1);
                Vertex result = (Vertex) locateNearestVertex.getOutputs().get(ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_GRAPH_nearest_vertex.toString());

                if(!result.equals(nearestVertex)) {
                    assertFalse(true);
                }
            }
        }

        assertTrue(true);
    }
}
