/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.ControllerNode;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IControllerNode;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.utils.Point3D;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Edge;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph Grüne on 01.03.2017.
 */
public class FunctionBlockTestExtractTargetVertex extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestExtractTargetVertex(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestExtractTargetVertex.class);
    }

    public void testExtractTargetVertex() {
        FunctionBlock extractTargetVertex = new ExtractTargetVertex();

        for(double x = -20.0; x < 20.0; x += 1.312) {
            for(double y = -20.0; y < 20.0; y += 1.365) {
                for(double z = -20.0; z < 20.0; z += 0.222) {
                    for(long i = -20; i < 20; i += 3) {
                        Point3D point = new Point3D(x, y, z);
                        RealVector vector = new ArrayRealVector(new Double[] {x, y, z});
                        Long osmId = i;

                        IControllerNode node = new ControllerNode(point, osmId);
                        List<Vertex> vertices = new LinkedList<>();
                        vertices.add(new Vertex(node.getId(), node.getOsmId(), vector, 0.0));
                        Graph graph = new Graph(vertices, new LinkedHashMap<Vertex, Map<Vertex, Edge>>(), false);

                        Map<String, Object> inputs = new LinkedHashMap<>();
                        inputs.put(ConnectionEntry.EXTRACT_TARGET_VERTEX_target_node.toString(), node);
                        inputs.put(ConnectionEntry.EXTRACT_TARGET_VERTEX_graph.toString(), graph);

                        extractTargetVertex.setInputs(inputs);
                        extractTargetVertex.execute(1);
                        Map<String, Object> outputs = extractTargetVertex.getOutputs();

                        Vertex vertex = (Vertex) outputs.get(ConnectionEntry.EXTRACT_TARGET_VERTEX_target_vertex.toString());

                        if(vertex.getOsmId() != i || !vertex.getPosition().equals(vector) || vertex.getMaximumSteeringAngle() != 0.0) {
                            assertFalse(true);
                        }
                    }
                }
            }
        }


        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/
}
