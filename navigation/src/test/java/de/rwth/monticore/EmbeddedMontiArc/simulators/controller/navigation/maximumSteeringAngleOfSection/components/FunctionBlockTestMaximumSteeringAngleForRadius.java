/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * this tests the function block MaximumSteeringAngleForRadius
 *
 * Created by Christoph Grüne on 25.01.2017.
 */
public class FunctionBlockTestMaximumSteeringAngleForRadius extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestMaximumSteeringAngleForRadius(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestMaximumSteeringAngleForRadius.class);
    }


    /**
     * tests if all inputs (see below) are handled correctly:
     *      constantWheelbase in [0.1, 25]
     *      radius in [0, 20000], whereby radius can be zero. Then, it is no curve but a straight line.
     */
    public void testMaximumSteeringAngleForRadius() {

        FunctionBlock maximumSteeringAngleForRadius = new MaximumSteeringAngleForRadius();

        Double result, compareResult;

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        for(double constantWheelbase = 0.1; constantWheelbase < 25.0; constantWheelbase += 0.1) {
            for(double radius = 0.0; radius < 20000.0; radius += 42.9) {

                input.put(BusEntry.CONSTANT_WHEELBASE.toString(), constantWheelbase);
                input.put(ConnectionEntry.MAXIMUM_STEERING_ANGLE_FOR_RADIUS_radius.toString(), radius);

                maximumSteeringAngleForRadius.setInputs(input);
                maximumSteeringAngleForRadius.execute(1);
                result = (Double) maximumSteeringAngleForRadius.getOutputs().get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_FOR_RADIUS_steering_angle.toString());

                if (radius == 0.0) {
                    compareResult = 0.0;
                } else {
                    compareResult = (Double) (Math.asin(constantWheelbase / radius));
                }

                if (result.doubleValue() != compareResult.doubleValue()) {
                    assertFalse(true);
                }
            }
        }
        assertTrue(true);
    }


    /*****************************
     * helper functions          *
     *****************************/

}

