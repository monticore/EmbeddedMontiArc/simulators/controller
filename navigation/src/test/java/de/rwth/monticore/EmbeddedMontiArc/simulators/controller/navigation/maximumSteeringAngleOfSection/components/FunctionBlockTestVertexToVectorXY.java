/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * test for the functionBlock VertexToVectorXY
 *
 * Created by Christoph Grüne on 25.01.2017.
 */
public class FunctionBlockTestVertexToVectorXY extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestVertexToVectorXY(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestVertexToVectorXY.class);
    }

    /**
     * tests if all inputs (see below) are handled correctly:
     *      vertex = (0L, 0L, vectorXYZ, 0.0), where vectorXYZ = (57464654.464, 454897.7898, 45649873854658.56469784545)
     */
    public void testMaximumSteeringAngleForRadius() {

        FunctionBlock vertexToVectorXY = new VertexToVectorXY();

        RealVector result, compareResult;

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        RealVector vectorXYZ = new ArrayRealVector(new double[] {57464654.464, 454897.7898, 45649873854658.56469784545});
        Vertex vertex = new Vertex(0L, 0L, vectorXYZ, 0.0);

        input.put(ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vertex.toString(), vertex);

        vertexToVectorXY.setInputs(input);
        vertexToVectorXY.execute(1);
        result = (RealVector) vertexToVectorXY.getOutputs().get(ConnectionEntry.VERTEX_TO_VECTOR_XYZ_vector_xy.toString());

        compareResult = new ArrayRealVector(new double[] {vectorXYZ.getEntry(0), vectorXYZ.getEntry(1)});

        if (result.getDimension()!= 2 || result.getDimension() != compareResult.getDimension() || result.getEntry(0) != compareResult.getEntry(0) || result.getEntry(1) != compareResult.getEntry(1)) {
            assertFalse(true);
        }
        assertTrue(true);
    }


    /*****************************
     * helper functions          *
     *****************************/

}

