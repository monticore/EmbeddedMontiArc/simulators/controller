/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * this tests the function block IntersectionPointOfLines
 *
 * Created by Christoph Grüne on 25.01.2017.
 */
public class FunctionBlockTestIntersectionPointOfLines extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestIntersectionPointOfLines(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestIntersectionPointOfLines.class);
    }


    /**
     * this is a test for a "complex" example. It is an example, which was precalculated per hand.
     * It tests the lines g(x) = (25, 2.8) + x * (53, 42) and h(x) = (42, 56) + x * (88.8, 77.7)
     */
    public void testIntersectionPointOfLines_ComplexExample() {
        RealVector positionVector1 = new ArrayRealVector(new double[]{25.0, 2.8});
        RealVector directionVector1 = new ArrayRealVector(new double[]{53.0, 42.0});
        RealVector positionVector2 = new ArrayRealVector(new double[]{42.0, 56.0});
        RealVector directionVector2 = new ArrayRealVector(new double[]{88.8, 77.7});

        RealVector result = executeTestCase(positionVector1, directionVector1, positionVector2, directionVector2);

        if (result.getDimension() != 2 || (result.getEntry(0) <= (-10982 / 25.0) - 0.05 || result.getEntry(0) >= (-10982 / 25.0) + 0.05) || (result.getEntry(1) <= (-9128 / 25.0) - 0.05 || result.getEntry(1) >= (-9128 / 25.0) + 0.05)) {
            assertFalse(true);
        }

        assertTrue(true);
    }

    /**
     * this is a test for equal lines.
     * It tests, if equality of the lines g(x) = (4, 2.8) + x * (51, 69.88) and h(x) = (55, 72.68) + x * (25.5, 34.94) is detected.
     */
    public void testIntersectionPointOfLines_EqualLines() {
        RealVector positionVector1 = new ArrayRealVector(new double[] {4.0, 2.8});
        RealVector directionVector1 = new ArrayRealVector(new double[] {51.0, 69.88});
        RealVector positionVector2 = new ArrayRealVector(new double[] {55.0, 72.68});
        RealVector directionVector2 = new ArrayRealVector(new double[] {25.5, 34.94});

        RealVector result = executeTestCase(positionVector1, directionVector1, positionVector2, directionVector2);

        if(result.getDimension() != 2 || !Double.isNaN(result.getEntry(0)) || !Double.isNaN(result.getEntry(1))) {
            assertFalse(true);
        }

        assertTrue(true);
    }

    /**
     * this is a test for an example, that has an intersection in the origin.
     * It tests the lines g(x) = (0, 0) + x * (1, 0) and h(x) = (0, 0) + x * (0, 1)
     */
    public void testIntersectionPointOfLines_IntersectionAtOrigin() {
        RealVector positionVector1 = new ArrayRealVector(new double[] {0.0, 0.0});
        RealVector directionVector1 = new ArrayRealVector(new double[] {1.0, 0.0});
        RealVector positionVector2 = new ArrayRealVector(new double[] {0.0, 0.0});
        RealVector directionVector2 = new ArrayRealVector(new double[] {0.0, 1.0});

        RealVector result = executeTestCase(positionVector1, directionVector1, positionVector2, directionVector2);

        if(result.getDimension() != 2 || result.getEntry(0) != 0.0 || result.getEntry(1) != 0.0) {
            assertFalse(true);
        }

        assertTrue(true);
    }

    /**
     * this is a test for an example, that provide parallel lines.
     * It tests the lines g(x) = (20, 5.8) + x * (1.8, 5.2) and h(x) = (42, 56) + x * (2.7, 7.8)
     */
    public void testIntersectionPointOfLines_ParallelLines() {
        RealVector positionVector1 = new ArrayRealVector(new double[] {20.0, 5.8});
        RealVector directionVector1 = new ArrayRealVector(new double[] {1.8, 5.2});
        RealVector positionVector2 = new ArrayRealVector(new double[] {42.0, 56.0});
        RealVector directionVector2 = new ArrayRealVector(new double[] {2.7, 7.8});

        RealVector result = executeTestCase(positionVector1, directionVector1, positionVector2, directionVector2);

        if(result.getDimension() != 2 || !Double.isNaN(result.getEntry(0)) || !Double.isNaN(result.getEntry(1))) {
            assertFalse(true);
        }

        assertTrue(true);
    }

    /**
     * this is a test for a "simple" example. It is an example, which was precalculated per hand.
     * It tests the lines g(x) = (1, 0) + x * (1, 0) and h(x) = (1, 1) + x * (0, 5)
     */
    public void testIntersectionPointOfLines_SimpleExample() {
        RealVector positionVector1 = new ArrayRealVector(new double[] {1.0, 0.0});
        RealVector directionVector1 = new ArrayRealVector(new double[] {1.0, 0.0});
        RealVector positionVector2 = new ArrayRealVector(new double[] {1.0, 1.0});
        RealVector directionVector2 = new ArrayRealVector(new double[] {0.0, 5.0});

        RealVector result = executeTestCase(positionVector1, directionVector1, positionVector2, directionVector2);

        if(result.getDimension() != 2 || result.getEntry(0) != 1.0 || result.getEntry(1) != 0.0) {
            assertFalse(true);
        }

        assertTrue(true);
    }


    /*****************************
     * helper functions          *
     *****************************/
    /**
     * this function sets inputs, executes the function block, and returns the outputs
     *
     * @param positionVector1 the position vector of the first line
     * @param directionVector1 the direction vector of the first lin
     * @param positionVector2 the position vector of the second line
     * @param directionVector2 the direction vector of the second line
     * @return the output(s) of the executed function block
     */
    private RealVector executeTestCase(RealVector positionVector1, RealVector directionVector1, RealVector positionVector2, RealVector directionVector2) {

        FunctionBlock intersectionPointOfLines = new IntersectionPointOfLines();

        RealVector result;

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        input.put(ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_1.toString(), positionVector1);
        input.put(ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_1.toString(), directionVector1);
        input.put(ConnectionEntry.INTERSECTION_POINT_OF_LINES_position_vector_2.toString(), positionVector2);
        input.put(ConnectionEntry.INTERSECTION_POINT_OF_LINES_direction_vector_2.toString(), directionVector2);

        intersectionPointOfLines.setInputs(input);
        intersectionPointOfLines.execute(1);
        result = (RealVector) intersectionPointOfLines.getOutputs().get(ConnectionEntry.INTERSECTION_POINT_OF_LINES_intersection_point.toString());

        return result;
    }
}
