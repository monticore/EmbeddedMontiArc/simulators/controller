/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * this is a test for the function block MinimumDistanceOfVectors
 *
 * Created by Christoph Grüne on 25.01.2017.
 */
public class FunctionBlockTestMinimumDistanceOfVectors extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestMinimumDistanceOfVectors(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestMinimumDistanceOfVectors.class);
    }


    /**
     * this tests, if the function block is working properly for the following input
     * distance between (57464654.464, 454897.7898) and (1.5, 1.8) and
     * distance between (569874123.0231456987, 45646464.4679886) and (5.89, 89.87)
     */
    public void testMaximumSteeringAngleForRadius() {

        FunctionBlock minimumDistanceOfVectors = new MinimumDistanceOfVectors();

        Double result, compareResult;

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        RealVector minimumDistanceVector1_1 = new ArrayRealVector(new double[] {57464654.464, 454897.7898});
        RealVector minimumDistanceVector1_2 = new ArrayRealVector(new double[] {1.5, 1.8});
        RealVector minimumDistanceVector2_1 = new ArrayRealVector(new double[] {569874123.0231456987, 45646464.4679886});
        RealVector minimumDistanceVector2_2 = new ArrayRealVector(new double[] {5.89, 89.87});


        input.put(ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_1_1.toString(), minimumDistanceVector1_1);
        input.put(ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_1_2.toString(), minimumDistanceVector1_2);
        input.put(ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_2_1.toString(), minimumDistanceVector2_1);
        input.put(ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_vector_2_2.toString(), minimumDistanceVector2_2);

        minimumDistanceOfVectors.setInputs(input);
        minimumDistanceOfVectors.execute(1);
        result = (Double) minimumDistanceOfVectors.getOutputs().get(ConnectionEntry.MINIMUM_DISTANCE_OF_VECTORS_minimum_distance.toString());

        compareResult = (Double) Math.min(minimumDistanceVector1_1.getDistance(minimumDistanceVector1_2), minimumDistanceVector2_1.getDistance(minimumDistanceVector2_2));

        if (result.doubleValue() != compareResult.doubleValue()) {
            assertFalse(true);
        }
        assertTrue(true);
    }


    /*****************************
     * helper functions          *
     *****************************/

}

