/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.PathGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph Grüne on 21.01.2017.
 */
public class FunctionBlockTestMaximumSteeringAngleOfSection extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestMaximumSteeringAngleOfSection(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestMaximumSteeringAngleOfSection.class);
    }


    /**
     * tests if all inputs (see below) are handled correctly:
     *      several circular paths with
     *          radius in [15, 5000]
     *          middle point = (0, 0)
     *      wheelbase = 4
     */
    public void testMaximumSteeringAngleOfSection_Circle() {

        PathGenerator pathGenerator = new PathGenerator();

        Double angle;
        Double result;

        Double constantWheelbase = 4.0;

        for(double radius = 15.0; radius < 5000.0; radius *= 5) {

            List<Vertex> path = pathGenerator.generateCircle(radius, 0.0, 0.0, 0);

            angle = Math.asin(constantWheelbase / radius);

            result = executeTestCase(path, constantWheelbase);

            if (result > angle + 0.005 || result < angle - 0.005 || Double.isNaN(result) || Double.isInfinite(result)) {
                assertFalse(true);
            }
        }

        assertTrue(true);
    }

    /**
     * tests if all inputs (see below) are handled correctly:
     *      several straight line paths with
     *          length = 100
     *          start vector in [-20, 20]^2
     *          direction vector in {1} x [-400, 400]
     *      wheelbase = 1
     */
    public void testMaximumSteeringAngleOfSection_StraightLine() {

        PathGenerator pathGenerator = new PathGenerator();

        Double constantWheelbase = 1.0;

        double result;
        for(double x = -20.0; x < 20.0; x += 0.1) {
            for(double y = -20.0; y < 20.0; y += 0.1) {
                for(double z = -20.0; z < 20.0; z += 5.0) {
                    List<Vertex> path = pathGenerator.generateStraightLine(100.0, x, y, new ArrayRealVector(new double[]{1.0, x * y}), 0);

                    result = executeTestCase(path, constantWheelbase);

                    if (result < 0.0 - 0.00000000001 || result > 0.0 + 0.00000000001 || Double.isNaN(result) || Double.isInfinite(result)) {
                        assertFalse(true);
                    }
                }
            }
        }

        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/

    /**
     * this function sets inputs, executes the function block, and returns the outputs
     *
     * @param path the path
     * @param constantWheelbase the wheelbase of the car
     * @return the output(s) of the executed function block
     */
    private Double executeTestCase(List<Vertex> path, Double constantWheelbase) {

        FunctionBlock maximumSteeringAngleOfSection = new MaximumSteeringAngleOfSection();

        RealVector beginning = new ArrayRealVector(new double[] {100.0, 0.0, 0.0});
        RealVector beginningInFront = new ArrayRealVector(new double[] {100.0, 0.0, 0.0});
        RealVector beginningBehind = new ArrayRealVector(new double[] {100.0, 0.0, 0.0});
        RealVector end = new ArrayRealVector(new double[] {100.0, 0.0, 0.0});
        RealVector endInFront = new ArrayRealVector(new double[] {100.0, 0.0, 0.0});
        RealVector endBehind = new ArrayRealVector(new double[] {100.0, 0.0, 0.0});

        Double result;

        beginningInFront = path.iterator().next().getPosition();

        RealVector last = beginningInFront;

        int state = 0;

        double distance = 0.0;
        for (Vertex vec : path) {
            if (distance >= 1.5 && state == 0) {
                beginning = vec.getPosition();
                state++;
            } else if (distance >= 3.0 && state == 1) {
                beginningBehind = vec.getPosition();
                state++;
            } else if (distance >= 10.0 && state == 2) {
                endInFront = vec.getPosition();
                state++;
            } else if (distance >= 11.5 && state == 3) {
                end = vec.getPosition();
                state++;
            } else if (distance >= 13.0 && state == 4) {
                endBehind = vec.getPosition();
                break;
            }
            distance += vec.getPosition().getDistance(last);
            last = vec.getPosition();
        }

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        input.put(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_wheelbase.toString(), constantWheelbase);
        input.put(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning.toString(), new Vertex(0L, 0L, beginning, 0.0));
        input.put(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_in_front.toString(), new Vertex(0L, 0L, beginningInFront, 0.0));
        input.put(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_behind.toString(), new Vertex(0L, 0L, beginningBehind, 0.0));
        input.put(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end.toString(), new Vertex(0L, 0L, end, 0.0));
        input.put(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end_in_front.toString(), new Vertex(0L, 0L, endInFront, 0.0));
        input.put(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_end_behind.toString(), new Vertex(0L, 0L, endBehind, 0.0));

        maximumSteeringAngleOfSection.setInputs(input);
        maximumSteeringAngleOfSection.execute(1);
        result = (Double) maximumSteeringAngleOfSection.getOutputs().get(ConnectionEntry.MAXIMUM_STEERING_ANGLE_OF_SECTION_steering_angle.toString());

        return result;
    }
}

