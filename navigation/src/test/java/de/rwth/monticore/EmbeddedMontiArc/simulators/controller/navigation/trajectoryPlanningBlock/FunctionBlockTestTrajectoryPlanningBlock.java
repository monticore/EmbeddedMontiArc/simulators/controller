/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.trajectoryPlanningBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.PathGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph Grüne on 25.01.2017.
 */
public class FunctionBlockTestTrajectoryPlanningBlock extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestTrajectoryPlanningBlock(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestTrajectoryPlanningBlock.class);
    }

    public void testTrajectoryPlanningBlock_Circle() {

        PathGenerator pathGenerator = new PathGenerator();

        List<Vertex> result;

        Double constantWheelbase = 4.0;
        Double distanceOfMeasuringPoints = 2.0;

        for (double radius = 15.0; radius < 5000.0; radius *= 5) {

            List<Vertex> path = pathGenerator.generateCircle(radius, 0.0, 0.0, 0);

            Double compareResult = calculateAngle(radius, constantWheelbase);

            for(Vertex vertex : path) {
                vertex.setMaximumSteeringAngle(compareResult);
            }

            result = executeTestCase(path, constantWheelbase, 10.0, distanceOfMeasuringPoints, Surface.Asphalt);

            if(result.size() != path.size()) {
                assertFalse(true);
            }
            for(int i = 0; i < result.size(); ++i) {
                if (!result.get(i).equals(path.get(i))) {
                    assertFalse(true);
                }
            }
        }

        assertTrue(true);
    }

    public void testTrajectoryPlanningBlock_StraightLine() {

        PathGenerator pathGenerator = new PathGenerator();

        Double constantWheelbase = 4.0;
        Double distanceOfMeasuringPoints = 2.0;

        List<Vertex> result;

        for (double x = -20.0; x < 20.0; x += 0.5) {
            for (double y = -20.0; y < 20.0; y += 0.5) {
                for (double z = -20.0; z < 20.0; z += 4.5) {
                    List<Vertex> path = pathGenerator.generateStraightLine(100.0, x, y, new ArrayRealVector(new double[]{z * x * y, x * y + 0.1}), 0);

                    Double compareResult = 0.0;

                    for(Vertex vertex : path) {
                        vertex.setMaximumSteeringAngle(compareResult);
                    }

                    result = executeTestCase(path, constantWheelbase, 10.0, distanceOfMeasuringPoints, Surface.Asphalt);

                    if(result.size() != path.size()) {
                        assertFalse(true);
                    }
                    for(int i = 0; i < result.size(); ++i) {
                        if (!result.get(i).equals(path.get(i))) {
                            assertFalse(true);
                        }
                    }
                }
            }
        }

        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/

    private List<Vertex> executeTestCase(List<Vertex> path, Double constantWheelbase, Double lengthOfSections, Double distanceOfMeasuringPoints, Surface sensorCurrentSurface) {

        FunctionBlock trajectoryPlanningBlock = new TrajectoryPlanningBlock();

        List<Vertex> result;

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        input.put(ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_wheelbase.toString(), constantWheelbase);
        input.put(ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_path.toString(), path);

        trajectoryPlanningBlock.setInputs(input);
        trajectoryPlanningBlock.execute(1);
        result = (List<Vertex>) trajectoryPlanningBlock.getOutputs().get(ConnectionEntry.TRAJECTORY_PLANNING_BLOCK_detailed_path.toString());

        return result;
    }

    private Double calculateAngle(Double radius, Double constantWheelbase) {
        if (radius == 0.0) {
            return 0.0;
        }
        return (Double) (Math.asin(constantWheelbase / radius));
    }
}
