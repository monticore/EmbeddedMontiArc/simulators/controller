/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.GraphGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IAdjacency;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Edge;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph Grüne on 01.03.2017.
 */
public class FunctionBlockTestExtractGraph extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestExtractGraph(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestExtractGraph.class);
    }

    public void testExtractGraph_1() {
        executeTest(1);
    }

    public void testExtractGraph_2() {
        executeTest(2);
    }

    /*****************************
     * helper functions          *
     *****************************/
    private void executeTest(int exampleIndex) {
        GraphGenerator graphGenerator = new GraphGenerator();
        List<IAdjacency> adjacencies = graphGenerator.getIAdjacencyList(exampleIndex);
        Graph graph = graphGenerator.getGraph(exampleIndex);

        FunctionBlock extractGraph = new ExtractGraph();

        Map<String, Object> inputs = new LinkedHashMap<>();
        inputs.put(ConnectionEntry.EXTRACT_GRAPH_adjacency_list.toString(), adjacencies);

        extractGraph.setInputs(inputs);
        extractGraph.execute(1);
        Graph result = (Graph) extractGraph.getOutputs().get(ConnectionEntry.EXTRACT_GRAPH_graph.toString());

        if(!result.getNumberOfVertices().equals(graph.getNumberOfVertices())) {
            assertFalse(true);
        }
        if(!hasVertices(result, graph)) {
            assertFalse(true);
        }
        if(!hasVertices(graph, result)) {
            assertFalse(true);
        }
        if(!hasEdges(result, graph)) {
            assertFalse(true);
        }
        if(!hasEdges(graph, result)) {
            assertFalse(true);
        }

        assertTrue(true);
    }

    /**
     * returns if graph1 has only vertices which also graph2 has.
     *
     * @param graph1 first graph
     * @param graph2 second graph
     * @return if graph1 has only vertices which also graph2 has.
     */
    private boolean hasVertices(Graph graph1, Graph graph2) {
        for(Vertex resultVertex : graph1.getVertices()) {
            if(!graph2.hasVertex(resultVertex)) {
                return false;
            }
        }
        return true;
    }

    /**
     * returns if graph1 has only edges which also graph2 has.
     *
     * @param graph1 first graph
     * @param graph2 second graph
     * @return if graph1 has only edges which also graph2 has.
     */
    private boolean hasEdges(Graph graph1, Graph graph2) {
        for(Vertex resultVertex1 : graph1.getVertices()) {
            for (Vertex resultVertex2 : graph1.getVertices()) {
                if(graph1.hasEdge(resultVertex1, resultVertex2)) {
                    Edge edge = graph2.getEdge(graph2.getVertex(resultVertex1), graph2.getVertex(resultVertex2));
                    if(!graph1.hasEdge(resultVertex1, resultVertex2, edge)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
