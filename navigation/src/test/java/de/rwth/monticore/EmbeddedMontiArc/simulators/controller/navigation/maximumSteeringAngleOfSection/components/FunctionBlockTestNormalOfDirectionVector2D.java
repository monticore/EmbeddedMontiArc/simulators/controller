/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.maximumSteeringAngleOfSection.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * this is a test for NormalOfDirectionVector2D
 *
 * Created by Christoph Grüne on 21.01.2017.
 */
public class FunctionBlockTestNormalOfDirectionVector2D extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestNormalOfDirectionVector2D(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestNormalOfDirectionVector2D.class);
    }


    /**
     * tests if all inputs (see below) are handled correctly:
     *      (x, y) in [-20, 20]^2 and
     *      (x, y) = (100000.0, -9990.0)
     */
    public void testNormal() {

        FunctionBlock normalOfDirectionVector2D = new NormalOfDirectionVector2D();


        for(double x = -20.0; x < 20.0; x += 0.05) {
            for(double y = -20.0; y < 20.0; y += 0.05) {
                RealVector directionVector = new ArrayRealVector(new double[]{x, y});
                RealVector result;


                Map<String, Object> input = new LinkedHashMap<String, Object>();

                input.put(ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_direction_vector.toString(), directionVector);

                normalOfDirectionVector2D.setInputs(input);
                normalOfDirectionVector2D.execute(1);
                result = (RealVector) normalOfDirectionVector2D.getOutputs().get(ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_normal_vector.toString());

                if (result.dotProduct(directionVector) > 0.0000001 || result.dotProduct(directionVector) < -0.0000001) {
                    assertFalse(true);
                }
            }
        }

        RealVector directionVector = new ArrayRealVector(new double[]{100000.0, -9990.0});
        RealVector result;


        Map<String, Object> input = new LinkedHashMap<String, Object>();

        input.put(ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_direction_vector.toString(), directionVector);

        normalOfDirectionVector2D.setInputs(input);
        normalOfDirectionVector2D.execute(1);
        result = (RealVector) normalOfDirectionVector2D.getOutputs().get(ConnectionEntry.NORMAL_OF_DIRECTION_VECTOR_2D_normal_vector.toString());

        if (result.dotProduct(directionVector) > 0.0000001 || result.dotProduct(directionVector) < -0.0000001) {
            assertFalse(true);
        }

        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/
}

