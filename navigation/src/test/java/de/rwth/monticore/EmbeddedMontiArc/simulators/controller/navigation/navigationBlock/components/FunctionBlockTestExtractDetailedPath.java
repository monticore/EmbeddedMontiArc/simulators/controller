/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.DetailedPathGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.GraphGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph Grüne on 28.02.2017.
 */
public class FunctionBlockTestExtractDetailedPath extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestExtractDetailedPath(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestExtractDetailedPath.class);
    }

    public void testExtractDetailedPath() {
        for (int i = 1; i <= GraphGenerator.NUMBER_OF_EXAMPLES; ++i) {
            executeTest(i);
        }
    }

    /*****************************
     * helper functions          *
     *****************************/
    private void executeTest(int exampleIndex) {
        DetailedPathGenerator nonDetailedPathGenerator = new DetailedPathGenerator();
        List<Vertex> path = nonDetailedPathGenerator.getPath(exampleIndex);
        List<Vertex> detailedPath = nonDetailedPathGenerator.getDetailedPath(exampleIndex);

        FunctionBlock extractDetailedPath = new ExtractDetailedPath();

        Map<String, Object> inputs = new LinkedHashMap<>();
        inputs.put(ConnectionEntry.EXTRACT_DETAILED_PATH_non_detailed_path.toString(), path);

        extractDetailedPath.setInputs(inputs);
        extractDetailedPath.execute(1);
        List<Vertex> result = (List<Vertex>) extractDetailedPath.getOutputs().get(ConnectionEntry.EXTRACT_DETAILED_PATH_detailed_path.toString());

        if(result.size() != detailedPath.size()) {
            assertFalse(true);
        }
        for(int i = 0; i < result.size(); ++i) {
            if(!result.get(i).equals(detailedPath.get(i))) {
                assertFalse(true);
            }
        }

        assertTrue(true);
    }
}
