/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.trajectoryPlanningBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.PathGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph Grüne on 02.03.2017.
 */
public class FunctionBlockTestSetMaximumSteeringAngleInVertex extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestSetMaximumSteeringAngleInVertex(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestSetMaximumSteeringAngleInVertex.class);
    }

    public void testSetMaximumSteeringAngleInVertex() {

        FunctionBlock setMaximumSteeringAngleInVertex = new SetMaximumSteeringAngleInVertex();

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        PathGenerator pathGenerator = new PathGenerator();
        for (double pathLength = 0.0; pathLength < 10.0; pathLength += 1.2) {
            for (double x = -20.0; x < 20.0; x += 12.0) {
                for (double y = -20.0; y < 20.0; y += 12.2) {
                    for (int i = 0; i <= pathLength / pathGenerator.DISTANCE_OF_POINTS; ++i) {
                        for (int j = 0; j <= i; ++j) {
                            Double maximumSteeringAngleOfSection = 10.1;
                            RealVector direction = new ArrayRealVector(new Double[]{x, y, 0.0});
                            if (direction.getNorm() == 0) {
                                direction = new ArrayRealVector(new Double[]{1.0, 0.0, 0.0});
                            }
                            List<Vertex> path = pathGenerator.generateStraightLine(pathLength, 0.0, 0.0, direction, 0);
                            Vertex beginning = path.get(j);
                            Vertex end = path.get(i);

                            input.put(ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_path.toString(), path);
                            input.put(ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_beginning.toString(), beginning);
                            input.put(ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_end.toString(), end);
                            input.put(ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_steering_angle.toString(), maximumSteeringAngleOfSection);

                            setMaximumSteeringAngleInVertex.setInputs(input);
                            setMaximumSteeringAngleInVertex.execute(1);
                            List<Vertex> result = (List<Vertex>) setMaximumSteeringAngleInVertex.getOutputs().get(ConnectionEntry.SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_path_with_angles.toString());

                            for (int k = j; k < i; ++k) {
                                if (k >= j && k <= i && !result.get(k).getMaximumSteeringAngle().equals(maximumSteeringAngleOfSection)) {
                                    assertFalse(true);
                                }
                            }
                        }
                    }
                }
            }
        }

        assertTrue(true);
    }


    /*****************************
     * helper functions          *
     *****************************/
}
