/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.navigation.navigationBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.GraphGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph Grüne on 28.02.2017.
 */
public class FunctionBlockTestFindPath extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestFindPath(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestFindPath.class);
    }

    public void testFindPath_1() {
        executeTest(1);
    }

    public void testFindPath_2() {
        executeTest(2);
    }

    /*****************************
     * helper functions          *
     *****************************/
    private void executeTest(int exampleIndex) {
        GraphGenerator graphGenerator = new GraphGenerator();
        Graph graph = graphGenerator.getGraph(exampleIndex);

        FunctionBlock findPath = new FindPath();

        for(Vertex vStart : graph.getVertices()) {
            for(Vertex vEnd : graph.getVertices()) {
                List<Vertex> path = graphGenerator.getShortestPath(exampleIndex, vStart, vEnd);

                Map<String, Object> inputs = new LinkedHashMap<>();
                inputs.put(ConnectionEntry.FIND_PATH_graph.toString(), graph);
                inputs.put(ConnectionEntry.FIND_PATH_start_vertex.toString(), vStart);
                inputs.put(ConnectionEntry.FIND_PATH_target_vertex.toString(), vEnd);

                findPath.setInputs(inputs);
                findPath.execute(1);
                List<Vertex> result = (List<Vertex>) findPath.getOutputs().get(ConnectionEntry.FIND_PATH_path.toString());

                if(result.size() != path.size()) {
                    assertFalse(true);
                }
                for(int i = 0; i < result.size(); ++i) {
                    Vertex resultCur = result.get(i);
                    Vertex pathCur = path.get(i);
                    if(!resultCur.equals(pathCur)) {
                        assertFalse(true);
                    }
                }
            }
        }

        assertTrue(true);
    }
}
