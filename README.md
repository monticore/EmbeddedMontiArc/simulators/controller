<!-- (c) https://github.com/MontiCore/monticore -->
<a href="https://codeclimate.com/github/MontiSim/controller/maintainability"><img src="https://api.codeclimate.com/v1/badges/4c5b1c0fa45ab841b493/maintainability" /></a>   [![Build Status](https://travis-ci.org/MontiSim/controller.svg?branch=master)](https://travis-ci.org/MontiSim/controller)   [![Coverage Status](https://coveralls.io/repos/github/MontiSim/controller/badge.svg?branch=master)](https://coveralls.io/github/MontiSim/controller?branch=master)

# Controller
This repository includes the vehicle controller for each simulated autonomously driving vehicle.

In this controller the concept of function blocks is used for the processing of data.

# Data exchange: Vehicle Controller
A controller is created for each simulated vehicle. The controller receives sensor data from the simulation module in each update step.

A data bus is simulated for the exchange of data. For example, sensor data are written to the bus and the controller writes the new actuator values to the bus as well.

It should be noted that the controller creates an exception for `null` values on the simulated data bus, which is the reason why this needs to be avoided.
