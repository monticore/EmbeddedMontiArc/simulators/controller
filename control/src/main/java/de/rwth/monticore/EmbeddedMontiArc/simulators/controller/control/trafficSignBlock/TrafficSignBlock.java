/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.trafficSignBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * this function block is just a mock version. It should calculate the maximal velocity we can drive by analysing all detected signs.
 *
 * used formulas: none, for now
 *
 * used function blocks: none, for now
 *
 * Created by Christoph on 09.03.2017.
 */

// Deprecated because computervision was removed
@Deprecated
public class TrafficSignBlock extends FunctionBlock {

    //Input Variables
    private List<Object> signs;
    private Double maxVelocity;

    //Output Variables
    private Double velocity;

    //Global Variables

    //Used Function Blocks

    /**
     * Constructor for a InnerControlBlock object
     */
    public TrafficSignBlock() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        velocity = maxVelocity;
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        signs = (List<Object>) inputs.get(ConnectionEntry.TRAFFIC_SIGN_BLOCK_signs.toString());
        if(signs == null) {
            signs = new LinkedList<>();
        }
        maxVelocity = (Double) inputs.get(ConnectionEntry.TRAFFIC_SIGN_BLOCK_max_velocity.toString());
        super.setInputs(inputs, ConnectionEntry.TRAFFIC_SIGN_BLOCK_signs.toString());
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.TRAFFIC_SIGN_BLOCK_velocity.toString(), velocity);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {};
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
}
