/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.mainControlBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.hazardBlock.HazardBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.InnerControlBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.trafficSignBlock.TrafficSignBlock;

import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.trajectoryBlock.TrajectoryBlock;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * this function block calculates the main functionality of the controller. For this, one has to pass the BusMap as input.
 * By execution, it calculates the actuator values for steering, engine, brake and gear.
 *
 * used formulas: only in subblocks
 *
 * used function blocks:
 *      trajectoryBlock
 *      signBlock
 *      hazardBlock
 *      innerControlBlock
 *
 * Created by Christoph Grüne on 13.01.2017.
 */
public class MainControlBlock extends FunctionBlock {

    //Input Variables
    //All BusEntries, not needed as explicit variables

    //Output Variables
    private Double actuatorEngine;
    private Double actuatorBrake;
    private Double actuatorSteering;
    private Integer actuatorGear;

    //Global Variables

    FunctionBlockManagement functionBlockManagement = new FunctionBlockManagement(this);

    /**
     * Constructor for a InnerControlBlock object
     */
    public MainControlBlock() {initComponents();}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        Map<String, Object> outputs = new LinkedHashMap<>();
        outputs = functionBlockManagement.executeAllFunctionBlocks(timeDelta);
        actuatorBrake = (Double) outputs.get(ConnectionEntry.MAIN_CONTROL_BLOCK_actuator_brake.toString());
        actuatorEngine = (Double) outputs.get(ConnectionEntry.MAIN_CONTROL_BLOCK_actuator_engine.toString());
        actuatorGear = (Integer) outputs.get(ConnectionEntry.MAIN_CONTROL_BLOCK_actuator_gear.toString());
        actuatorSteering = (Double) outputs.get(ConnectionEntry.MAIN_CONTROL_BLOCK_actuator_steering.toString());
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {

        // If navigation is not set, then allow for default navigation list with only current position
        if (inputs.get(BusEntry.NAVIGATION_DETAILED_PATH_WITH_MAX_STEERING_ANGLE.toString()) == null) {
            List<Vertex> defaultNavigationList = new ArrayList<>();
            RealVector pos = (RealVector)(inputs.get(BusEntry.SENSOR_GPS_COORDINATES.toString()));
            Vertex currentPos = new Vertex(-1L, -1L, pos, 0.0);
            defaultNavigationList.add(currentPos);
            inputs.put(BusEntry.NAVIGATION_DETAILED_PATH_WITH_MAX_STEERING_ANGLE.toString(), defaultNavigationList);
        }

        super.setInputs(inputs, BusEntry.SENSOR_CAMERA.toString());
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(BusEntry.ACTUATOR_ENGINE.toString(), actuatorEngine);
        outputs.put(BusEntry.ACTUATOR_STEERING.toString(), actuatorSteering);
        outputs.put(BusEntry.ACTUATOR_BRAKE.toString(), actuatorBrake);
        outputs.put(BusEntry.ACTUATOR_GEAR.toString(), actuatorGear);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                BusEntry.NAVIGATION_DETAILED_PATH_WITH_MAX_STEERING_ANGLE.toString(),
                BusEntry.CONSTANT_MAXIMUM_TOTAL_VELOCITY.toString(),
                BusEntry.SIMULATION_DELTA_TIME.toString(),
                BusEntry.CONSTANT_NUMBER_OF_GEARS.toString(),
                BusEntry.CONSTANT_WHEELBASE.toString(),
                BusEntry.CONSTANT_MOTOR_MAX_ACCELERATION.toString(),
                BusEntry.CONSTANT_MOTOR_MIN_ACCELERATION.toString(),
                BusEntry.CONSTANT_BRAKES_MAX_ACCELERATION.toString(),
                BusEntry.CONSTANT_BRAKES_MIN_ACCELERATION.toString(),
                BusEntry.CONSTANT_STEERING_MAX_ANGLE.toString(),
                BusEntry.CONSTANT_STEERING_MIN_ANGLE.toString(),
                BusEntry.CONSTANT_TRAJECTORY_ERROR.toString(),
                BusEntry.VEHICLE_MAX_TEMPORARY_ALLOWED_VELOCITY.toString(),
                BusEntry.SENSOR_VELOCITY.toString(),
                BusEntry.SENSOR_STEERING.toString(),
                BusEntry.SENSOR_DISTANCE_TO_RIGHT.toString(),
                BusEntry.SENSOR_DISTANCE_TO_LEFT.toString(),
                BusEntry.SENSOR_GPS_COORDINATES.toString(),
                BusEntry.SENSOR_CURRENT_SURFACE.toString(),
                BusEntry.SENSOR_WEATHER.toString(),
                BusEntry.SENSOR_CAMERA.toString(),
                BusEntry.SENSOR_COMPASS.toString(),
                BusEntry.COMPUTERVISION_VANISHING_POINT.toString(),
                BusEntry.COMPUTERVISION_DETECTED_CARS.toString(),
                BusEntry.COMPUTERVISION_DETECTED_PEDESTRIANS.toString(),
                BusEntry.COMPUTERVISION_DETECTED_LANES.toString(),
                BusEntry.COMPUTERVISION_DETECTED_SIGNS.toString(),
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {
        functionBlockManagement = new FunctionBlockManagement(this);

        FunctionBlock trajectoryBlock = new TrajectoryBlock();
        functionBlockManagement.addFunctionBlock(trajectoryBlock);

        FunctionBlock hazardBlock = new HazardBlock();
        functionBlockManagement.addFunctionBlock(hazardBlock);

        FunctionBlock trafficSignBlock = new TrafficSignBlock();
        functionBlockManagement.addFunctionBlock(trafficSignBlock);

        FunctionBlock innerControlBlock = new InnerControlBlock();
        functionBlockManagement.addFunctionBlock(innerControlBlock);

        //connections to trajectoryBlock
        functionBlockManagement.connect(this, this, BusEntry.NAVIGATION_DETAILED_PATH_WITH_MAX_STEERING_ANGLE.toString(), trajectoryBlock, ConnectionEntry.TRAJECTORY_BLOCK_path.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_GPS_COORDINATES.toString(), trajectoryBlock, ConnectionEntry.TRAJECTORY_BLOCK_gps_coordinates.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_VELOCITY.toString(), trajectoryBlock, ConnectionEntry.TRAJECTORY_BLOCK_current_velocity.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_CURRENT_SURFACE.toString(), trajectoryBlock, ConnectionEntry.TRAJECTORY_BLOCK_current_surface.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_WEATHER.toString(), trajectoryBlock, ConnectionEntry.TRAJECTORY_BLOCK_current_weather.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_MAXIMUM_TOTAL_VELOCITY.toString(), trajectoryBlock, ConnectionEntry.TRAJECTORY_BLOCK_max_velocity.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_BRAKES_MAX_ACCELERATION.toString(), trajectoryBlock, ConnectionEntry.TRAJECTORY_BLOCK_brakes_max_acceleration.toString());

        //connections to hazardBlock
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_STEERING.toString(), hazardBlock, ConnectionEntry.HAZARD_BLOCK_current_steering.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_CURRENT_SURFACE.toString(), hazardBlock, ConnectionEntry.HAZARD_BLOCK_current_surface.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_VELOCITY.toString(), hazardBlock, ConnectionEntry.HAZARD_BLOCK_current_velocity.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_BRAKES_MAX_ACCELERATION.toString(), hazardBlock, ConnectionEntry.HAZARD_BLOCK_brakes_max_acceleration.toString());
        functionBlockManagement.connect(this, this, BusEntry.COMPUTERVISION_DETECTED_CARS.toString(), hazardBlock, ConnectionEntry.HAZARD_BLOCK_cars.toString());
        functionBlockManagement.connect(this, this, BusEntry.COMPUTERVISION_DETECTED_PEDESTRIANS.toString(), hazardBlock, ConnectionEntry.HAZARD_BLOCK_pedestrians.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_DISTANCE_TO_LEFT.toString(), hazardBlock, ConnectionEntry.HAZARD_BLOCK_distance_to_left.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_DISTANCE_TO_RIGHT.toString(), hazardBlock, ConnectionEntry.HAZARD_BLOCK_distance_to_right.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_MAXIMUM_TOTAL_VELOCITY.toString(), hazardBlock, ConnectionEntry.HAZARD_BLOCK_maximum_velocity.toString());

        //connections to trafficSignBlock
        functionBlockManagement.connect(this, this, BusEntry.COMPUTERVISION_DETECTED_SIGNS.toString(), trafficSignBlock, ConnectionEntry.TRAFFIC_SIGN_BLOCK_signs.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_MAXIMUM_TOTAL_VELOCITY.toString(), trafficSignBlock, ConnectionEntry.TRAFFIC_SIGN_BLOCK_max_velocity.toString());

        //connections to innerControlBlock
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_DISTANCE_TO_LEFT.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_distance_to_left.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_DISTANCE_TO_RIGHT.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_distance_to_right.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_STEERING.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_steering.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_CURRENT_SURFACE.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_current_surface.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_WEATHER.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_weather.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_VELOCITY.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_velocity.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_COMPASS.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_orientation.toString());
        functionBlockManagement.connect(this, this, BusEntry.SENSOR_GPS_COORDINATES.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_gps_coordinates.toString());
        functionBlockManagement.connect(this, this, BusEntry.NAVIGATION_DETAILED_PATH_WITH_MAX_STEERING_ANGLE.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_path.toString());
        functionBlockManagement.connect(this, this, BusEntry.COMPUTERVISION_VANISHING_POINT.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_angle_to_vanishing_point.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_MAXIMUM_TOTAL_VELOCITY.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_constant_maximum_velocity.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_NUMBER_OF_GEARS.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_constant_number_of_gears.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_STEERING_MIN_ANGLE.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_constant_min_steering_angle.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_STEERING_MAX_ANGLE.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_constant_max_steering_angle.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_MOTOR_MAX_ACCELERATION.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_constant_motor_max_acceleration.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_BRAKES_MAX_ACCELERATION.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_constant_brake_max_acceleration.toString());
        functionBlockManagement.connect(this, this, BusEntry.CONSTANT_TRAJECTORY_ERROR.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_constant_trajectory_error.toString());
        functionBlockManagement.connect(this, this, BusEntry.VEHICLE_MAX_TEMPORARY_ALLOWED_VELOCITY.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_vehicle_max_temporary_allowed_velocity.toString());
        functionBlockManagement.connect(this, hazardBlock, ConnectionEntry.HAZARD_BLOCK_hazard_velocity.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_hazard_velocity.toString());
        functionBlockManagement.connect(this, trafficSignBlock, ConnectionEntry.TRAFFIC_SIGN_BLOCK_velocity.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_sign_velocity.toString());
        functionBlockManagement.connect(this, trajectoryBlock, ConnectionEntry.TRAJECTORY_BLOCK_velocity.toString(), innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_trajectory_velocity.toString());

        //connections to this
        functionBlockManagement.connect(this, innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_actuator_brake.toString(), this, ConnectionEntry.MAIN_CONTROL_BLOCK_actuator_brake.toString());
        functionBlockManagement.connect(this, innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_actuator_engine.toString(), this, ConnectionEntry.MAIN_CONTROL_BLOCK_actuator_engine.toString());
        functionBlockManagement.connect(this, innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_actuator_gear.toString(), this, ConnectionEntry.MAIN_CONTROL_BLOCK_actuator_gear.toString());
        functionBlockManagement.connect(this, innerControlBlock, ConnectionEntry.INNER_CONTROL_BLOCK_actuator_steering.toString(), this, ConnectionEntry.MAIN_CONTROL_BLOCK_actuator_steering.toString());
    }
}
