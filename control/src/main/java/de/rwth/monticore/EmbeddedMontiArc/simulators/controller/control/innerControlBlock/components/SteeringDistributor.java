/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * component SteeringDistributor {
 *     ports    in  Double anglePID,
 *              out Double steeringAngle,
 *              out Double actuatorSteeringValue;
 * }
 */

/**
 * Created by Christoph Grüne on 05.12.2016.
 */
public class SteeringDistributor extends FunctionBlock {

    //Input Arguments
    private Double anglePID;
    private Double sensorSteering;
    private Double minSteeringAngle;
    private Double maxSteeringAngle;

    //Output Arguments
    private Double actuatorSteeringValue;

    //Global Variables

    /**
     * Constructor for a SteeringDistributor object
     */
    public SteeringDistributor() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        actuatorSteeringValue = calculateAngleValue();
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        anglePID = (Double) inputs.get(ConnectionEntry.STEERING_DISTRIBUTOR_angle_pid.toString());
        sensorSteering = (Double) inputs.get(ConnectionEntry.STEERING_DISTRIBUTOR_sensor_steering.toString());
        minSteeringAngle = (Double) inputs.get(ConnectionEntry.STEERING_DISTRIBUTOR_min_steering_angle.toString());
        maxSteeringAngle = (Double) inputs.get(ConnectionEntry.STEERING_DISTRIBUTOR_max_steering_angle.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.STEERING_DISTRIBUTOR_actuator_steering_value.toString(), actuatorSteeringValue);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.STEERING_DISTRIBUTOR_angle_pid.toString(),
                ConnectionEntry.STEERING_DISTRIBUTOR_sensor_steering.toString(),
                ConnectionEntry.STEERING_DISTRIBUTOR_min_steering_angle.toString(),
                ConnectionEntry.STEERING_DISTRIBUTOR_max_steering_angle.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * function to calculate actuatorSteeringValue
     *
     * @return the value of actuatorSteeringValue
     */
    private Double calculateAngleValue() {
        if(anglePID < minSteeringAngle) {
            return minSteeringAngle;
        } else if(anglePID > maxSteeringAngle) {
            return maxSteeringAngle;
        } else {
            return anglePID;
        }
    }
}
