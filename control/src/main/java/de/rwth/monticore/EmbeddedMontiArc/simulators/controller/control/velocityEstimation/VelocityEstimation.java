/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.velocityEstimation;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.exceptions.IllegalPathIteratorException;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.GetNextElement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.MaximumVelocityBySteeringAnAngle;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.velocityEstimation.components.ExtractMaxSteeringAngleFromVertex;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * this function block calculates the maximal velocity one can drive by foreseeing the path.
 * It calculates the maximal velocity by iterating over all elements from the start vertex to
 * the vertex that is maximal the specified distance away and extracting the minimum over all
 * maximal velocities of every vertex.
 *
 * used formulas: none
 *
 * used function blocks:
 *      getNextElement
 *      extractMaxSteeringAngleFromVertex
 *      maximumVelocityBySteeringAnAngle
 *
 * Created by Christoph on 07.03.2017.
 */
public class VelocityEstimation extends FunctionBlock {

    //Input Variables
    private Iterator<Vertex> pathIterator;
    private Vertex startVertex;
    private Double distance;
    private Double maxVelocity;
    private Surface currentSurface;
    private Double currentWeather;
    private Double wheelbase;

    //Output Variables
    private Double velocity;

    //Global Variables
    private Boolean pathEnd = false;
    private Vertex cur;

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public VelocityEstimation() {initComponents();}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        Map<String, Object> outputs = null;
        velocity = maxVelocity;
        cur = startVertex;
        if(!pathIterator.hasNext()) {
            velocity = 0.0;
        } else {
            while (!pathEnd && startVertex.getPosition().getDistance(cur.getPosition()) <= distance) {
                outputs = functionBlockManagement.executeAllFunctionBlocks(timeDelta);
                cur = (Vertex) outputs.get(ConnectionEntry.VELOCITY_ESTIMATION_next_vertex.toString());
                pathEnd = (Boolean) outputs.get(ConnectionEntry.VELOCITY_ESTIMATION_path_end.toString());
            }
            if (outputs != null) {
                if (pathEnd) {
                    velocity = 0.0;
                } else {
                    velocity = (Double) outputs.get(ConnectionEntry.VELOCITY_ESTIMATION_velocity.toString());
                }
            } else {
                if (pathEnd) {
                    velocity = 0.0;
                } else {
                    throw new IllegalPathIteratorException("MainControlBlock->TrajectoryBlock->VelocityEstimation: An incorrect PathIterator was passed.");
                }
            }
        }
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        pathIterator = (Iterator<Vertex>) inputs.get(ConnectionEntry.VELOCITY_ESTIMATION_path_iterator.toString());
        startVertex = (Vertex) inputs.get(ConnectionEntry.VELOCITY_ESTIMATION_start_vertex.toString());
        distance = (Double) inputs.get(ConnectionEntry.VELOCITY_ESTIMATION_distance.toString());
        currentSurface = (Surface) inputs.get(ConnectionEntry.VELOCITY_ESTIMATION_current_surface.toString());
        currentWeather = (Double) inputs.get(ConnectionEntry.VELOCITY_ESTIMATION_current_weather.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.VELOCITY_ESTIMATION_velocity.toString(), velocity);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.VELOCITY_ESTIMATION_path_iterator.toString(),
                ConnectionEntry.VELOCITY_ESTIMATION_start_vertex.toString(),
                ConnectionEntry.VELOCITY_ESTIMATION_distance.toString(),
                ConnectionEntry.VELOCITY_ESTIMATION_current_surface.toString(),
                ConnectionEntry.VELOCITY_ESTIMATION_current_weather.toString()

        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {

        functionBlockManagement = new FunctionBlockManagement(this);

        FunctionBlock getNextVertex = new GetNextElement<Vertex>();
        functionBlockManagement.addFunctionBlock(getNextVertex);

        FunctionBlock extractSteeringAngleFromVertex = new ExtractMaxSteeringAngleFromVertex();
        functionBlockManagement.addFunctionBlock(extractSteeringAngleFromVertex);

        FunctionBlock maximumVelocityBySteeringAnAngle = new MaximumVelocityBySteeringAnAngle();
        functionBlockManagement.addFunctionBlock(maximumVelocityBySteeringAnAngle);

        //connections to getNextVertex
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_ESTIMATION_path_iterator.toString(), getNextVertex, ConnectionEntry.GET_NEXT_ELEMENT_iterator.toString());

        //connections to extractSteeringAngleFromVertex
        functionBlockManagement.connect(this, getNextVertex, ConnectionEntry.GET_NEXT_ELEMENT_next_element.toString(), extractSteeringAngleFromVertex, ConnectionEntry.EXTRACT_MAX_STEERING_ANGLE_FROM_VERTEX_vertex.toString());

        //connections to maximumVelocityBySteeringAnAngle
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_ESTIMATION_current_surface.toString(), maximumVelocityBySteeringAnAngle, ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_surface.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_ESTIMATION_current_weather.toString(), maximumVelocityBySteeringAnAngle, ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_weather.toString());
        functionBlockManagement.connect(this, extractSteeringAngleFromVertex, ConnectionEntry.EXTRACT_MAX_STEERING_ANGLE_FROM_VERTEX_steering_angle.toString(), maximumVelocityBySteeringAnAngle, ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_steering_angle.toString());

        //connections to this
        functionBlockManagement.connect(this, maximumVelocityBySteeringAnAngle, ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_velocity.toString(), this, ConnectionEntry.VELOCITY_ESTIMATION_velocity.toString());
        functionBlockManagement.connect(this, getNextVertex, ConnectionEntry.GET_NEXT_ELEMENT_list_end.toString(), this, ConnectionEntry.VELOCITY_ESTIMATION_path_end.toString());
        functionBlockManagement.connect(this, getNextVertex, ConnectionEntry.GET_NEXT_ELEMENT_next_element.toString(), this, ConnectionEntry.VELOCITY_ESTIMATION_next_vertex.toString());
    }
}
