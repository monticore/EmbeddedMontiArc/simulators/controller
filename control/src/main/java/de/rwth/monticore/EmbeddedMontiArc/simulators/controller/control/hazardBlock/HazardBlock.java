/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.hazardBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph on 04.03.2017.
 * Modified by Xiangwei Wang on 03.03.2017
 * should be improved after the trackObject is finished by CV group
 *
 * used formula :
 * depth*Math.tan(angle) > distanceToRight + widthOfCar/2 and
 * depth*Math.abs(Math.tan(angle)) > distanceToLeft + widthOfCar/2
 * to check if the pedestrian is in the road
 *
 * depth*Math.abs(Math.tan(angle)) > widthOfCar + 0.1
 * to check if the detected car has conflict with our car
 *
 * brakeAcceleration = (currentVelocity * currentVelocity) / (2 * (m.getDepth() - safeDistance))
 * to calculate the brake acceleration
 *
 * used function blocks: none
 */

// Deprecated, required computervision, which is no longer available
@Deprecated
public class HazardBlock extends FunctionBlock {

    //Input Arguments
    private Double currentVelocity;
    private Double brakesMaxAcceleration;
    private List<Object> cars;
    private List<Object> pedestrians;
    private Double distanceToLeft;
    private Double distanceToRight;
    private Double maximumVelocity;

    //Output Arguments
    private Double desiredVelocity;

    //Global Arguments
    private boolean brake1;
    private boolean brake2;
    private double widthOfCar = 1.81356;
    //after deltaTime, we need to evaluate the Hazard Velocity again.
    private Double deltaTime;
    //private double lengthOfCar = 4.826;
    //set safe distance between car and object to be 1m
    private final double safeDistance = 2.0;
    //the depth could be -1 m if CV gets the depth of object incorrectly
    private final double errorDepth = -1;


    /**
     * Constructor for a VelocityLogic object
     */
    public HazardBlock() {
        initComponents();
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/
    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        try {
            if (judgeForBrakeBecauseOfCar(cars) || judgeForBrakeBecauseOfPedestrian(pedestrians)) {
                desiredVelocity = currentVelocity - calculateMaximalBrakeAcceleration(pedestrians, cars) * deltaTime;
            } else {
                desiredVelocity = maximumVelocity;
            }
        }
        catch (NullPointerException npe) {
            desiredVelocity = maximumVelocity;
        }
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    @SuppressWarnings("unchecked")
    public void setInputs(Map<String, Object> inputs) {
        deltaTime = (Double)  inputs.get(BusEntry.SIMULATION_DELTA_TIME.toString());
        currentVelocity = (Double) inputs.get(ConnectionEntry.HAZARD_BLOCK_current_velocity.toString());
        brakesMaxAcceleration = (Double) inputs.get(ConnectionEntry.HAZARD_BLOCK_brakes_max_acceleration.toString());
        cars = (List<Object>) inputs.get(ConnectionEntry.HAZARD_BLOCK_cars.toString());
        pedestrians = (List<Object>) inputs.get(ConnectionEntry.HAZARD_BLOCK_pedestrians.toString());
        if(cars == null) {
            cars = new LinkedList<>();
        }
        if(pedestrians == null) {
            pedestrians = new LinkedList<>();
        }
        distanceToLeft = (Double) inputs.get(ConnectionEntry.HAZARD_BLOCK_distance_to_left.toString());
        distanceToRight = (Double) inputs.get(ConnectionEntry.HAZARD_BLOCK_distance_to_right.toString());
        maximumVelocity = (Double) inputs.get(ConnectionEntry.HAZARD_BLOCK_maximum_velocity.toString());
        super.setInputs(inputs, ConnectionEntry.HAZARD_BLOCK_cars.toString(), ConnectionEntry.HAZARD_BLOCK_pedestrians.toString());
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.HAZARD_BLOCK_hazard_velocity.toString(), desiredVelocity);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[]{
                BusEntry.SIMULATION_DELTA_TIME.toString(),
                ConnectionEntry.HAZARD_BLOCK_current_velocity.toString(),
                ConnectionEntry.HAZARD_BLOCK_brakes_max_acceleration.toString(),
                ConnectionEntry.HAZARD_BLOCK_cars.toString(),
                ConnectionEntry.HAZARD_BLOCK_pedestrians.toString(),
                ConnectionEntry.HAZARD_BLOCK_distance_to_left.toString(),
                ConnectionEntry.HAZARD_BLOCK_distance_to_right.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {}

    /**
     * since the implementation of trackObject will not be finished, so i can not *
     * predict the path of the pedestrian and the velocity of the car             *
     *  i just check if there is a pedestrian in the path of the car              *
     * if yes, evaluate the brake acceleration, with which the car will stop in   *
     * front of the pedestrian with safe distance smoothly                                           *
     */


    private boolean judgeForBrakeBecauseOfPedestrian(List<Object> pedestrians) {

        //first make sure that the detected pedestrian is in the road, otherwise ignore it
        for (Object r : pedestrians) {
            //assuming the CV returns an angle is positive in degree when the pedestrian is on the right side.
            if(isPedestrianInRoad(r))
            {
                return true;
            }
        }
        return false;
    }

    private boolean judgeForBrakeBecauseOfCar(List<Object> cars){

        for(Object m : cars){
            if(isConflict(m)){
                return true;
            }
        }
        return false;
    }


    /*******************************************************************************************
     * calculate the soft brake acceleration to stop at least safe distance away from the pedestrian *
     *******************************************************************************************/

    private double calculateMaximalBrakeAcceleration(List<Object> pedestrians, List<Object> cars) {
        // assume that the brake acceleration is constant
        double brakeAcceleration;
        double maxBrakeAcceleration1 = 0.0;
        double maxBrakeAcceleration2 = 0.0;
        double maxBrakeAcceleration;

        for (Object r : pedestrians) {
            //set the brakeAcceleration to be positive
            if(isPedestrianInRoad(r)) {
                brakeAcceleration = (currentVelocity * currentVelocity) / (2 * (0.0 - safeDistance));
                if (brakeAcceleration > maxBrakeAcceleration1) {
                    maxBrakeAcceleration1 = brakeAcceleration;
                }
            }
        }


        for(Object m : cars )
        {
            if(isConflict(m)) {
                //set the brakeAcceleration to be positive
                brakeAcceleration = (currentVelocity * currentVelocity) / (2 * (0.0 - safeDistance));
                if (brakeAcceleration > maxBrakeAcceleration2) {
                    maxBrakeAcceleration2 = brakeAcceleration;
                }
            }
        }

        maxBrakeAcceleration = Math.max(maxBrakeAcceleration1,maxBrakeAcceleration2);
        if (maxBrakeAcceleration > brakesMaxAcceleration) {
            //TODO
            //connect to the steeringLogic
            maxBrakeAcceleration = brakesMaxAcceleration;
        }

        return maxBrakeAcceleration;
    }


    /**
     * judge if the pedestrian is in road
     */

    boolean isPedestrianInRoad(Object r)
    {
        double depth = 0.0;
        double angle = 0.0;
        if(angle >= 0.0){
            if(depth*Math.tan(angle) > distanceToRight + widthOfCar/2){
                return false;
            }
            else
                return true;
        }
        else if(depth*Math.abs(Math.tan(angle)) > distanceToLeft + widthOfCar/2){
            return false;
        }
        else return true;

    }

    /**
     * judge if the detected car has conflict with our car
     */

    boolean isConflict(Object c)
    {
        double depth = 0.0;
        double angle = 0.0;

        if(depth*Math.abs(Math.tan(angle)) > widthOfCar + 0.1){
            return false;
        }
        return true;
    }
}
