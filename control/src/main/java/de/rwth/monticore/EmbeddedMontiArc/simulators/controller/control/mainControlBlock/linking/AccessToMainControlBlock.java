/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.mainControlBlock.linking;

/**
 * Created by Christoph Grüne on 13.01.2017.
 */
@Deprecated
public class AccessToMainControlBlock {
}
