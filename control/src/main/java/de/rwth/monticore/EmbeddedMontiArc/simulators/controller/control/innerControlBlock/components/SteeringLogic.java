/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * component VelocityDistributor {
 *     ports out steeringAngle;
 * }
 */

/**
 * Created by Christoph Grüne on 04.12.2016.
 * Modified on 10.01.2017 by Stefan Erlbeck
 */
public class SteeringLogic extends FunctionBlock {

    //Input Arguments get from sensor
    private Double currentVelocity;
    private Double distanceToRight;
    private Double distanceToLeft;
    //assuming the CV returns an angle in radians with counter-clockwise orientation stored as double
    private Double vanishingAngle;
    //assuming the compass sensor returns the angle from (0,1) to car-direction counter-clockwise in radians
    private Double globalOrientation;
    private List<Vertex> path;
    private RealVector gpsCoordinates;

    //Output Arguments
    private Double steeringAngle;

    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a SteeringLogic object
     */
    public SteeringLogic() { initComponents(); }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        Map<String, Object> outputs = functionBlockManagement.executeAllFunctionBlocks(timeDelta);

        steeringAngle = (Double) outputs.get(ConnectionEntry.STEERING_LOGIC_steering_angle.toString());
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        currentVelocity = (Double) inputs.get(ConnectionEntry.STEERING_LOGIC_current_velocity.toString());
        distanceToLeft = (Double) inputs.get(ConnectionEntry.STEERING_LOGIC_distance_to_left.toString());
        distanceToRight = (Double) inputs.get(ConnectionEntry.STEERING_LOGIC_distance_to_right.toString());
        vanishingAngle = (Double) inputs.get(ConnectionEntry.STEERING_LOGIC_vanishing_angle.toString());
        path = (List<Vertex>) inputs.get(ConnectionEntry.STEERING_LOGIC_path.toString());
        gpsCoordinates = (RealVector) inputs.get(ConnectionEntry.STEERING_LOGIC_gps_coordinates.toString());
        globalOrientation = (Double) inputs.get(ConnectionEntry.STEERING_LOGIC_global_orientation.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.STEERING_LOGIC_steering_angle.toString(), steeringAngle);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.STEERING_LOGIC_current_velocity.toString(),
                ConnectionEntry.STEERING_LOGIC_distance_to_left.toString(),
                ConnectionEntry.STEERING_LOGIC_distance_to_right.toString(),
                ConnectionEntry.STEERING_LOGIC_vanishing_angle.toString(),
                ConnectionEntry.STEERING_LOGIC_path.toString(),
                ConnectionEntry.STEERING_LOGIC_gps_coordinates.toString(),
                ConnectionEntry.STEERING_LOGIC_global_orientation.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {

        functionBlockManagement = new FunctionBlockManagement(this);

        FunctionBlock steeringLogicHelper = new SteeringLogicHelper();
        functionBlockManagement.addFunctionBlock(steeringLogicHelper);

        //connections to steeringLogicHelper
        functionBlockManagement.connect(this, this, ConnectionEntry.STEERING_LOGIC_current_velocity.toString(), steeringLogicHelper, ConnectionEntry.STEERING_LOGIC_HELPER_current_velocity.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.STEERING_LOGIC_distance_to_left.toString(), steeringLogicHelper, ConnectionEntry.STEERING_LOGIC_HELPER_distance_to_left.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.STEERING_LOGIC_distance_to_right.toString(), steeringLogicHelper, ConnectionEntry.STEERING_LOGIC_HELPER_distance_to_right.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.STEERING_LOGIC_vanishing_angle.toString(), steeringLogicHelper, ConnectionEntry.STEERING_LOGIC_HELPER_vanishing_angle.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.STEERING_LOGIC_path.toString(), steeringLogicHelper, ConnectionEntry.STEERING_LOGIC_HELPER_path.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.STEERING_LOGIC_gps_coordinates.toString(), steeringLogicHelper, ConnectionEntry.STEERING_LOGIC_HELPER_gps_coordinates.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.STEERING_LOGIC_global_orientation.toString(), steeringLogicHelper, ConnectionEntry.STEERING_LOGIC_HELPER_global_orientation.toString());

        //connections to this
        functionBlockManagement.connect(this, steeringLogicHelper, ConnectionEntry.STEERING_LOGIC_HELPER_steering_angle.toString(), this, ConnectionEntry.STEERING_LOGIC_steering_angle.toString());
    }
}
