/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.trajectoryBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.CreateIterator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.MinimumDouble;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.trajectoryBlock.components.DistanceEstimation;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.trajectoryBlock.components.LocateNearestVertexInPath;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.velocityEstimation.VelocityEstimation;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * this function block calculates the velocity we can drive by foreseeing the trajectory.
 * It uses the distanceEstimation in order to estimate the distance we have to foresee.
 * Then, it calculates the maximal velocity we can drive by using the maximal steering angle we drive by passing the vertex.
 * -> min({{max(v) | v := velocity of vertex } | vertex in path })
 *
 * used formulas: none
 *
 * used functionBlocks:
 *      distanceEstimation
 *      locateNearestVertexInPath
 *      createPathIterator
 *      velocityEstimation
 *
 * Created by Christoph on 07.03.2017.
 */
public class TrajectoryBlock extends FunctionBlock {

    //Input Variables
    private List<Vertex> path;
    private RealVector gpsCoordinates;
    private Double currentVelocity;
    private Surface currentSurface;
    private Double currentWeather;
    private Double maxVelocity;
    private Double brakesMaxAcceleration;

    //Output Variables
    private Double velocity;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public TrajectoryBlock() {initComponents();}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        Map<String, Object> outputs = functionBlockManagement.executeAllFunctionBlocks(timeDelta);
        velocity = (Double) outputs.get(ConnectionEntry.TRAJECTORY_BLOCK_velocity.toString());
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        path = (List<Vertex>) inputs.get(ConnectionEntry.TRAJECTORY_BLOCK_path.toString());
        currentVelocity = (Double) inputs.get(ConnectionEntry.TRAJECTORY_BLOCK_current_velocity.toString());
        currentSurface = (Surface) inputs.get(ConnectionEntry.TRAJECTORY_BLOCK_current_surface.toString());
        currentWeather = (Double) inputs.get(ConnectionEntry.TRAJECTORY_BLOCK_current_weather.toString());
        gpsCoordinates = (RealVector) inputs.get(ConnectionEntry.TRAJECTORY_BLOCK_gps_coordinates.toString());
        maxVelocity = (Double) inputs.get(ConnectionEntry.TRAJECTORY_BLOCK_max_velocity.toString());
        brakesMaxAcceleration = (Double) inputs.get(ConnectionEntry.TRAJECTORY_BLOCK_brakes_max_acceleration.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.TRAJECTORY_BLOCK_velocity.toString(), velocity);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.TRAJECTORY_BLOCK_path.toString(),
                ConnectionEntry.TRAJECTORY_BLOCK_current_surface.toString(),
                ConnectionEntry.TRAJECTORY_BLOCK_current_weather.toString(),
                ConnectionEntry.TRAJECTORY_BLOCK_gps_coordinates.toString(),
                ConnectionEntry.TRAJECTORY_BLOCK_max_velocity.toString(),
                ConnectionEntry.TRAJECTORY_BLOCK_brakes_max_acceleration.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {

        functionBlockManagement = new FunctionBlockManagement(this);

        FunctionBlock distanceEstimation = new DistanceEstimation();
        functionBlockManagement.addFunctionBlock(distanceEstimation);

        FunctionBlock locateNearestVertexInPath = new LocateNearestVertexInPath();
        functionBlockManagement.addFunctionBlock(locateNearestVertexInPath);

        FunctionBlock createPathIterator = new CreateIterator<Vertex>();
        functionBlockManagement.addFunctionBlock(createPathIterator);

        FunctionBlock velocityEstimation = new VelocityEstimation();
        functionBlockManagement.addFunctionBlock(velocityEstimation);

        FunctionBlock minimumDouble = new MinimumDouble();
        functionBlockManagement.addFunctionBlock(minimumDouble);

        //connections to distanceEstimation
        functionBlockManagement.connect(this, this, ConnectionEntry.TRAJECTORY_BLOCK_brakes_max_acceleration.toString(), distanceEstimation, ConnectionEntry.DISTANCE_ESTIMATION_brakes_max_acceleration.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.TRAJECTORY_BLOCK_current_velocity.toString(), distanceEstimation, ConnectionEntry.DISTANCE_ESTIMATION_current_velocity.toString());

        //connections to locateNearestVertexInPath
        functionBlockManagement.connect(this, this, ConnectionEntry.TRAJECTORY_BLOCK_path.toString(), locateNearestVertexInPath, ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_path.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.TRAJECTORY_BLOCK_gps_coordinates.toString(), locateNearestVertexInPath, ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_gps_coordinates.toString());

        //connections to createPathIterator
        functionBlockManagement.connect(this, this, ConnectionEntry.TRAJECTORY_BLOCK_path.toString(), createPathIterator, ConnectionEntry.CREATE_ITERATOR_list.toString());
        functionBlockManagement.connect(this, locateNearestVertexInPath, ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_nearest_vertex.toString(), createPathIterator, ConnectionEntry.CREATE_ITERATOR_element_to_begin.toString());

        //connections to velocityEstimation
        functionBlockManagement.connect(this, this, ConnectionEntry.TRAJECTORY_BLOCK_current_surface.toString(), velocityEstimation, ConnectionEntry.VELOCITY_ESTIMATION_current_surface.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.TRAJECTORY_BLOCK_current_weather.toString(), velocityEstimation, ConnectionEntry.VELOCITY_ESTIMATION_current_weather.toString());
        functionBlockManagement.connect(this, locateNearestVertexInPath, ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_nearest_vertex.toString(), velocityEstimation, ConnectionEntry.VELOCITY_ESTIMATION_start_vertex.toString());
        functionBlockManagement.connect(this, createPathIterator, ConnectionEntry.CREATE_ITERATOR_iterator.toString(), velocityEstimation, ConnectionEntry.VELOCITY_ESTIMATION_path_iterator.toString());
        functionBlockManagement.connect(this, distanceEstimation, ConnectionEntry.DISTANCE_ESTIMATION_estimated_distance.toString(), velocityEstimation, ConnectionEntry.VELOCITY_ESTIMATION_distance.toString());

        //connections to minimumDouble
        functionBlockManagement.connect(this, this, ConnectionEntry.TRAJECTORY_BLOCK_max_velocity.toString(), minimumDouble, ConnectionEntry.MINIMUM_DOUBLE_double_1.toString());
        functionBlockManagement.connect(this, velocityEstimation, ConnectionEntry.VELOCITY_ESTIMATION_velocity.toString(), minimumDouble, ConnectionEntry.MINIMUM_DOUBLE_double_2.toString());

        //connections to this
        functionBlockManagement.connect(this, minimumDouble, ConnectionEntry.MINIMUM_DOUBLE_minimum.toString(), this, ConnectionEntry.TRAJECTORY_BLOCK_velocity.toString());
    }
}
