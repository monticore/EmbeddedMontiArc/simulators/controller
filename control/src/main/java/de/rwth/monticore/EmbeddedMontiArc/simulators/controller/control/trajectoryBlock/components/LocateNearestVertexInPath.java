/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.trajectoryBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph on 07.03.2017.
 */
public class LocateNearestVertexInPath extends FunctionBlock {

    //Input Arguments
    List<Vertex> path;
    RealVector sensorGPSCoordinates;

    //Output Arguments
    Vertex nearestVertex;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a VelocityLogic object
     */
    public LocateNearestVertexInPath() {
        initComponents();
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/
    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        nearestVertex = locateNearestVertex(path, sensorGPSCoordinates);
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        path = (List<Vertex>) inputs.get(ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_path.toString());
        sensorGPSCoordinates = (RealVector) inputs.get(ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_gps_coordinates.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_nearest_vertex.toString(), nearestVertex);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[]{
                ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_path.toString(),
                ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_gps_coordinates.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {
    }

    public Vertex locateNearestVertex(List<Vertex> path, RealVector sensorGPSCoordinates) {
        Vertex nearest = path.get(0);
        for (Vertex vertex : path) {
            if (vertex.getPosition().getDistance(sensorGPSCoordinates) < nearest.getPosition().getDistance(sensorGPSCoordinates)) {
                nearest = vertex;
            }
        }
        return nearest;
    }

}
