/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.trajectoryBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockParameter;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * this function block calculates the distance which we have to foresee in order to see if we have to brake e.g.
 * because of the form of the trajectory or because of an object that prohibits us to follow the street normally.
 *
 * used formulas:
 *      estimatedDistance: a = ((v_cur)^2 - (v_new)^2) / (2 * s) => s = ((v_cur)^2 - (v_new)^2) / (2 * a), where v_new = 0.
 *      estimatedDistance: s = 1.5 * s, as a safety distance.
 *
 * Created by Christoph Grüne on 13.01.2017.
 */
public class DistanceEstimation extends FunctionBlock {

    //Input Variables
    private Double currentVelocity;
    private Double brakesMaxAcceleration;

    //Output Variables
    private Double estimatedDistance;

    //Global Variables

    //Used Function Blocks

    /**
     * Constructor for a InnerControlBlock object
     */
    public DistanceEstimation() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        estimatedDistance = calculateEstimatedDistance(currentVelocity, brakesMaxAcceleration);
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        currentVelocity = (Double) inputs.get(ConnectionEntry.DISTANCE_ESTIMATION_current_velocity.toString());
        brakesMaxAcceleration = (Double) inputs.get(ConnectionEntry.DISTANCE_ESTIMATION_brakes_max_acceleration.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.DISTANCE_ESTIMATION_estimated_distance.toString(), estimatedDistance);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.DISTANCE_ESTIMATION_current_velocity.toString(),
                ConnectionEntry.DISTANCE_ESTIMATION_brakes_max_acceleration.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * function to calculate the needed distance
     *
     * @param sensorVelocity the current velocity
     * @param constantBrakesMaxAcceleration the maximal brake acceleration
     * @return the estimated distance
     */
    private Double calculateEstimatedDistance(Double sensorVelocity, Double constantBrakesMaxAcceleration) {
        double s = (sensorVelocity * sensorVelocity) / (2 * constantBrakesMaxAcceleration);
        s = s * FunctionBlockParameter.DISTANCE_ESTIMATION_multiplication.getParameter() + FunctionBlockParameter.DISTANCE_ESTIMATION_offset.getParameter();//use safety distance
        return s;
    }
}
