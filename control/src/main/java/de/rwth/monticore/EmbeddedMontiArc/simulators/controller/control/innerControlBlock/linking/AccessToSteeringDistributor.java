/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.linking;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.AccessToFunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

/**
 * Further information in the abstract super class
 * Created by s.erlbeck on 27.12.2016.
 */
@Deprecated
public class AccessToSteeringDistributor extends AccessToFunctionBlock {

    //input blocks
    FunctionBlock parent;
    FunctionBlock steeringController;

    /**
     * Further information in the abstract super class
     */
//    public AccessToSteeringDistributor() {
//        connectionMap = new LinkedHashMap<String, Object>();
//        connectionMap.put("ANGLE_PID", 0.0d);
//    }

    /**
     * connects function blocks to the other function block
     *
     * @param steeringController is a SteeringController function block
     */
//    public void connect(FunctionBlock parent, FunctionBlock steeringController) {
//        this.parent = parent;
//        this.steeringController = steeringController;
//    }

    /**
     * Further information in the abstract super class
     */
//    public void collectAllInputs() {
//        connectionMap.put("ANGLE_PID", steeringController.getOutputs().get("ANGLE_PID"));
//    }
}
