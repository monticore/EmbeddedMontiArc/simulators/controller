/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * component VelocityDistributor {
 *     ports    in  Double velocityPID,
 *              out Integer actuatorGearValue,
 *              out Double actuatorEngineValue,
 *              out Double actuatorBrakeValue;
 * }
 */

/**
 * This function block uses the results of the PID to calculate the values for the actuators engine, brake and gear.
 * It uses the new velocity given by the PID to calculate the next acceleration of the brakes and engine and the next gear.
 *
 * used formulas:
 *      engine: v = a * t => a = v / t
 *      brake: v = -a * t => a = -v / t
 *      gear: v / (v_max + 0.00001 / #gears) + 1. Therefor, only a gear in [0, #gears] is used. They are distributed uniformly.
 *
 * used function blocks: none
 *
 * Created by Christoph Grüne on 05.12.2016.
 */
public class VelocityDistributor extends FunctionBlock {

    //Input Arguments
    private Double velocityPID;
    private Double currentVelocity;
    private Double maximumVelocity;
    private Integer numberOfGears;
    private Double motorMaxAcceleration;
    private Double brakeMaxAcceleration;

    //Output Arguments
    private Integer actuatorGearValue;
    private Double actuatorEngineValue;
    private Double actuatorBrakeValue;

    //Global Variables

    /**
     * Constructor for a VelocityDistributor object
     */
    public VelocityDistributor() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/
    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        actuatorGearValue = calculateGear();
        actuatorEngineValue = calculateEngineValue(timeDelta);
        actuatorBrakeValue = calculateBrakeValue(timeDelta);
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        velocityPID = (Double) inputs.get(ConnectionEntry.VELOCITY_DISTRIBUTOR_velocity_pid.toString());
        currentVelocity = (Double) inputs.get(ConnectionEntry.VELOCITY_DISTRIBUTOR_current_velocity.toString());
        maximumVelocity = (Double) inputs.get(ConnectionEntry.VELOCITY_DISTRIBUTOR_maximum_velocity.toString());
        numberOfGears = (Integer) inputs.get(ConnectionEntry.VELOCITY_DISTRIBUTOR_number_of_gears.toString());
        motorMaxAcceleration = (Double) inputs.get(ConnectionEntry.VELOCITY_DISTRIBUTOR_motor_max_acceleration.toString());
        brakeMaxAcceleration = (Double) inputs.get(ConnectionEntry.VELOCITY_DISTRIBUTOR_brake_max_acceleration.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.VELOCITY_DISTRIBUTOR_actuator_gear_value.toString(), actuatorGearValue);
        outputs.put(ConnectionEntry.VELOCITY_DISTRIBUTOR_actuator_engine_value.toString(), actuatorEngineValue);
        outputs.put(ConnectionEntry.VELOCITY_DISTRIBUTOR_actuator_brake_value.toString(), actuatorBrakeValue);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.VELOCITY_DISTRIBUTOR_velocity_pid.toString(),
                ConnectionEntry.VELOCITY_DISTRIBUTOR_current_velocity.toString(),
                ConnectionEntry.VELOCITY_DISTRIBUTOR_maximum_velocity.toString(),
                ConnectionEntry.VELOCITY_DISTRIBUTOR_number_of_gears.toString(),
                ConnectionEntry.VELOCITY_DISTRIBUTOR_motor_max_acceleration.toString(),
                ConnectionEntry.VELOCITY_DISTRIBUTOR_brake_max_acceleration.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * function to calculate the actuatorGearValue
     *
     * @return the momentary actuatorGearValue that is needed
     */
    private Integer calculateGear() {
        //simple function because we have no engine speed
        return (Integer) (int) (currentVelocity / (maximumVelocity + 0.00001 / numberOfGears) + 1);
    }

    /**
     * calculates the value given to the engine actuator using the value put out by the controller
     *
     * @return new engine actuator value
     */
    private Double calculateEngineValue(double timeDelta) {
        Double a = velocityPID / timeDelta;
        return calculateValue(a, motorMaxAcceleration);
    }

    /**
     * calculates the value given to the brake actuator using the value put out by the controller
     *
     * @return new brake actuator value
     */
    private Double calculateBrakeValue(double timeDelta) {
        Double a = -velocityPID / timeDelta;
        return calculateValue(a, brakeMaxAcceleration);
    }

    /**
     * constrains a value to the interval [0, maxValue]
     *
     * @param value the value to be constrained to the interval [0, maxValue]
     * @param maxValue the maximal Value
     * @return the value constrained to the interval [0, maxValue]
     */
    private Double calculateValue(Double value, Double maxValue) {
        if(value > maxValue) {
            return maxValue;
        } else if(value <= 0) {
            return 0.0;
        } else {
            return value;
        }
    }
}
