/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.linking;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.AccessToFunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

/**
 * Further information in the abstract super class
 * Created by s.erlbeck on 27.12.2016.
 */
@Deprecated
public class AccessToVelocityDistributor extends AccessToFunctionBlock {

    //input blocks
    FunctionBlock parent;
    FunctionBlock velocityController;

    /**
     * Further information in the abstract super class
     */
    public AccessToVelocityDistributor() {
//        connectionMap = new LinkedHashMap<String, Object>();
//        connectionMap.put("VELOCITY_PID", 0.0d);
    }

//    /**
//     * connects function blocks to the other function block
//     *
//     * @param velocityController is a SteeringController function block
//     */
//    public void connect(FunctionBlock parent, FunctionBlock velocityController) {
//        this.parent = parent;
//        this.velocityController = velocityController;
//    }
//
//    /**
//     * Further information in the abstract super class
//     */
//    public void collectAllInputs() {
//        connectionMap.put("VELOCITY_PID", velocityController.getOutputs().get("VELOCITY_PID"));
//    }

}
