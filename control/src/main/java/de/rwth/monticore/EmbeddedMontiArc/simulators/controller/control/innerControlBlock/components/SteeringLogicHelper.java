/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.*;

/**
 * Created by s.erlbeck on 28.02.2017.
 */
public class SteeringLogicHelper extends FunctionBlock {

    //Input Arguments get from sensor
    private Double currentVelocity;
    private Double distanceToRight;
    private Double distanceToLeft;
    //assuming the CV returns an angle in radians with counter-clockwise orientation stored as double
    private Double vanishingAngle;
    //this angle is in range [0, 2*PI]
    private Double globalOrientation;
    private List<Vertex> vertexPath;
    private RealVector gpsCoordinates;

    //Output Arguments
    private Double steeringAngle;

    //Global Variables
    private List<RealVector> vectorPath = null;



    /**
     * Constructor for a SteeringLogicHelper object
     */
    public SteeringLogicHelper() {}


    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        //we want to steer the arithmetic mean between the angle towards the vanishing point
        //and the angle needed to put the car back on the trajectory
        double angleTowardsTrajectory = this.calculateAngleTowardsTrajectory();

        if(angleTowardsTrajectory > Math.PI)
            angleTowardsTrajectory -= 2*Math.PI;
        else if(angleTowardsTrajectory < -Math.PI)
            angleTowardsTrajectory += 2*Math.PI;


        //currently unused: keeping the car on the street
        //double angleOnStreet = this.calculateStreetDirectionWithSideDistanceSensors();
        //steeringAngle = 0.5 * (angleTowardsTrajectory + angleOnStreet);
        steeringAngle = angleTowardsTrajectory;
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        currentVelocity = (Double) inputs.get(ConnectionEntry.STEERING_LOGIC_HELPER_current_velocity.toString());
        distanceToLeft = (Double) inputs.get(ConnectionEntry.STEERING_LOGIC_HELPER_distance_to_left.toString());
        distanceToRight = (Double) inputs.get(ConnectionEntry.STEERING_LOGIC_HELPER_distance_to_right.toString());
        vanishingAngle = (Double) inputs.get(ConnectionEntry.STEERING_LOGIC_HELPER_vanishing_angle.toString());
        vertexPath = (List<Vertex>) inputs.get(ConnectionEntry.STEERING_LOGIC_HELPER_path.toString());
        gpsCoordinates = (RealVector) inputs.get(ConnectionEntry.STEERING_LOGIC_HELPER_gps_coordinates.toString());
        globalOrientation = (Double) inputs.get(ConnectionEntry.STEERING_LOGIC_HELPER_global_orientation.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.STEERING_LOGIC_HELPER_steering_angle.toString(), steeringAngle);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.STEERING_LOGIC_HELPER_current_velocity.toString(),
                ConnectionEntry.STEERING_LOGIC_HELPER_distance_to_left.toString(),
                ConnectionEntry.STEERING_LOGIC_HELPER_distance_to_right.toString(),
                ConnectionEntry.STEERING_LOGIC_HELPER_vanishing_angle.toString(),
                ConnectionEntry.STEERING_LOGIC_HELPER_path.toString(),
                ConnectionEntry.STEERING_LOGIC_HELPER_gps_coordinates.toString(),
                ConnectionEntry.STEERING_LOGIC_HELPER_global_orientation.toString()
        };
        return names;
    }





    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/

    /***************************************************************************
     * these functions calculates the steering needed to get back on the
     *      trajectory
     * these functions are currently used
     * functions written by Stefan Erlbeck
     ***************************************************************************/

    private double calculateAngleTowardsTrajectory() {

        vectorPath = transformVertexToVector(vertexPath);

        //transform global orientation
        if(globalOrientation > Math.PI) {
            globalOrientation -= 2 * Math.PI;
        }

        if(vectorPath.size() == 0) {
            //nothing to do here, there is no trajectory
            return 0;
        }
        if(vectorPath.size() == 1) {
            RealVector desiredDirection = vectorPath.get(0).copy().subtract(gpsCoordinates);
            double desiredAngle = this.calculateGlobalOrientation(desiredDirection);

            // Avoid invalid computations
            if (Double.isNaN(desiredAngle)) {
                return 0.0;
            }
            
            return desiredAngle - globalOrientation;
        }

        RealVector[] positions = this.getPositionsOfTrajectory(vectorPath);
        RealVector firstPos = positions[0];
        RealVector secondPos = positions[1];

        double orientedDistance = this.calculateDistanceFromTrajectory(firstPos,secondPos);

        //we want to correct the offset of the car from the trajectory in one second (assuming no angle to vanishing point)
        //thus, the hypotenuses is the current velocity
        //with the orientation from calculateDistanceFromTrajectory(), we can directly use arctan to get the angle
        //in counter-clockwise orientation (no sign-flipping needed)
        double angleTowardsTrajectory = Math.atan(orientedDistance / this.setDistanceInFront(currentVelocity));

        //until now we ignored the angle between car direction and trajectory
        double orientationOfTrajectory = this.calculateGlobalOrientation(secondPos.copy().subtract(firstPos));
        double angleTrajectoryAndCarDirection = orientationOfTrajectory - globalOrientation;

        //the resulting angle is the angle needed to steer the car parallel to the trajectory
        // plus the angle towards the trajectory
        return angleTrajectoryAndCarDirection + angleTowardsTrajectory;
    }


    /**
     * transforms the vectorPath of vertices to a vectorPath of coordinates
     * @param l the vectorPath with the vertices
     * @return the vectorPath of coordinates
     */
    private List<RealVector> transformVertexToVector(List<Vertex> l) {
        List<RealVector> res = new LinkedList<RealVector>();

        for(Vertex v : l) {
            res.add(v.getPosition());
        }

        return res;
    }

    /**
     * takes a list of vectors and finds the nearest position to the car and its successor
     * @param path the list of vectors
     * @return the vector which comes first in path at index 0 and the other vector at index 1
     * @throws IllegalArgumentException iff path is null or has a length smaller than 2
     * @throws IllegalStateException if the nearest position could not be found although the input is correct
     *                               (occurrence most likely means a bug in this function)
     */
    private RealVector[] getPositionsOfTrajectory(List<RealVector> path) {

        if(path == null || path.size() < 2) {
            throw new IllegalArgumentException("Path is null or has insufficient length.");
        }

        RealVector firstPos = null;
        int indexOfFirstPos = -1;
        int i = 0;
        double currentDistance = -1.0;
        double closestDistance = Double.MAX_VALUE;

        //search closest point on trajectory
        for(RealVector v : path) {
            currentDistance = gpsCoordinates.getDistance(v);
            if(closestDistance > currentDistance) {
                firstPos = v.copy();
                indexOfFirstPos = i;
                closestDistance = currentDistance;
            }
            i++;
        }

        if(firstPos == null || indexOfFirstPos == -1 || currentDistance == -1.0) {
            throw new IllegalStateException("Error: Did not find the closest position of the car on the trajectory.");
        }


        RealVector secondPos;
        if (indexOfFirstPos == path.size() - 1) {
            //car is in the last segment
            //in this case we need to swap firstPos and secondPos to get the right orientation of the distance
            secondPos = firstPos;
            firstPos = path.get(indexOfFirstPos - 1).copy();
        } else {
            secondPos = path.get(indexOfFirstPos + 1).copy();
        }

        return new RealVector[] {firstPos, secondPos};
    }

    /**
     * never ever touch again!!!
     * this function calculates the oriented distance from the car's position to the trajectory
     * oriented means that the distance is negative if and only if the car is on the left side
     *      of the trajectory
     * @param firstPos the point of the trajectory the car already passed
     * @param secondPos the next point of the trajectory the car aims for
     * @return the oriented distance
     */
    private double calculateDistanceFromTrajectory(RealVector firstPos, RealVector secondPos) {
        //calculate the distance between trajectory through the two points and the coordinates of the car
        //using the formula on "https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line"

        double res = (secondPos.getEntry(1)  - firstPos.getEntry(1)) * gpsCoordinates.getEntry(0);
        res -= (secondPos.getEntry(0) - firstPos.getEntry(0)) * gpsCoordinates.getEntry(1);
        res += secondPos.getEntry(0) * firstPos.getEntry(1);
        res -= secondPos.getEntry(1) * firstPos.getEntry(0);
        res /= firstPos.getDistance(secondPos);

        //now by definition of firstPost and secondPos, res is the distance with a negative sign
        //iff the car is on the left side of the trajectory
        return res;
    }


    /**
     * calculates the angle in xy-plane between the y-Axis (0, 1) and the given vector
     * @param v the vector
     * @return the counter-clockwise angle in radians FROM the y-Axis to the given vector, range is [-Pi, Pi]
     * @throws IllegalArgumentException iff v has less than two dimensions
     */
    private double calculateGlobalOrientation(RealVector v) {
        int n = v.getDimension();

        //check dimension
        if(n < 2) {
            throw new IllegalArgumentException("Vector must have at least two components to compute global orientation.");
        }

        double v1 = v.getEntry(0);
        double v2 = v.getEntry(1);
        //cosine of angle between v and (0, 1)
        double cosineAngle = v2 / Math.sqrt(v1 * v1 + v2 * v2);
        //angle in range [0, PI]
        double angle = Math.acos(cosineAngle);

        if(v1 > 0) {
            //if the first component of v is greater 0, then v lies on the right side of (0, 1)
            // so we have a negative angle assuming counter-clockwise orientation
            return -angle;
        } else {
            //if the first component of v is not greater 0, then v lies on the left side of (0, 1)
            // so we have a positive angle assuming counter-clockwise orientation
            return angle;
        }
    }


    /***************************************************************************
     * this function calculates the angle between car and street
     *      by using the detected vanishing point from the CV module
     * this function is currently unused
     * function written by Stefan Erlbeck
     ***************************************************************************/
    private double calculateStreetDirectionWithVanishingPoint() {
        //nothing to do here as the computer vision module already calculates the needed angle
        return vanishingAngle;
    }


    /***************************************************************************
     * this function calculates the angle between car and street
     *      by using the sensors which measure the distance between the car
     *      and the lane marks at the sides of the road
     * this function is currently used
     * algorithm idea by Xiangwei Wang
     ***************************************************************************/

    private double calculateStreetDirectionWithSideDistanceSensors() {
        //opposite side of the triangle is the way from the car to the middle of the road
        double oppositeSide = (distanceToLeft + distanceToRight) / 2.0 - distanceToRight;
        //same as with trajectory, we want to correct during one second
        //=> hypotenuses is current velocity
        double angleInTangent = oppositeSide / this.setDistanceInFront(currentVelocity);

        //no sign-flipping needed
        return Math.atan(angleInTangent);
    }


    /**
     * This function determines the distance during which the car's position shall be corrected, e.g. corrected
     *      towards the middle of the street
     */
    public Double setDistanceInFront(Double sensorCurrentVelocity) {
        Double distanceInFront = sensorCurrentVelocity;
        return 2.0;
    }
}
