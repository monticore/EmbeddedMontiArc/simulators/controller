/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.linking;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.AccessToFunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

/**
 * Further information in the abstract super class
 * Created by s.erlbeck on 27.12.2016.
 */
@Deprecated
public class AccessToSteeringLogic extends AccessToFunctionBlock {

    //input blocks
    FunctionBlock parent;

    /**
     * Further information in the abstract super class
     */
//    public AccessToSteeringLogic() {
//        connectionMap = new LinkedHashMap<String, Object>();
//        connectionMap.put("VANISHING_POINT_ANGLE", 0.0d);
//        connectionMap.put("DISTANCE_TO_LEFT", 0.0d);
//        connectionMap.put("DISTANCE_TO_RIGHT", 0.0d);
//    }

//    /**
//     * connects function blocks to the other function block
//     */
//    public void connect(FunctionBlock parent) {
//        this.parent = parent;
//    }
//
//    /**
//     * Further information in the abstract super class
//     */
//    public void collectAllInputs() {
//        connectionMap.put("VANISHING_POINT_ANGLE", parent.getInputs().get("VANISHING_POINT_ANGLE"));
//        connectionMap.put("DISTANCE_TO_LEFT", parent.getInputs().get("DISTANCE_TO_LEFT"));
//        connectionMap.put("DISTANCE_TO_RIGHT", parent.getInputs().get("DISTANCE_TO_RIGHT"));
//    }
}
