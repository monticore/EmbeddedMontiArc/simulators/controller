/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.linking;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.AccessToFunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

/**
 * Further information in the abstract super class
 * Created by s.erlbeck on 27.12.2016.
 */
@Deprecated
public class AccessToVelocityController extends AccessToFunctionBlock {

    //input blocks
    FunctionBlock parent;
    FunctionBlock velocityLogic;
    FunctionBlock velocitySensor;

    /**
     * Further information in the abstract super class
     */
    public AccessToVelocityController() {
//        this.velocityLogic = velocityLogic;
//        this.velocitySensor = velocitySensor;
//
//        connectionMap = new LinkedHashMap<String, Object>();
//        //TODO change this to in1 and in2???
//        connectionMap.put("DESIRED_VALUE", 0.0d);
//        connectionMap.put("SENSOR_VALUE", 0.0d);
    }

//    /**
//     * connects function blocks to the other function block
//     *
//     * @param velocityLogic is a SteeringController function block
//     */
//    public void connect(FunctionBlock parent, FunctionBlock velocityLogic) {
//        this.parent = parent;
//        this.velocityLogic = velocityLogic;
//    }
//
//    /**
//     * Further information in the abstract super class
//     */
//    public void collectAllInputs() {
//        //TODO change this to in1 and in2???
//        connectionMap.put("DESIRED_VALUE", velocityLogic.getOutputs().get("DESIRED_VELOCITY"));
//        connectionMap.put("SENSOR_VALUE", parent.getInputs().get("VELOCITY_SENSOR_VALUE"));
//    }

}
