/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.AbsoluteMaximumDouble;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.MinimumDouble;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.MaximumVelocityBySteeringAnAngle;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * component SteeringDistributor {
 *     ports    in  Double sensorSteering,
 *              in  Double hazardVelocity,
 *              in  Double signVelocity,
 *              in  Double trajectoryVelocity;
 *              out Double desiredVelocity;
 * }
 */

/**
 * This function block calculates the next velocity we want to drive.
 * For this, we use the new steering angle which is calculated by the steering logic and the velocities calculated by the
 * hazardBlock, signBlock and trajectoryBlock. We take the minimum of them.
 *
 * used formulas
 *      velocity = a * exp(b * steeringAngle), a and b as specified in surface
 *      desiredVelocity = min(velocities), where velocities is the set over all calculates velocities (hazard, sign, trajectory and steering)
 *
 * used function blocks:
 *      MaximumVelocityBySteeringAnAngle
 *      MinimumDouble
 *      MaximumDouble
 *
 * Created by Christoph Grüne on 04.12.2016.
 */
public class VelocityLogic extends FunctionBlock {

    //Input Arguments
    private Surface currentSurface;
    private Double currentWeather;
    private Double constantMaximumVelocity;
    private Double hazardVelocity;
    private Double signVelocity;
    private Double trajectoryVelocity;
    private Double vehicleMaxTemporaryAllowedVelocity;
    private Double newSteeringAngle;
    private Double currentSteeringAngle;

    //Output Arguments
    private Double desiredVelocity;

    //Global Arguments

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a VelocityLogic object
     */
    public VelocityLogic() {
        initComponents();
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/
    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        Map<String, Object> outputs = functionBlockManagement.executeAllFunctionBlocks(timeDelta);
        desiredVelocity = (Double) outputs.get(ConnectionEntry.VELOCITY_LOGIC_desired_velocity.toString());
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        currentSurface = (Surface) inputs.get(ConnectionEntry.VELOCITY_LOGIC_current_surface.toString());
        currentWeather = (Double) inputs.get(ConnectionEntry.VELOCITY_LOGIC_current_weather.toString());
        constantMaximumVelocity = (Double) inputs.get(ConnectionEntry.VELOCITY_LOGIC_max_velocity.toString());
        hazardVelocity = (Double) inputs.get(ConnectionEntry.VELOCITY_LOGIC_hazard_velocity.toString());
        signVelocity = (Double) inputs.get(ConnectionEntry.VELOCITY_LOGIC_sign_velocity.toString());
        trajectoryVelocity = (Double) inputs.get(ConnectionEntry.VELOCITY_LOGIC_trajectory_velocity.toString());
        vehicleMaxTemporaryAllowedVelocity = (Double) inputs.get(ConnectionEntry.VELOCITY_LOGIC_vehicle_max_temporary_allowed_velocity.toString());
        newSteeringAngle = (Double) inputs.get(ConnectionEntry.VELOCITY_LOGIC_new_steering_angle.toString());
        currentSteeringAngle = (Double) inputs.get(ConnectionEntry.VELOCITY_LOGIC_current_steering_angle.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.VELOCITY_LOGIC_desired_velocity.toString(), desiredVelocity);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[]{
                BusEntry.CONSTANT_MAXIMUM_TOTAL_VELOCITY.toString(),
                ConnectionEntry.VELOCITY_LOGIC_hazard_velocity.toString(),
                ConnectionEntry.VELOCITY_LOGIC_sign_velocity.toString(),
                ConnectionEntry.VELOCITY_LOGIC_trajectory_velocity.toString(),
                ConnectionEntry.VELOCITY_LOGIC_vehicle_max_temporary_allowed_velocity.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {
        functionBlockManagement = new FunctionBlockManagement(this);

        FunctionBlock absoluteMaximumDouble = new AbsoluteMaximumDouble();
        functionBlockManagement.addFunctionBlock(absoluteMaximumDouble);

        FunctionBlock maximumVelocityBySteeringAnAngle = new MaximumVelocityBySteeringAnAngle();
        functionBlockManagement.addFunctionBlock(maximumVelocityBySteeringAnAngle);

        FunctionBlock minimumDouble1 = new MinimumDouble();
        functionBlockManagement.addFunctionBlock(minimumDouble1);

        FunctionBlock minimumDouble2 = new MinimumDouble();
        functionBlockManagement.addFunctionBlock(minimumDouble2);

        FunctionBlock minimumDouble3 = new MinimumDouble();
        functionBlockManagement.addFunctionBlock(minimumDouble3);

        FunctionBlock minimumDouble4 = new MinimumDouble();
        functionBlockManagement.addFunctionBlock(minimumDouble4);

        FunctionBlock minimumDouble5 = new MinimumDouble();
        functionBlockManagement.addFunctionBlock(minimumDouble5);

        //connections to maximumDouble
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_LOGIC_new_steering_angle.toString(), absoluteMaximumDouble, ConnectionEntry.ABSOLUTE_MAXIMUM_DOUBLE_double_1.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_LOGIC_current_steering_angle.toString(), absoluteMaximumDouble, ConnectionEntry.ABSOLUTE_MAXIMUM_DOUBLE_double_2.toString());

        //connections to maximumVelocityBySteeringAnAngle
        functionBlockManagement.connect(this, absoluteMaximumDouble, ConnectionEntry.ABSOLUTE_MAXIMUM_DOUBLE_maximum.toString(), maximumVelocityBySteeringAnAngle, ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_steering_angle.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_LOGIC_current_surface.toString(), maximumVelocityBySteeringAnAngle, ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_surface.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_LOGIC_current_weather.toString(), maximumVelocityBySteeringAnAngle, ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_weather.toString());

        //connections to minimumDouble1
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_LOGIC_max_velocity.toString(), minimumDouble1, ConnectionEntry.MINIMUM_DOUBLE_double_1.toString());
        functionBlockManagement.connect(this, maximumVelocityBySteeringAnAngle, ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_velocity.toString(), minimumDouble1, ConnectionEntry.MINIMUM_DOUBLE_double_2.toString());

        //connections to minimumDouble2
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_LOGIC_hazard_velocity.toString(), minimumDouble2, ConnectionEntry.MINIMUM_DOUBLE_double_1.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_LOGIC_sign_velocity.toString(), minimumDouble2, ConnectionEntry.MINIMUM_DOUBLE_double_2.toString());

        //connections to minimumDouble3
        functionBlockManagement.connect(this, minimumDouble1, ConnectionEntry.MINIMUM_DOUBLE_minimum.toString(), minimumDouble3, ConnectionEntry.MINIMUM_DOUBLE_double_1.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_LOGIC_trajectory_velocity.toString(), minimumDouble3, ConnectionEntry.MINIMUM_DOUBLE_double_2.toString());

        //connections to minimumDouble4
        functionBlockManagement.connect(this, minimumDouble2, ConnectionEntry.MINIMUM_DOUBLE_minimum.toString(), minimumDouble4, ConnectionEntry.MINIMUM_DOUBLE_double_1.toString());
        functionBlockManagement.connect(this, minimumDouble3, ConnectionEntry.MINIMUM_DOUBLE_minimum.toString(), minimumDouble4, ConnectionEntry.MINIMUM_DOUBLE_double_2.toString());

        //connections to minimumDouble5
        functionBlockManagement.connect(this, this, ConnectionEntry.VELOCITY_LOGIC_vehicle_max_temporary_allowed_velocity.toString(), minimumDouble5, ConnectionEntry.MINIMUM_DOUBLE_double_1.toString());
        functionBlockManagement.connect(this, minimumDouble4, ConnectionEntry.MINIMUM_DOUBLE_minimum.toString(), minimumDouble5, ConnectionEntry.MINIMUM_DOUBLE_double_2.toString());

        //connections to this
        functionBlockManagement.connect(this, minimumDouble5, ConnectionEntry.MINIMUM_DOUBLE_minimum.toString(), this, ConnectionEntry.VELOCITY_LOGIC_desired_velocity.toString());
    }
}







