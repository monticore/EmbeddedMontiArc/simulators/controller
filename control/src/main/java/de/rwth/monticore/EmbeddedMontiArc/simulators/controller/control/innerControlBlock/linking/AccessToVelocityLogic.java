/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.linking;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.AccessToFunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

/**
 * Further information in the abstract super class
 * Created by s.erlbeck on 27.12.2016.
 */
@Deprecated
public class AccessToVelocityLogic extends AccessToFunctionBlock {

    //input blocks
    FunctionBlock parent;
    FunctionBlock steeringLogic;

    /**
     * Further information in the abstract super class
     */
    public AccessToVelocityLogic() {
//        connectionMap = new LinkedHashMap<String, Object>();
//        connectionMap.put("DESIRED_STEERING_ANGLE", 0.0d);
    }

//    /**
//     * connects function blocks to the other function block
//     *
//     * @param steeringLogic is a SteeringController function block
//     */
//    public void connect(FunctionBlock parent, FunctionBlock steeringLogic) {
//        this.parent = parent;
//        this.steeringLogic = steeringLogic;
//    }
//
//    /**
//     * Further information in the abstract super class
//     */
//    public void collectAllInputs() {
//        connectionMap.put("DESIRED_STEERING_ANGLE", steeringLogic.getOutputs().get("DESIRED_STEERING_ANGLE"));
//    }

}
