/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.linking;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.AccessToFunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

/**
 * Further information in the abstract super class
 * Created by s.erlbeck on 27.12.2016.
 */
@Deprecated
public class AccessToSteeringController extends AccessToFunctionBlock {

    //input blocks
    FunctionBlock parent;
    FunctionBlock steeringLogic;

    /**
     * Further information in the abstract super class
     */
//    public AccessToSteeringController() {
//        connectionMap = new LinkedHashMap<String, Object>();
//        connectionMap.put("DESIRED_VALUE", 0.0d);
//        connectionMap.put("SENSOR_VALUE", 0.0d);
//    }

    /**
     * connects function blocks to the other function block
     *
     * @param steeringLogic is a SteeringLogic function block
     */
//    public void connect(FunctionBlock parent, FunctionBlock steeringLogic) {
//        this.parent = parent;
//        this.steeringLogic = steeringLogic;
//    }

    /**
     * Further information in the abstract super class
     */
//    public void collectAllInputs() {
//        connectionMap.put("DESIRED_VALUE", steeringLogic.getOutputs().get("DESIRED_STEERING_ANGLE"));
//        connectionMap.put("SENSOR_VALUE", parent.getInputs().get("STEERING_SENSOR_VALUE"));
//    }
}
