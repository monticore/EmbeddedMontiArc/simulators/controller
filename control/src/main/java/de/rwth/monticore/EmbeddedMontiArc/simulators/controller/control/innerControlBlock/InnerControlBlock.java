/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockParameter;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.PID.PID;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components.SteeringDistributor;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components.SteeringLogic;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components.VelocityDistributor;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components.VelocityLogic;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * component InnerControlBlock {
 *
 * }
 */
/**
 * this function block calculates the new actuator values for the car by using the outputs of
 * trajectoryBlock, hazardBlock and signBlock.
 *
 * used formulas: only in sub blocks
 *
 * used function blocks:
 *      SteeringLogic
 *      PID (for steering)
 *      SteeringDistributor
 *      VelocityLogic
 *      PID (for velocity)
 *      VelocityDistributor
 *
 * Created by Christoph Grüne on 04.12.2016.
 */
public class InnerControlBlock extends FunctionBlock {

    //Input Variables
    private Double sensorDistanceToLeft;
    private Double sensorDistanceToRight;

    private Double sensorSteering;

    private Surface sensorCurrentSurface;
    private Double sensorWeather;

    private Double sensorVelocity;

    private Double sensorOrientation;

    private RealVector sensorGPSCoordinates;
    private List<Vertex> path;

    private Double angleToVanishingPoint;

    private Double constantMaximumVelocity;
    private Integer constantNumberOfGears;

    private Double constantMinSteeringAngle;
    private Double constantMaxSteeringAngle;
    private Double constantMotorMaxAcceleration;
    private Double constantBrakeMaxAcceleration;
    private Double constantTrajectoryError;
    private Double vehicleMaxTemporaryAllowedVelocity;
    private Double hazardVelocity;
    private Double signVelocity;
    private Double trajectoryVelocity;

    //Output Variables
    private Double actuatorSteering;
    private Double actuatorBrake;
    private Double actuatorEngine;
    private Integer actuatorGear;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public InnerControlBlock() {
        initComponents();
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {

        Map<String, Object> outputs = functionBlockManagement.executeAllFunctionBlocks(timeDelta);

        actuatorSteering = (Double) outputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_steering.toString());
        actuatorBrake = (Double) outputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_brake.toString());
        actuatorEngine = (Double) outputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_engine.toString());
        actuatorGear = (Integer) outputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_gear.toString());
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        sensorDistanceToLeft = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_distance_to_left.toString());
        sensorDistanceToRight = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_distance_to_right.toString());
        sensorSteering = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_steering.toString());
        sensorCurrentSurface = (Surface) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_current_surface.toString());
        sensorWeather = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_weather.toString());
        sensorVelocity = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_velocity.toString());
        constantMaximumVelocity = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_constant_maximum_velocity.toString());
        constantNumberOfGears = (Integer) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_constant_number_of_gears.toString());
        sensorGPSCoordinates = (RealVector) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_gps_coordinates.toString());
        path = (List<Vertex>) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_path.toString());
        angleToVanishingPoint = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_angle_to_vanishing_point.toString());
        sensorOrientation = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_orientation.toString());
        constantMinSteeringAngle = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_constant_min_steering_angle.toString());
        constantMaxSteeringAngle = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_constant_max_steering_angle.toString());
        constantMotorMaxAcceleration = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_constant_motor_max_acceleration.toString());
        constantBrakeMaxAcceleration = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_constant_brake_max_acceleration.toString());
        constantTrajectoryError = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_constant_trajectory_error.toString());
        vehicleMaxTemporaryAllowedVelocity = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_vehicle_max_temporary_allowed_velocity.toString());
        hazardVelocity = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_hazard_velocity.toString());
        signVelocity = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_sign_velocity.toString());
        trajectoryVelocity = (Double) inputs.get(ConnectionEntry.INNER_CONTROL_BLOCK_trajectory_velocity.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_steering.toString(), actuatorSteering);
        outputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_brake.toString(), actuatorBrake);
        outputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_engine.toString(), actuatorEngine);
        outputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_gear.toString(), actuatorGear);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[]   {
                ConnectionEntry.INNER_CONTROL_BLOCK_sensor_distance_to_left.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_sensor_distance_to_right.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_sensor_steering.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_sensor_current_surface.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_sensor_velocity.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_constant_maximum_velocity.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_constant_number_of_gears.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_sensor_gps_coordinates.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_path.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_angle_to_vanishing_point.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_sensor_orientation.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_constant_min_steering_angle.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_constant_max_steering_angle.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_constant_motor_max_acceleration.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_constant_brake_max_acceleration.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_constant_trajectory_error.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_vehicle_max_temporary_allowed_velocity.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_hazard_velocity.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_sign_velocity.toString(),
                ConnectionEntry.INNER_CONTROL_BLOCK_trajectory_velocity.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {

        functionBlockManagement = new FunctionBlockManagement(this);

        FunctionBlock steeringLogic = new SteeringLogic();
        functionBlockManagement.addFunctionBlock(steeringLogic);

        FunctionBlock steeringController = new PID(
                FunctionBlockParameter.STEERING_PID_CONTROLLER_P.getParameter(),
                FunctionBlockParameter.STEERING_PID_CONTROLLER_I.getParameter(),
                FunctionBlockParameter.STEERING_PID_CONTROLLER_D.getParameter()
        );
        functionBlockManagement.addFunctionBlock(steeringController);

        FunctionBlock steeringDistributor = new SteeringDistributor();
        functionBlockManagement.addFunctionBlock(steeringDistributor);

        FunctionBlock velocityLogic = new VelocityLogic();
        functionBlockManagement.addFunctionBlock(velocityLogic);

        FunctionBlock velocityController = new PID(
                FunctionBlockParameter.VELOCITY_PID_CONTROLLER_P.getParameter(),
                FunctionBlockParameter.VELOCITY_PID_CONTROLLER_I.getParameter(),
                FunctionBlockParameter.VELOCITY_PID_CONTROLLER_D.getParameter()
        );
        functionBlockManagement.addFunctionBlock(velocityController);

        FunctionBlock velocityDistributor = new VelocityDistributor();
        functionBlockManagement.addFunctionBlock(velocityDistributor);

        //connections to steeringLogic
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_velocity.toString(), steeringLogic, ConnectionEntry.STEERING_LOGIC_current_velocity.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_distance_to_right.toString(), steeringLogic, ConnectionEntry.STEERING_LOGIC_distance_to_right.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_distance_to_left.toString(), steeringLogic, ConnectionEntry.STEERING_LOGIC_distance_to_left.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_angle_to_vanishing_point.toString(), steeringLogic, ConnectionEntry.STEERING_LOGIC_vanishing_angle.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_orientation.toString(), steeringLogic, ConnectionEntry.STEERING_LOGIC_global_orientation.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_path.toString(), steeringLogic, ConnectionEntry.STEERING_LOGIC_path.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_gps_coordinates.toString(), steeringLogic, ConnectionEntry.STEERING_LOGIC_gps_coordinates.toString());

        //connections to steeringController
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_constant_trajectory_error.toString(), steeringController, ConnectionEntry.PID_desired_value.toString());
        functionBlockManagement.connect(this, steeringLogic, ConnectionEntry.STEERING_LOGIC_steering_angle.toString(), steeringController, ConnectionEntry.PID_current_value.toString());

        //connections to steeringDistributor
        functionBlockManagement.connect(this, steeringController, ConnectionEntry.PID_new_controlled_value.toString(), steeringDistributor, ConnectionEntry.STEERING_DISTRIBUTOR_angle_pid.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_steering.toString(), steeringDistributor, ConnectionEntry.STEERING_DISTRIBUTOR_sensor_steering.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_constant_min_steering_angle.toString(), steeringDistributor, ConnectionEntry.STEERING_DISTRIBUTOR_min_steering_angle.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_constant_max_steering_angle.toString(), steeringDistributor, ConnectionEntry.STEERING_DISTRIBUTOR_max_steering_angle.toString());

        //connections to velocityLogic
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_current_surface.toString(), velocityLogic, ConnectionEntry.VELOCITY_LOGIC_current_surface.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_weather.toString(), velocityLogic, ConnectionEntry.VELOCITY_LOGIC_current_weather.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_constant_maximum_velocity.toString(), velocityLogic, ConnectionEntry.VELOCITY_LOGIC_max_velocity.toString());
        functionBlockManagement.connect(this, steeringLogic, ConnectionEntry.STEERING_LOGIC_steering_angle.toString(), velocityLogic, ConnectionEntry.VELOCITY_LOGIC_new_steering_angle.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_steering.toString(), velocityLogic, ConnectionEntry.VELOCITY_LOGIC_current_steering_angle.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_hazard_velocity.toString(), velocityLogic, ConnectionEntry.VELOCITY_LOGIC_hazard_velocity.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sign_velocity.toString(), velocityLogic, ConnectionEntry.VELOCITY_LOGIC_sign_velocity.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_trajectory_velocity.toString(), velocityLogic, ConnectionEntry.VELOCITY_LOGIC_trajectory_velocity.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_vehicle_max_temporary_allowed_velocity.toString(), velocityLogic, ConnectionEntry.VELOCITY_LOGIC_vehicle_max_temporary_allowed_velocity.toString());

        //connections to velocityController
        functionBlockManagement.connect(this, velocityLogic, ConnectionEntry.VELOCITY_LOGIC_desired_velocity.toString(), velocityController, ConnectionEntry.PID_desired_value.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_velocity.toString(), velocityController, ConnectionEntry.PID_current_value.toString());

        //connections to velocityDistributor
        functionBlockManagement.connect(this, velocityController, ConnectionEntry.PID_new_controlled_value.toString(), velocityDistributor, ConnectionEntry.VELOCITY_DISTRIBUTOR_velocity_pid.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_sensor_velocity.toString(), velocityDistributor, ConnectionEntry.VELOCITY_DISTRIBUTOR_current_velocity.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_constant_maximum_velocity.toString(), velocityDistributor, ConnectionEntry.VELOCITY_DISTRIBUTOR_maximum_velocity.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_constant_number_of_gears.toString(), velocityDistributor, ConnectionEntry.VELOCITY_DISTRIBUTOR_number_of_gears.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_constant_motor_max_acceleration.toString(), velocityDistributor, ConnectionEntry.VELOCITY_DISTRIBUTOR_motor_max_acceleration.toString());
        functionBlockManagement.connect(this, this, ConnectionEntry.INNER_CONTROL_BLOCK_constant_brake_max_acceleration.toString(), velocityDistributor, ConnectionEntry.VELOCITY_DISTRIBUTOR_brake_max_acceleration.toString());

        //connection to this
        functionBlockManagement.connect(this, steeringDistributor, ConnectionEntry.STEERING_DISTRIBUTOR_actuator_steering_value.toString(), this, ConnectionEntry.INNER_CONTROL_BLOCK_actuator_steering.toString());
        functionBlockManagement.connect(this, velocityDistributor, ConnectionEntry.VELOCITY_DISTRIBUTOR_actuator_gear_value.toString(), this, ConnectionEntry.INNER_CONTROL_BLOCK_actuator_gear.toString());
        functionBlockManagement.connect(this, velocityDistributor, ConnectionEntry.VELOCITY_DISTRIBUTOR_actuator_engine_value.toString(), this, ConnectionEntry.INNER_CONTROL_BLOCK_actuator_engine.toString());
        functionBlockManagement.connect(this, velocityDistributor, ConnectionEntry.VELOCITY_DISTRIBUTOR_actuator_brake_value.toString(), this, ConnectionEntry.INNER_CONTROL_BLOCK_actuator_brake.toString());
    }
}
