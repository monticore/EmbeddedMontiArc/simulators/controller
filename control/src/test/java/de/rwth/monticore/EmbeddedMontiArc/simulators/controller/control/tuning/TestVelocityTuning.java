/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.tuning;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.PID.PID;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

/**
 * Testing and tuning a functionblocks.PID that controls the velocity over 1,000,000 microseconds
 * Created by s.erlbeck on 27.12.2016.
 */
public class TestVelocityTuning extends TestCase {

    //default visibility for debug purposes (see TestVelocityHighResolution)
    private HashMap<Long, Double> error;
    private HashMap<Long, Double> desiredValues;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TestVelocityTuning(String testName) {
        super(testName);
        error = new LinkedHashMap<Long, Double>(1000);
        desiredValues = new LinkedHashMap<Long, Double>(1000);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TestVelocityTuning.class);
    }

    public void testVelocityControl() {

        PID velControl = initPID(9000, 0, 0);
        case2();

        HashMap<String, Object> input = new LinkedHashMap<String, Object>(2);
        double desiredCorrection = 0;
        double speed = 0;
        double sensorValue = 0;

        //simulate for 1,000,000 milliseconds, but only every second the error is measured
        for(long time = 0; time < 1000000; time += 1) {
            //log error
            if(time % 1000 == 0) {
                error.put(time, sensorValue - desiredValues.get(time));
            }

            //calculate this step
            input.put(ConnectionEntry.PID_desired_value.toString(), desiredValues.get(time - (time % 1000)));
            input.put(ConnectionEntry.PID_current_value.toString(), sensorValue);

            velControl.setInputs(input);
            velControl.execute(1);
            desiredCorrection = (Double)velControl.getOutputs().get(ConnectionEntry.PID_new_controlled_value.toString());


            //simulate time step
            sensorValue = simulationSensor(speed, desiredCorrection);
            speed = sensorValue;


        }

        try{
            PrintWriter writer = new PrintWriter("error.txt");
            Set<Long> keys = error.keySet();

            for(Long l : keys) {
                writer.println(l + "   " + desiredValues.get(l) + "   " + error.get(l));
            }

            writer.close();
        } catch (IOException e) {
            // do something
            System.out.println("Problem with File.");
        }
        System.out.println(mse());

        assertTrue(true);
    }


    /*****************************
     * helper functions
     * (package visibility so debug class can access)

     *****************************/
    private double mse() {
        Set<Long> keys = error.keySet();
        double res = 0;
        double value;

        for(Long l : keys) {
            value = error.get(l);
            res += value * value;
        }

        return res / keys.size();
    }

    private PID initPID(double k_p, double k_i, double k_d) {
        //time step: one millisecond!!!
        //there is most likely still a mistake here with the delta time
        PID controller = new PID(k_p, k_i, k_d);//new PID(k_p, k_i, k_d, 1000);
        return controller;
    }

    private double simulationSensor(double previousSpeed, double desiredControl) {
        //assuming max acceleration a = 5 m/s^2
        //assuming that for a desired correction of 20 m/s, the full acceleration is used
        //assuming that the previous speed decays with a factor of 0.99

        //calculate how much the control differs from the actual previous speed
        double desiredCorrection = desiredControl - previousSpeed;

        //scaling the correction from [-20, 20] to [-1, 1]
        desiredCorrection /= 20.0;

        //v_new = (scaling * max_acc) * time + damping_factor * v_old

        //time step: one millisecond!!!
        return desiredCorrection * 5.0 / 1000.0 + 0.99*previousSpeed;
    }

    /*****************************
     * maps of desired values
     *****************************/
     private void case1() {
        long time = 0;

        desiredValues.clear();

        for(int i = 0; i < 10; i++) {
            desiredValues.put(time, 5.0);
            time += 1000;
        }

        for(int i = 10; i < 30; i++) {
            desiredValues.put(time, 10.0);
            time += 1000;
        }

        for(int i = 30; i < 500; i++) {
            desiredValues.put(time, 13.0);
            time += 1000;
        }

        for(int i = 500; i < 600; i++) {
            desiredValues.put(time, 0.0);
            time += 1000;
        }

        for(int i = 600; i < 1000; i++) {
            desiredValues.put(time, 9.0);
            time += 1000;
        }
    }

    //debug purpose
    private void case2() {
        long time = 0;

        desiredValues.clear();

        for (int i = 0; i < 1000; i++) {
            desiredValues.put(time, 5.0);
            time += 1000;
        }
    }
}
