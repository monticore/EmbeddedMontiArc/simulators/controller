/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.tuning;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.PID.PID;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;


/**
 * Created by s.erlbeck on 18.01.2017.
 */
public class TestZieglerNicholsTuning extends TestCase {

    //delta time in microseconds!!!
    private final int deltaTime = 100;

    //the precision of the analysis of the function
    private final double eps = 0.01;

    //controllers and car
    private PID velocityPID;
    private PID steeringPID;

    //only mock assignment
    private ControllerTuningCar car;

    //the arrays to save the measured values
    private double[] measuredSpeed;
    private double[] measuredSteering;

    //the results
    private double velocity_k_krit;
    private double velocity_t_krit;
    private double steering_k_krit;
    private double steering_t_krit;


    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TestZieglerNicholsTuning (String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TestZieglerNicholsTuning.class);
    }

    public void testDebug() {

        //only tests steering
        steeringPID = new PID(1, 0, 0);//new PID(1, 0, 0, 10 * deltaTime);
        velocityPID = new PID(0, 0, 0);//new PID(0, 0, 0, 10 * deltaTime);
        measuredSteering = new double[1000];

        List<Integer> maxima = new LinkedList<Integer>();
        List<Integer> minima = new LinkedList<Integer>();

        measureValues(false, true);
        extractExtrema(measuredSteering, maxima, minima);
        analyseExtrema(measuredSteering, maxima, minima);

        printResult("Debug", measuredSteering, maxima, minima);

        assertTrue(true);
    }

    public void testTuneVelocity() {

        System.out.println("Velocity Tuning: ");
        tuningHelper(true);
        assertTrue(true);
    }

    public void testTuneSteering() {

        System.out.println("Steering Tuning: ");
        tuningHelper(false);
        assertTrue(true);
    }

    /**
     * starting here with all k_p = 0, k_i = 0, k_d = 0
     * create the PIDs and call measureValues
     * then call extractExtrema, check whether all extrema are in an epsilon-neighbourhood of the mean value of the extrema
     * if yes -> found k_krit, read period from extrema list
     * if no -> increase k_p, start at second step again
     * @param tuneVelocity true, if velocity shall be tuned and false if steering shall be tuned
     */
    private void tuningHelper(boolean tuneVelocity) {

        //init
        if(tuneVelocity) {
            steeringPID = new PID(0, 0, 0);//new PID(0, 0, 0, 10 * deltaTime);
            measuredSpeed = new double[1000];
        } else {
            velocityPID = new PID(0, 0, 0);//new PID(0, 0, 0, 10 * deltaTime);
            measuredSteering = new double[1000];
        }
        List<Integer> maxima = new LinkedList<Integer>();
        List<Integer> minima = new LinkedList<Integer>();

        double k_p = 0.0;
        int status = 0;
        int maxIter = 10000;
        int i = 0;
        double lowerBound = 0;
        double upperBound = -1;
        boolean foundUpperBound = false;

        do {
            if(tuneVelocity) {
                velocityPID = new PID(k_p, 0, 0);//new PID(k_p, 0, 0, 10 * deltaTime);
                measureValues(true, false);
                extractExtrema(measuredSpeed, maxima, minima);
                status = analyseExtrema(measuredSpeed, maxima, minima);
            } else {
                steeringPID = new PID(k_p, 0, 0);//new PID(k_p, 0, 0, 10 * deltaTime);
                measureValues(false, true);
                extractExtrema(measuredSteering, maxima, minima);
                status = analyseExtrema(measuredSteering, maxima, minima);
            }

            //no upper bound found yet, so we use linear search
            if(!foundUpperBound) {
                if(status < 2) {
                    lowerBound = k_p;
                    k_p++;
                } else if(status == 3){
                    //now we found the upper bound
                    //initialise binary search
                    foundUpperBound = true;
                    upperBound = k_p;
                    k_p = (upperBound - lowerBound) / 2.0;
                }
            } else if(foundUpperBound) {
                if(status < 2) {
                    lowerBound = k_p;
                } else if(status == 3) {
                    upperBound = k_p;
                }
                k_p = (upperBound - lowerBound) / 2.0;
            }

            i++;

            //System.out.println(k_p);
        } while(status != 2 && i < maxIter);

        if(tuneVelocity) {
            printResult("Velocity", measuredSpeed, maxima, minima);
        } else {
            printResult("Steering", measuredSteering, maxima, minima);
        }

        if(i == maxIter) {
            System.out.println("Convergence issues. The optimal k_p could not be found");
            assertTrue(true);
        } else {

            if(tuneVelocity) {
                velocity_k_krit = k_p;

                //assume that the period stays roughly the same
                velocity_t_krit = (maxima.get(1) - maxima.get(0)) * deltaTime;

                System.out.println("k_krit for Velocity: " + velocity_k_krit);
                System.out.println("T_krit for Velocity: " + velocity_t_krit);
            } else {
                steering_k_krit = k_p;

                //assume that the period stays roughly the same
                steering_t_krit = (maxima.get(1) - maxima.get(0)) * deltaTime;

                System.out.println("k_krit for Steering: " + steering_k_krit);
                System.out.println("T_krit for Steering: " + steering_t_krit);

            }

            assertTrue(true);
        }

    }

    /**
     * prints the results in a file
     * @param file the resulting filename will be "ZiNi + file + .txt"
     * @param values the values to print
     * @param maxima the list of maxima
     * @param minima the list of minima
     */
    private void printResult(String file, double values[], List<Integer> maxima, List<Integer> minima) {

        try{
            PrintWriter writer = new PrintWriter("ZiNi" + file + ".txt");

            //iterator init
            Iterator<Integer> maxIterator = maxima.iterator();
            Iterator<Integer> minIterator = minima.iterator();
            int maxIndex = -1;
            int minIndex = -1;
            if(maxIterator.hasNext()) {
                maxIndex = maxIterator.next();
            }
            if(minIterator.hasNext()) {
                minIndex = minIterator.next();
            }

            for(int i = 0; i < values.length; i++) {
                if(maxIndex == i) {
                    writer.println(values[i] + "\t(max)");
                    if(maxIterator.hasNext()) {
                        maxIndex = maxIterator.next();
                    }
                } else if(minIndex == i) {
                    writer.println(values[i] + "\t(min)");
                    if(minIterator.hasNext()) {
                        minIndex = minIterator.next();
                    }
                } else {
                    writer.println(values[i]);
                }
            }

            writer.close();
        } catch (IOException e) {
            // do something, e.g. cry out for help
            System.out.println("Problem with File.");
        }
    }

    /**
     * starts a simulation, depending on what we want to tune
     * creates a new car because MockCar saves old values
     * @param tuningVelocity true, when tuning of the velocity controller is desired
     * @param tuningSteering true, when tuning of the steering controller is desired
     */
    private void measureValues(boolean tuningVelocity, boolean tuningSteering) {
        //point in time in microseconds
        int time = 0;

        //create new car
        car = new MockCar();

        //input for the functionblocks.PID
        HashMap<String, Object> inputVelocity = new LinkedHashMap<String, Object>(2);
        HashMap<String, Object> inputSteering = new LinkedHashMap<String, Object>(2);
        inputVelocity.put(ConnectionEntry.PID_current_value.toString(), 0d);
        inputSteering.put(ConnectionEntry.PID_current_value.toString(), 0d);
        //PID now need delta time in seconds!

        //simulate desiredSpeed = 0, desiredSteering = 0 for 2000 microseconds
        inputVelocity.put(ConnectionEntry.PID_desired_value.toString(), 0d);
        inputSteering.put(ConnectionEntry.PID_desired_value.toString(), 0d);
        time = simulate(time, 2000, inputVelocity, inputSteering, false, false);

        //simulate desiredSpeed = 5, desiredSteering = 0 for 100 milliseconds (!) with the given coefficient
        inputVelocity.put(ConnectionEntry.PID_desired_value.toString(), 5d);
        inputSteering.put(ConnectionEntry.PID_desired_value.toString(), 0d);
        time = simulate(time, 100000, inputVelocity, inputSteering, tuningVelocity, false);

        //if we are not tuning the steering controller, we can abort here
        if(tuningSteering) {
            //simulate desiredSpeed = 5, desiredSteering = PI / 6 for 100 milliseconds (!) with the given coefficients
            inputVelocity.put(ConnectionEntry.PID_desired_value.toString(), 5d);
            inputSteering.put(ConnectionEntry.PID_desired_value.toString(), Math.PI / 6.0);
            time = simulate(time, 100000, inputVelocity, inputSteering, false, true);
        }

    }

    /**
     * simulates the controlling of the car by measuring the sensor values every deltaTime and calling the PIDs ever (10*deltaTime)
     * ConnectionEntry.PID_desired_value must already be set!
     * @param timeStart time when this simulation part starts running
     *                  use the time in microseconds since simulation start (only use multiples of deltaTime!!!)
     * @param timeDuration   time for how long this simulation part runs in microseconds (inclusive)
     *                       (only use multiples of deltaTime!!!)
     * @param inputVelocity the HashMap for the Velocity-functionblocks.PID
     * @param inputSteering the HashMap for the Steering-functionblocks.PID
     * @return (timeStart + timeDuration), the sensor values have not yet been requested
     */
    private int simulate(int timeStart, int timeDuration, HashMap<String, Object> inputVelocity,
                         HashMap<String, Object> inputSteering, boolean traceVelocity, boolean traceSteering) {

        if(timeStart % deltaTime != 0 || timeDuration % deltaTime != 0) {
            throw new IllegalArgumentException("TestZieglerNicholsTuning.simulate(...) got called with wrong arguments. " +
                    "Please only use multiples of deltaTime for timeStart and timeDuration:");
        }

        Double sensorSpeed;
        Double sensorSteering;
        int time;

        for(time = timeStart; time < timeStart + timeDuration; time += deltaTime) {
            //get the sensor values
            sensorSpeed = car.getCurrentSpeed();
            sensorSteering = car.getCurrentSteering();

            //store them while praying to branch prediction for salvation
            if(traceVelocity) {
                measuredSpeed[(time - timeStart) / deltaTime] = sensorSpeed;
            }
            if(traceSteering) {
                measuredSteering[(time - timeStart) / deltaTime] = sensorSteering;
            }

            //functionblocks.PID gets called every millisecond
            //using factor of 10 (Nyquist-Shannon sampling theorem)
            if(time % (10*deltaTime) == 0) {
                //give the sensor values to the functionblocks.PID
                inputVelocity.put(ConnectionEntry.PID_current_value.toString(), sensorSpeed);
                inputSteering.put(ConnectionEntry.PID_current_value.toString(), sensorSteering);
                velocityPID.setInputs(inputVelocity);
                steeringPID.setInputs(inputSteering);

                //execute functionblocks.PID
                velocityPID.execute(10 * deltaTime / 1000.0);
                steeringPID.execute(10 * deltaTime / 1000.0);

                //set the actuators
                car.setCurrentSpeed((Double)velocityPID.getOutputs().get(ConnectionEntry.PID_new_controlled_value.toString()));
                car.setCurrentSteering((Double)steeringPID.getOutputs().get(ConnectionEntry.PID_new_controlled_value.toString()));
            }
        }

        return time;
    }

    /**
     * calculates all local extrema in the array values, assuming the resolution is high enough
     * the lists will be cleared in the beginning
     * @param values the array which extrema we want to examine
     * @param maxima the list, in which the indices of maxima will be stored
     * @param minima the list, in which the indices of minima will be stored
     */
    private void extractExtrema(double[] values, List<Integer> maxima, List<Integer> minima) {

        //clear the lists
        maxima.clear();
        minima.clear();

        /*
        Yeah, this code is probably much longer than needed, but values can be pretty long
        and this code is probably a bit faster than a shortened version
        */

        boolean oldRising = values[1] > values[0];
        boolean oldFalling = values[1] < values[0];
        boolean newRising;
        boolean newFalling;

        for(int i = 1; i < values.length - 1; i++) {
            newRising = values[i + 1] > values[i];
            newFalling = values[i + 1] < values[i];
            if(oldRising) {
                if(newFalling) {
                    //here the values were rising and are now falling, thus we found a local maximum
                    maxima.add(i);
                }
            } else if(oldFalling) {
                if(newRising) {
                    //here the values were falling and are now rising, thus we found a local minimum
                    minima.add(i);
                }
            }
            //as we will consider the following value we can save the current slope behaviour
            oldRising = newRising;
            oldFalling = newFalling;
        }
    }

    /**
     * analyses the function in values with the help of the provided maxima and minima
     * @param values the function
     * @param maxima the list of maxima
     * @param minima the list of minima
     * @return
     *          0 iff function is not oscillating at all
     *          1 iff function oscillates but the oscillation decays
     *          2 iff function oscillates in a stable manner
     *          3 iff function starts to diverge due to oscillation
     */
    private int analyseExtrema(double[] values, List<Integer> maxima, List<Integer> minima) {

        if(maxima.isEmpty() || minima.isEmpty()) {
            //function is not oscillating
            return 0;
        }

        //1. analyse Maxima
        double meanOfMax = 0;
        int lastIndexMax = maxima.get(0);

        //calculate arithmetic mean
        for(int i : maxima) {
            meanOfMax += values[i];

            if(values[lastIndexMax] < values[i]) {
                //oscillation gets too strong over time
                return 3;
            }
            lastIndexMax = i;
        }
        meanOfMax /= maxima.size();

        //check whether all maxima are in an eps neighbourhood of the mean
        for(int i : maxima) {
            if(!(meanOfMax - eps < values[i] && values[i] < meanOfMax + eps)) {
                //oscillation not stable enough (too much decay)
                return 1;
            }
        }


        //1. analyse Minima
        double meanOfMin = 0;
        int lastIndexMin = minima.get(0);

        //calculate arithmetic mean
        for(int i : minima) {
            meanOfMin += values[i];

            if(values[lastIndexMin] > values[i]) {
                //oscillation gets too strong over time
                return 3;
            }
            lastIndexMin = i;
        }
        meanOfMin /= minima.size();

        //check whether all minima are in an eps neighbourhood of the mean
        for(int i : minima) {
            if(!(meanOfMin - eps < values[i] && values[i] < meanOfMin + eps)) {
                //oscillation not stable enough (too much decay)
                return 1;
            }
        }


        //everything seems to be fine then
        return 2;
    }

}
