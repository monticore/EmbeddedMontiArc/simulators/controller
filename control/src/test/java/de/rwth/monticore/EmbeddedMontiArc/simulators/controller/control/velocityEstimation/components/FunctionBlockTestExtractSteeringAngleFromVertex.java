/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.velocityEstimation.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Test for the FunctionBlock ExtractSteeringAngleFromVertex
 *
 * Created by Christoph Grüne on 11.03.2017.
 */
public class FunctionBlockTestExtractSteeringAngleFromVertex extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestExtractSteeringAngleFromVertex(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestExtractSteeringAngleFromVertex.class);
    }

    /**
     * tests if all inputs (see below) are handled correctly:
     *      angle in [-20.0, 20.0]
     */
    public void testExtractSteeringAngleFromVertex() {
        for (double angle = -20.0; angle <= 20.0; angle += 0.1) {
            double result = executeTestCase(new Vertex(0L, 0L, new ArrayRealVector(new Double[] {0.0, 0.0, 0.0}), angle));
            if (result != angle) {
                assertFalse(true);
            }
        }
        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/
    /**
     * this function sets inputs, executes the function block, and returns the outputs
     *
     * @param vertex the vertex form that the angle should be extracted
     * @return the output(s) of the executed function block
     */
    private Double executeTestCase(Vertex vertex) {

        Map<String, Object> inputs = new LinkedHashMap<>();
        inputs.put(ConnectionEntry.EXTRACT_MAX_STEERING_ANGLE_FROM_VERTEX_vertex.toString(), vertex);

        FunctionBlock extractMaxSteeringAngleFromVertex = new ExtractMaxSteeringAngleFromVertex();

        extractMaxSteeringAngleFromVertex.setInputs(inputs);
        extractMaxSteeringAngleFromVertex.execute(1);

        Double result = (Double) extractMaxSteeringAngleFromVertex.getOutputs().get(ConnectionEntry.EXTRACT_MAX_STEERING_ANGLE_FROM_VERTEX_steering_angle.toString());

        return result;
    }
}
