/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.hazardBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by Christoph on 08.03.2017.
 * implemented by Xiangwei Wang on 12.03.2017
 */

// Deprecated because computervision was removed
@Deprecated
public class FunctionBlockTestHazardBlock extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */

    public FunctionBlockTestHazardBlock(String testName)
    {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */

    public static Test suite()
    {
        return new TestSuite(FunctionBlockTestHazardBlock.class);
    }



    public void testHazardBlock()
    {
        Double currentVelocity = 25.0;
        Double brakeMaxAcceleration = 8.0;
        Double distanceToLeft = 8.0;
        Double distanceToRight = 4.0;
        //assume that the time interval is 1s
        Double deltaTime = 1.0;
        //final double widthOfCar = 1.81356;

        double depthForPedestrians = 60.0;
        double depthForCars = 50.0;
        double angleForPedestrians = 0.0;
        double angleForCars = 5.0;
        double maxVelocity = 100.0;

        //mock 6 pedestrians with different angels and depths into the ConnectionEntry
        LinkedList<Object> pedestrians = new LinkedList<>();
        //mock 4 cars with different angels and depths into the ConnectionEntry
        LinkedList<Object> cars = new LinkedList<>();

        Object detectedObject1 = new Object();
        pedestrians.add(detectedObject1);


        Object detectedObject2 = new Object();
        cars.add(detectedObject2);
    /**
       for(int i = 0; i < 3; i++)
        {
            depthForPedestrians -= 10.0;
            angleForPedestrians -= 3.0;
            DetectedObject detectedObject = new DetectedObject(angleForPedestrians, depthForPedestrians);
            pedestrians.add(detectedObject);
        }

        for(int j = 0; j < 2; j++)
        {
            DetectedObject detectedObject = new DetectedObject(angleForCars,depthForCars);
            cars.add(detectedObject);

            depthForCars += 5.0;
            angleForCars += 15.0;
        }

        for(int j = 0; j < 2; j++)
        {
            depthForCars -=2.0;
            angleForCars -=10.0;

            DetectedObject detectedObject = new DetectedObject(angleForCars, depthForCars);
            cars.add(detectedObject);
        }
       */

        Double resultVelocity = executeTestCase(currentVelocity, brakeMaxAcceleration, distanceToLeft, distanceToRight,
                 deltaTime, pedestrians, cars, maxVelocity);

        assertEquals(resultVelocity, 19.612068965517242);


    }

    public Double executeTestCase(Double velocity, Double maximalBrakeAcceleration, Double distanceToLeft, Double distanceToRight,
                                  Double deltaTime, LinkedList<Object> pedestrians,
                                  LinkedList<Object> cars, Double maxVelocity){


        Map<String, Object> inputs = new LinkedHashMap<>();

        inputs.put(ConnectionEntry.HAZARD_BLOCK_current_velocity.toString(), velocity);
        inputs.put(ConnectionEntry.HAZARD_BLOCK_brakes_max_acceleration.toString(), maximalBrakeAcceleration);
        inputs.put(ConnectionEntry.HAZARD_BLOCK_distance_to_left.toString(), distanceToLeft);
        inputs.put(ConnectionEntry.HAZARD_BLOCK_distance_to_right.toString(), distanceToRight);
        inputs.put(BusEntry.SIMULATION_DELTA_TIME.toString(), deltaTime);
        inputs.put(ConnectionEntry.HAZARD_BLOCK_pedestrians.toString(), pedestrians);
        inputs.put(ConnectionEntry.HAZARD_BLOCK_cars.toString(), cars);
        inputs.put(ConnectionEntry.HAZARD_BLOCK_maximum_velocity.toString(), maxVelocity);


        FunctionBlock hazardBlock = new HazardBlock();
        hazardBlock.setInputs(inputs);
        hazardBlock.execute(1);
        Double res = (Double) hazardBlock.getOutputs().get(ConnectionEntry.HAZARD_BLOCK_hazard_velocity.toString());
        return res;
    }

}
