/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.mainControlBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.PathGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This is a test for the FunctionBlock MainControlBlock
 *
 * Created by Christoph on 08.03.2017.
 */
public class FunctionBlockTestMainControlBlock extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestMainControlBlock(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestMainControlBlock.class);
    }

    /**
     * tests if all inputs (see below) are handled correctly:
     *      Surface = Asphalt
     *      constantMaximumVelocity = 100
     *      constantDeltaTime = 0.01
     *      constantNumberOfGears = 1
     *      constantWheelbase = 2.921
     *      constantMotorMaxAcceleration = 3.5
     *      constantMotorMinAcceleration = -1.5
     *      constantBrakesMaxAcceleration = 5.0
     *      constantBrakesMinAcceleration = 0.0
     *      constantSteeringMaxAngle = 0.785398
     *      constantSteeringMinAngle = -0.785398
     *      velocity in [0, 100]
     *      steering in [-0.8, 0.8]
     *      distanceToRight = 1
     *      distanceToLeft = 1
     *      gps = (100, 0, 300.4)^T
     *      weather in [0, 1]
     *      orientation in [0, 2 * Math.PI]
     *      vanishingPoint in [0, 2 * Math.PI]
     */
    public void testMainControlBlock() {

        PathGenerator pathGenerator = new PathGenerator();

        List<Vertex> path = pathGenerator.generateStraightLine(100, 0.0, 0.0, new ArrayRealVector(new Double[]{1.0, 0.0, 0.0}), 0);
        Double velocity;
        Double steering;
        Double distanceToRight = 1.0;
        Double distanceToLeft = 1.0;
        RealVector gps = new ArrayRealVector(new Double[] {100.0, 0.0, 300.4});
        Double weather;
        Double orientation;
        Double vanishingPoint;
        List<Object> cars = new LinkedList<>();
        List<Object> pedestrians = new LinkedList<>();
        List<Object> signs = new LinkedList<>();

        for(velocity = 0.0; velocity <= 100.0; velocity += 5.0) {
            for(steering = -0.8; steering <= 0.8; steering += 0.1) {
                for(weather = 0.0; weather <= 1.0; weather += 0.1) {
                    for(orientation = 0.0; orientation <= 2 * Math.PI; orientation += 0.1) {
                        for(vanishingPoint = 0.0; vanishingPoint <= 2* Math.PI; vanishingPoint += 0.1) {
                            Object[] result = executeTestCase(path, velocity, steering, distanceToRight, distanceToLeft,
                                    gps, weather, orientation, vanishingPoint, cars, pedestrians, signs);
                            if((Double) result[0] < 0.0 || (Double) result[0] > 3.5) {
                                assertFalse(true);
                            }
                            if((Double) result[1] < 0.0 || (Double) result[1] > 5.0) {
                                assertFalse(true);
                            }
                            if((Double) result[2] < -0.785398 || (Double) result[2] > 0.785398) {
                                assertFalse(true);
                            }
                            if((Integer) result[3] != 1) {
                                assertFalse(true);
                            }
                        }
                    }
                }
            }
        }
        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/
    /**
     * this function sets inputs, executes the function block, and returns the outputs
     *
     * @param path the path
     * @param velocity the current velocity
     * @param steering the current steering angle
     * @param distanceToRight the current distance to right
     * @param distanceToLeft the current distance to left
     * @param gps the current gps coordinates
     * @param weather the current wether coefficient
     * @param orientation the current global orientation
     * @param vanishingPoint the current angle to the vanishing point
     * @param cars all detected cars
     * @param pedestrians all detected pedestrians
     * @param signs all detected signs
     * @return the output(s) of the executed function block
     */
    private Object[] executeTestCase(List<Vertex> path,
                                     Double velocity,
                                     Double steering,
                                     Double distanceToRight,
                                     Double distanceToLeft,
                                     RealVector gps,
                                     Double weather,
                                     Double orientation,
                                     Double vanishingPoint,
                                     List<Object> cars,
                                     List<Object> pedestrians,
                                     List<Object> signs) {

        Map<String, Object> inputs = new LinkedHashMap<>();
        inputs.put(BusEntry.NAVIGATION_DETAILED_PATH_WITH_MAX_STEERING_ANGLE.toString(), path);
        inputs.put(BusEntry.CONSTANT_MAXIMUM_TOTAL_VELOCITY.toString(), new Double(100.0));
        inputs.put(BusEntry.SIMULATION_DELTA_TIME.toString(), new Double(0.01));
        inputs.put(BusEntry.CONSTANT_NUMBER_OF_GEARS.toString(), new Integer(1));
        inputs.put(BusEntry.CONSTANT_WHEELBASE.toString(), new Double(2.921));
        inputs.put(BusEntry.CONSTANT_MOTOR_MAX_ACCELERATION.toString(), new Double(3.5));
        inputs.put(BusEntry.CONSTANT_MOTOR_MIN_ACCELERATION.toString(), new Double(-1.5));
        inputs.put(BusEntry.CONSTANT_BRAKES_MAX_ACCELERATION.toString(), new Double(5.0));
        inputs.put(BusEntry.CONSTANT_BRAKES_MIN_ACCELERATION.toString(), new Double(0.0));
        inputs.put(BusEntry.CONSTANT_STEERING_MAX_ANGLE.toString(), new Double(0.785398));
        inputs.put(BusEntry.CONSTANT_STEERING_MIN_ANGLE.toString(), new Double(-0.785398));
        inputs.put(BusEntry.CONSTANT_TRAJECTORY_ERROR.toString(), new Double(0.0));
        inputs.put(BusEntry.VEHICLE_MAX_TEMPORARY_ALLOWED_VELOCITY.toString(), new Double(Double.MAX_VALUE));
        inputs.put(BusEntry.SENSOR_VELOCITY.toString(), velocity);
        inputs.put(BusEntry.SENSOR_STEERING.toString(), steering);
        inputs.put(BusEntry.SENSOR_DISTANCE_TO_RIGHT.toString(), distanceToRight);
        inputs.put(BusEntry.SENSOR_DISTANCE_TO_LEFT.toString(), distanceToLeft);
        inputs.put(BusEntry.SENSOR_GPS_COORDINATES.toString(), gps);
        inputs.put(BusEntry.SENSOR_CURRENT_SURFACE.toString(), Surface.Asphalt);
        inputs.put(BusEntry.SENSOR_WEATHER.toString(), weather);
        inputs.put(BusEntry.SENSOR_CAMERA.toString(), 1);
        inputs.put(BusEntry.SENSOR_COMPASS.toString(), orientation);
        inputs.put(BusEntry.COMPUTERVISION_VANISHING_POINT.toString(), vanishingPoint);
        inputs.put(BusEntry.COMPUTERVISION_DETECTED_CARS.toString(), cars);
        inputs.put(BusEntry.COMPUTERVISION_DETECTED_PEDESTRIANS.toString(), pedestrians);
        inputs.put(BusEntry.COMPUTERVISION_DETECTED_SIGNS.toString(), signs);

        FunctionBlock mainControlBlock = new MainControlBlock();

        mainControlBlock.setInputs(inputs);
        mainControlBlock.execute(1);

        Object[] result = new Object[4];
        result[0] = mainControlBlock.getOutputs().get(BusEntry.ACTUATOR_ENGINE.toString());
        result[1] = mainControlBlock.getOutputs().get(BusEntry.ACTUATOR_BRAKE.toString());
        result[2] = mainControlBlock.getOutputs().get(BusEntry.ACTUATOR_STEERING.toString());
        result[3] = mainControlBlock.getOutputs().get(BusEntry.ACTUATOR_GEAR.toString());

        return result;
    }
}
