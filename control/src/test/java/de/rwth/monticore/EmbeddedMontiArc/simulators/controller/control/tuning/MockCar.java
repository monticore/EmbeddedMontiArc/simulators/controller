/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.tuning;

/**
 * Just a class implementing the same simulation as TestVelocityTuning
 * For debugging of TestZieglerNichols only!
 * Created by s.erlbeck on 22.01.2017.
 */
public class MockCar implements ControllerTuningCar {

    private double previousSpeed = 0;
    private double desiredCorrection = 0;
    private double angle = 0;

    public double getCurrentSpeed() {
        previousSpeed = desiredCorrection * 5.0 / 1000.0 + previousSpeed;
        return previousSpeed;
    }

    public void setCurrentSpeed(double speed) {
        desiredCorrection = speed - previousSpeed;
    }


    //no steering implemented
    public double getCurrentSteering() {
        return angle;
    }

    public void setCurrentSteering(double angle) {
        this.angle = 0.9 * angle;
    }
}
