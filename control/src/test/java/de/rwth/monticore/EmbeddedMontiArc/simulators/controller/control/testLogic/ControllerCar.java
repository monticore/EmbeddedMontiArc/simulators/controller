/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.testLogic;

/**
 * Created by xiangwei wang on 2/2/2017.
 */
public interface ControllerCar {

    double getDistanceToLeft();
    double getDistanceToRight();

    double getCurrentVelocity();
    double getCurrentSteeringAngel();


    void setCurrentVelocity(double velocity);
    void setCurrentSteeringAngel(double steeringAngel);

    void setDistanceToLeft(double distanceToLeft);
    void setDistanceToRight(double distanceToRight);
}
