/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.trajectoryBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.PathGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a test for the FunctionBlock LocateNearestVertexInPath.
 *
 * Created by Christoph Grüne on 05.03.2017.
 */
public class FunctionBlockTestLocateNearestVertexInPath extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestLocateNearestVertexInPath(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestLocateNearestVertexInPath.class);
    }

    /**
     * this tests the correctness in a path that is a straight line (example 1)
     */
    public void testLocateNearestVertex_1() {
        executeTest(1);
    }

    /**
     * this tests the correctness in a path that is a circle (example 2)
     */
    public void testLocateNearestVertex_2() {
        executeTest(2);
    }


    /*****************************
     * helper functions          *
     *****************************/
    /**
     * executes the test for either a straight line (example 1) or a circular path (example 2).
     * The straight path begins at (0.0, 0.0, 0.0) and has a length of 100 and the circular has the middle point in (0.0, 0.0, 0.0) and a radius of 50.
     * The gps coordinates which are given to the block are (x, y, 0.0), whereby x in [-20.0, 20.0] and y in [-20.0, 20.0]
     *
     * @param exampleIndex the example number
     */
    private void executeTest(int exampleIndex) {
        PathGenerator pathGenerator = new PathGenerator();
        List<Vertex> path;
        switch(exampleIndex) {
            case 1 : path = pathGenerator.generateStraightLine(100, 0.0, 0.0, new ArrayRealVector(new Double[] {1.0, 0.0, 0.0}), 0); break;
            case 2 : path = pathGenerator.generateCircle(50, 0.0, 0.0, 0); break;
            default : path = pathGenerator.generateStraightLine(100, 0.0, 0.0, new ArrayRealVector(new Double[] {1.0, 0.0, 0.0}), 0); break;
        }

        FunctionBlock locateNearestVertex = new LocateNearestVertexInPath();

        for(double x = -20.0; x < 20.0; x += 0.1) {
            for(double y = -20.0; y < 20.0; y += 0.1) {
                RealVector gps = new ArrayRealVector(new Double[] {x, y, 0.0});

                Map<String, Object> inputs = new LinkedHashMap<>();
                inputs.put(ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_gps_coordinates.toString(), gps);
                inputs.put(ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_path.toString(), path);

                Vertex nearestVertex = path.get(0);
                for(Vertex vertex : path) {
                    if(vertex.getPosition().getDistance(gps) < nearestVertex.getPosition().getDistance(gps)) {
                        nearestVertex = vertex;
                    }
                }

                locateNearestVertex.setInputs(inputs);
                locateNearestVertex.execute(1);
                Vertex result = (Vertex) locateNearestVertex.getOutputs().get(ConnectionEntry.LOCATE_NEAREST_VERTEX_IN_PATH_nearest_vertex.toString());

                if(!result.equals(nearestVertex)) {
                    assertFalse(true);
                }
            }
        }

        assertTrue(true);
    }
}
