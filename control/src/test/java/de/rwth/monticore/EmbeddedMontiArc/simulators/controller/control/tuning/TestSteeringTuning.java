/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.tuning;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Testing and tuning a functionblocks.PID that controls the steering
 * Created by s.erlbeck on 27.12.2016.
 */
public class TestSteeringTuning extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TestSteeringTuning(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TestSteeringTuning.class);
    }


    public void testSteeringControl() {
        assertTrue(true);
    }

    /*****************************
     * helper functions
     *****************************/

}
