/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * this is a test for the FunctionBlock SteeringDistributor
 *
 * Created by Christoph on 08.03.2017.
 */
public class FunctionBlockTestSteeringDistributor extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestSteeringDistributor(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestSteeringDistributor.class);
    }


    /**
     * tests if all inputs (see below) are handled correctly by the SteeringDistributor:
     *      anglePID in [-10.0, 10.0]
     *      sensorSteering in [-10.0, 10.0]
     *      minSteeringAngle = -5.0
     *      maxSteeringAngle = 5.0
     */
    public void testSteeringDistributor() {
        Double anglePid = 0.0;
        Double sensorSteering = 0.0;
        Double minSteeringAngle = -5.0;
        Double maxSteeringAngle = 5.0;

        for(anglePid = -10.0; anglePid <= 10.0; anglePid += 0.1) {
            for(sensorSteering = -10.0; anglePid <= 10.0; anglePid += 0.1) {
                double result = executeTestCase(anglePid, sensorSteering, minSteeringAngle, maxSteeringAngle);

                double compareResult = anglePid + sensorSteering;
                if(compareResult < minSteeringAngle) {
                    compareResult = minSteeringAngle;
                } else if(compareResult > maxSteeringAngle) {
                    compareResult = maxSteeringAngle;
                }
                if (result != compareResult) {
                    assertFalse(true);
                }
                assertTrue(true);
            }
        }
    }

    /*****************************
     * helper functions          *
     *****************************/
    /**
     * this function sets inputs, executes the function block, and returns the outputs
     *
     * @param anglePid the angle calculated by the PID
     * @param sensorSteering the current steering angle
     * @param minSteeringAngle the minimal steering angle
     * @param maxSteeringAngle the maximal steering angle
     * @return the output(s) of the executed function block
     */
    private Double executeTestCase(Double anglePid, Double sensorSteering, Double minSteeringAngle, Double maxSteeringAngle) {

        Map<String, Object> inputs = new LinkedHashMap<>();
        inputs.put(ConnectionEntry.STEERING_DISTRIBUTOR_angle_pid.toString(), anglePid);
        inputs.put(ConnectionEntry.STEERING_DISTRIBUTOR_max_steering_angle.toString(), maxSteeringAngle);
        inputs.put(ConnectionEntry.STEERING_DISTRIBUTOR_min_steering_angle.toString(), minSteeringAngle);
        inputs.put(ConnectionEntry.STEERING_DISTRIBUTOR_sensor_steering.toString(), sensorSteering);

        FunctionBlock steeringDistributor = new SteeringDistributor();

        steeringDistributor.setInputs(inputs);
        steeringDistributor.execute(1);

        Double result = (Double) steeringDistributor.getOutputs().get(ConnectionEntry.STEERING_DISTRIBUTOR_actuator_steering_value.toString());

        return result;
    }
}
