/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.PathGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a test for the FunctionBlock InnerControlBlock.
 *
 * Created by Christoph on 08.03.2017.
 */
public class FunctionBlockTestInnerControlBlock extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestInnerControlBlock(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestInnerControlBlock.class);
    }

    /**
     * tests if all inputs (see below) are handled correctly:
     *      for all Surface s
     *      velocity in [0, 100]
     *      steering in [-0.8, 0.8]
     *      distanceToRight = 1
     *      distanceToLeft = 1
     *      gps = (100, 0, 300.4)^T
     *      weather in [0, 1]
     *      orientation in [0, 2 * Math.PI]
     *      vanishingPoint in [0, 2 * Math.PI]
     *      hazardVelocity in [0, 100.0]
     *      signVelocity in [0, 100]
     *      trajectoryVelocity in [0, 100]
     */
    public void testInnerControlBlock() {

        PathGenerator pathGenerator = new PathGenerator();

        List<Vertex> path = pathGenerator.generateStraightLine(100, 0.0, 0.0, new ArrayRealVector(new Double[]{1.0, 0.0, 0.0}), 0);
        Double velocity;
        Double steering;
        Double distanceToRight = 1.0;
        Double distanceToLeft = 1.0;
        RealVector gps = new ArrayRealVector(new Double[] {100.0, 0.0, 300.4});
        Double weather;
        Double orientation;
        Integer camera = 1;
        Double vanishingPoint;

        Double hazardVelocity;
        Double signVelocity;
        Double trajectoryVelocity;

        for(Surface s : Surface.values()) {
            for (velocity = 0.0; velocity <= 100.0; velocity += 20.0) {
                for (steering = -0.8; steering <= 0.8; steering += 0.2) {
                    for (weather = 0.0; weather <= 1.0; weather += 0.1) {
                        for (orientation = 0.0; orientation <= 2 * Math.PI; orientation += 0.5) {
                            for (vanishingPoint = 0.0; vanishingPoint <= 2 * Math.PI; vanishingPoint += 0.5) {
                                for (hazardVelocity = 0.0; hazardVelocity <= 100.0; hazardVelocity += 20.0) {
                                    for (signVelocity = 0.0; signVelocity <= 100.0; signVelocity += 20.0) {
                                        for (trajectoryVelocity = 0.0; trajectoryVelocity <= 100.0; trajectoryVelocity += 20.0) {
                                            Object[] result = executeTestCase(path, velocity, steering, distanceToRight, distanceToLeft,
                                                    gps, weather, orientation, camera, vanishingPoint, signVelocity, hazardVelocity, trajectoryVelocity);
                                            if ((Double) result[0] < 0.0 || (Double) result[0] > 3.5) {
                                                assertFalse(true);
                                            }
                                            if ((Double) result[1] < 0.0 || (Double) result[1] > 5.0) {
                                                assertFalse(true);
                                            }
                                            if ((Double) result[2] < -0.785398 || (Double) result[2] > 0.785398) {
                                                assertFalse(true);
                                            }
                                            if ((Integer) result[3] != 1) {
                                                assertFalse(true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/
    /**
     * this function sets inputs, executes the function block, and returns the outputs
     *
     * @param path the path
     * @param velocity the current velocity
     * @param steering the current steering
     * @param distanceToRight the distance to the right lane
     * @param distanceToLeft the distance to the left lane
     * @param gps the gps coordinate
     * @param weather the coefficient of the current weather situation
     * @param orientation the orientation of the car
     * @param camera a camera??? (mock)
     * @param vanishingPoint the angle vanishing to the vanishing point
     * @param signVelocity the velocity calculated by the sign block
     * @param hazardVelocity the velocity calculated by the hazard block
     * @param trajectoryVelocity the velocity calculated by the trajectory block
     * @return the output(s) of the executed function block
     */
    private Object[] executeTestCase(List<Vertex> path,
                                     Double velocity,
                                     Double steering,
                                     Double distanceToRight,
                                     Double distanceToLeft,
                                     RealVector gps,
                                     Double weather,
                                     Double orientation,
                                     Integer camera,
                                     Double vanishingPoint,
                                     Double signVelocity,
                                     Double hazardVelocity,
                                     Double trajectoryVelocity) {

        Map<String, Object> inputs = new LinkedHashMap<>();
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_path.toString(), path);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_constant_maximum_velocity.toString(), new Double(100.0));
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_constant_number_of_gears.toString(), new Integer(1));
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_constant_motor_max_acceleration.toString(), new Double(3.5));
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_constant_brake_max_acceleration.toString(), new Double(5.0));
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_constant_max_steering_angle.toString(), new Double(0.785398));
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_constant_min_steering_angle.toString(), new Double(-0.785398));
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_constant_trajectory_error.toString(), new Double(0.0));
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_vehicle_max_temporary_allowed_velocity.toString(), new Double(0.0));
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_velocity.toString(), velocity);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_steering.toString(), steering);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_distance_to_right.toString(), distanceToRight);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_distance_to_left.toString(), distanceToLeft);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_gps_coordinates.toString(), gps);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_current_surface.toString(), Surface.Asphalt);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_weather.toString(), weather);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_sensor_orientation.toString(), orientation);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_angle_to_vanishing_point.toString(), vanishingPoint);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_sign_velocity.toString(), signVelocity);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_hazard_velocity.toString(), hazardVelocity);
        inputs.put(ConnectionEntry.INNER_CONTROL_BLOCK_trajectory_velocity.toString(), trajectoryVelocity);

        FunctionBlock innerControlBlock = new InnerControlBlock();

        innerControlBlock.setInputs(inputs);
        innerControlBlock.execute(0.01);

        Object[] result = new Object[4];
        result[0] = innerControlBlock.getOutputs().get(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_engine.toString());
        result[1] = innerControlBlock.getOutputs().get(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_brake.toString());
        result[2] = innerControlBlock.getOutputs().get(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_steering.toString());
        result[3] = innerControlBlock.getOutputs().get(ConnectionEntry.INNER_CONTROL_BLOCK_actuator_gear.toString());

        return result;
    }
}
