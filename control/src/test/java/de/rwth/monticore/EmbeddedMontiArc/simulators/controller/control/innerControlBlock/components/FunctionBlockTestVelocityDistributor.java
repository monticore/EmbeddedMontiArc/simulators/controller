/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * this is a test for the FunctionBlock VelocityDistributor.
 *
 * Created by Christoph on 08.03.2017.
 */
public class FunctionBlockTestVelocityDistributor extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestVelocityDistributor(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestVelocityDistributor.class);
    }

    /**
     * tests if all inputs (see below) are handled correctly:
     *      maxVelocity = 200
     *      numberOfGears = 1
     *      velocityPID in [0, maxVelocity]
     *      deltaTime in [0.01, 10]
     *      brakeMaxAcceleration in [0, 10]
     *      currentVelocity in [0, maxVelocity]
     */
    public void testVelocityDistributor() {
        Double currentVelocity = 0.0;
        Double velocityPid = 0.0;
        Double maxVelocity = 200.0;
        Integer numberGears = 1;
        Double deltaTime = 0.0;
        Double motorMaxAcceleration = 0.0;
        Double brakeMaxAcceleration = 0.0;

        for (velocityPid = 0.0; velocityPid <= maxVelocity; velocityPid += 2.5) {
            for (deltaTime = 0.01; deltaTime <= 10.0; deltaTime *= 2) {
                for (motorMaxAcceleration = 0.0; motorMaxAcceleration <= 10.0; motorMaxAcceleration += 0.9) {
                    for (brakeMaxAcceleration = 0.0; brakeMaxAcceleration <= 10.0; brakeMaxAcceleration += 0.9) {
                        for (currentVelocity = 0.0; currentVelocity <= maxVelocity; currentVelocity += 2.5) {
                            Object[] result = executeTestCase(currentVelocity, velocityPid, maxVelocity, numberGears, deltaTime, motorMaxAcceleration, brakeMaxAcceleration);

                            double compareResultEngine = velocityPid / deltaTime;
                            if (compareResultEngine < 0.0) {
                                compareResultEngine = 0.0;
                            } else if (compareResultEngine > motorMaxAcceleration) {
                                compareResultEngine = motorMaxAcceleration;
                            }
                            if ((Double) result[0] != compareResultEngine) {
                                assertFalse(true);
                            }

                            double compareResultBrake = -velocityPid / deltaTime;
                            if (compareResultBrake < 0.0) {
                                compareResultBrake = 0.0;
                            } else if (compareResultBrake > brakeMaxAcceleration) {
                                compareResultBrake = brakeMaxAcceleration;
                            }
                            if ((Double) result[1] != compareResultBrake) {
                                assertFalse(true);
                            }

                            int compareResultGears = 1;
                            if ((Integer) result[2] != compareResultGears) {
                                assertFalse(true);
                            }

                            assertTrue(true);
                        }
                    }
                }
            }
        }
    }

    /*****************************
     * helper functions          *
     *****************************/
    /**
     * this function sets inputs, executes the function block, and returns the outputs
     *
     * @param currentVelocity the current velocity
     * @param velocityPid the velocity passed form the PID
     * @param maxVelocity the maximal velocity
     * @param numberGears the number of gears th car has
     * @param deltaTime the delta time between the last call and this call
     * @param motorMaxAcceleration the maximal acceleration of the engine
     * @param brakeMaxAcceleration the maximal acceleration of the brakes
     * @return the output(s) of the executed function block
     */
    private Object[] executeTestCase(Double currentVelocity, Double velocityPid, Double maxVelocity, Integer numberGears, Double deltaTime, Double motorMaxAcceleration, Double brakeMaxAcceleration) {

        Map<String, Object> inputs = new LinkedHashMap<>();
        inputs.put(ConnectionEntry.VELOCITY_DISTRIBUTOR_velocity_pid.toString(), velocityPid);
        inputs.put(ConnectionEntry.VELOCITY_DISTRIBUTOR_current_velocity.toString(), currentVelocity);
        inputs.put(ConnectionEntry.VELOCITY_DISTRIBUTOR_maximum_velocity.toString(), maxVelocity);
        inputs.put(ConnectionEntry.VELOCITY_DISTRIBUTOR_number_of_gears.toString(), numberGears);
        inputs.put(ConnectionEntry.VELOCITY_DISTRIBUTOR_motor_max_acceleration.toString(), motorMaxAcceleration);
        inputs.put(ConnectionEntry.VELOCITY_DISTRIBUTOR_brake_max_acceleration.toString(), brakeMaxAcceleration);

        FunctionBlock velocityDistributor = new VelocityDistributor();

        velocityDistributor.setInputs(inputs);
        velocityDistributor.execute(deltaTime);

        Double resultEngine = (Double) velocityDistributor.getOutputs().get(ConnectionEntry.VELOCITY_DISTRIBUTOR_actuator_engine_value.toString());
        Double resultBrake = (Double) velocityDistributor.getOutputs().get(ConnectionEntry.VELOCITY_DISTRIBUTOR_actuator_brake_value.toString());
        Integer resultGear = (Integer) velocityDistributor.getOutputs().get(ConnectionEntry.VELOCITY_DISTRIBUTOR_actuator_gear_value.toString());

        return new Object[]{resultEngine, resultBrake, resultGear};
    }
}
