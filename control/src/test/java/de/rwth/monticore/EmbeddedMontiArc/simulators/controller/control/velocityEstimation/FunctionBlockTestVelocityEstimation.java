/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.velocityEstimation;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.PathGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Test for the FunctionBlock VelocityEstimation
 *
 * Created by Christoph Grüne on 11.03.2017.
 */
public class FunctionBlockTestVelocityEstimation extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestVelocityEstimation(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestVelocityEstimation.class);
    }

    /**
     * tests if all inputs (see below) are handled correctly:
     *      for all Surfaces.values()
     *      currentWeather in [0, 1]
     *      wheelbase = 4
     *      various straight line and circular paths
     */
    public void testVelocityEstimation() {

        PathGenerator pathGenerator = new PathGenerator();
        List<Vertex> path;

        Iterator<Vertex> pathIterator;
        Vertex startVertex;
        Double distance;
        Surface currentSurface = Surface.Asphalt;
        Double currentWeather;

        double steeringAngle;
        double compareResult;
        double wheelbase = 4.0;


        for (double x = -10.0; x <= 10.0; x += 5.0) {
            for (double y = -10.0; y <= 10.0; y += 5.0) {
                for (distance = 0.0; distance <= 150.0; distance += 2.5) {
                    for (int i = 0; i < 2; ++i) {
                        if (i == 0) {
                            path = pathGenerator.generateStraightLine(distance * 2, x, y, new ArrayRealVector(new Double[]{x * y, x + y, 0.0}), 0);
                            steeringAngle = 0.0;
                        } else {
                            steeringAngle = (Double) (Math.asin(wheelbase / (distance + 5)));
                            path = pathGenerator.generateCircle(distance + 5, x, y, 0);
                        }
                        for (Vertex vertex : path) {
                            vertex.setMaximumSteeringAngle(steeringAngle);
                        }
                        for (Surface s : Surface.values()) {
                            for (currentWeather = 0.0; currentWeather <= 1.0; currentWeather += 0.1) {
                                pathIterator = path.iterator();
                                double result = executeTestCase(pathIterator, path.get(0), distance, currentSurface, currentWeather);

                                compareResult = s.getParameterA() * Math.exp(s.getParameterB(currentWeather) * steeringAngle) + s.getParameterC();
                                boolean iteratorFlag = false;
                                if (!pathIterator.hasNext()) {
                                    iteratorFlag = true;
                                }
                                if (iteratorFlag) {
                                    compareResult = 0.0;
                                }
                                if (result != compareResult) {
                                    assertFalse(true);
                                }
                            }
                        }
                    }

                }
            }
        }
        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/
    /**
     * this function sets inputs, executes the function block, and returns the outputs
     *
     * @param pathIterator the pathIterator of the path
     * @param vertex the start vertex of the local path, the to the car nearest vertex
     * @param distance the distance to that the path should be "foreseen"
     * @param currentSurface the current surface the car drives on
     * @param currentWeather the current weather coefficient
     * @return the output(s) of the executed function block
     */
    private Double executeTestCase(Iterator<Vertex> pathIterator,
                                   Vertex vertex,
                                   Double distance,
                                   Surface currentSurface,
                                   Double currentWeather) {

        Map<String, Object> inputs = new LinkedHashMap<>();
        inputs.put(ConnectionEntry.VELOCITY_ESTIMATION_path_iterator.toString(), pathIterator);
        inputs.put(ConnectionEntry.VELOCITY_ESTIMATION_start_vertex.toString(), vertex);
        inputs.put(ConnectionEntry.VELOCITY_ESTIMATION_distance.toString(), distance);
        inputs.put(ConnectionEntry.VELOCITY_ESTIMATION_current_surface.toString(), currentSurface);
        inputs.put(ConnectionEntry.VELOCITY_ESTIMATION_current_weather.toString(), currentWeather);

        FunctionBlock velocityEstimation = new VelocityEstimation();

        velocityEstimation.setInputs(inputs);
        velocityEstimation.execute(1);

        Double result = (Double) velocityEstimation.getOutputs().get(ConnectionEntry.VELOCITY_ESTIMATION_velocity.toString());

        return result;
    }
}
