/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.tuning;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.PID.PID;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Testing and tuning a functionblocks.PID that controls the velocity
 * This testing case has a much higher resolution plot than the other so you can see the oscillation better
 * Created by s.erlbeck on 17.01.2016.
 */
public class TestVelocityHighResolution extends TestCase {

    //default visibility for debug purposes (see TestVelocityHighResolution)
    private HashMap<Long, Double> error;
    private HashMap<Long, Double> desiredValues;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TestVelocityHighResolution(String testName) {
        super(testName);
        error = new LinkedHashMap<Long, Double>(1000);
        desiredValues = new LinkedHashMap<Long, Double>(1000);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TestVelocityHighResolution.class);
    }

    public void test() {

        //k_krit = 7958.999
        PID velControl = initPID(7958.999, 0, 0);
        case2();

        HashMap<String, Object> input = new LinkedHashMap<String, Object>(2);
        double desiredCorrection = 0;
        double speed = 0;
        double sensorValue = 0;

        try{
            PrintWriter writer = new PrintWriter("errorHighResolution.txt");

            //time in microseconds
            for(long time = 0; time < 4001; time += 1) {
                //log error
                writer.println(" " + sensorValue + " " + desiredValues.get(time- (time % 1000)));

                //calculate this step
                input.put(ConnectionEntry.PID_desired_value.toString(), desiredValues.get(time - (time % 1000)));
                input.put(ConnectionEntry.PID_current_value.toString(), sensorValue);

                velControl.setInputs(input);
                velControl.execute(1);
                desiredCorrection = (Double)velControl.getOutputs().get(ConnectionEntry.PID_new_controlled_value.toString());


                //simulate time step
                sensorValue = simulationSensor(speed, desiredCorrection);
                speed = sensorValue;


            }

            writer.close();

        } catch (IOException e) {
            // do something
            System.out.println("Problem with File.");
        }

        assertTrue(true);
    }


    /*****************************
     * helper functions
     *****************************/

    //this is basically wrong because the controller gets called every microsecond in this class
    //it is about the principle that for suiting k_p the controlled value oscillates
    //changing d_t does not seem to make a difference
    private PID initPID(double k_p, double k_i, double k_d) {
        PID controller = new PID(k_p, k_i, k_d);//new PID(k_p, k_i, k_d, 1000);
        return controller;
    }

    //same as for initPID
    private double simulationSensor(double previousSpeed, double desiredControl) {
        //assuming max acceleration a = 5 m/s^2
        //assuming that for a desired correction of 20 m/s, the full acceleration is used
        //assuming that the previous speed decays with a factor of 0.99

        //calculate how much the control differs from the actual previous speed
        double desiredCorrection = desiredControl - previousSpeed;

        //scaling the correction from [-20, 20] to [-1, 1]
        desiredCorrection /= 20.0;

        //v_new = (scaling * max_acc) * time + damping_factor * v_old

        //time step: one millisecond!!!
        return desiredCorrection * 5.0 / 1000.0 + 0.99*previousSpeed;
    }

    //probably not need anymore
    private void case2() {
        long time = 0;

        desiredValues.clear();

        for (int i = 0; i < 1000; i++) {
            desiredValues.put(time, 5.0);
            time += 1000;
        }
    }
}
