/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by s.erlbeck on 06.03.2017.
 */
public class FunctionBlockTestSteeringLogic extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestSteeringLogic(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestSteeringLogic.class);
    }


    public void testSteeringLogic() {

        //the distances
        double d_l = 2;
        double d_r = 2;
        //the orientation in (-PI, PI)
        double orient = 0.25D * Math.PI;


        List<Vertex> path = new LinkedList<Vertex>();

        Vertex vertex1 = new Vertex(0L, 0L, new ArrayRealVector(new double[]{0, 0}), 0D);
        Vertex vertex2 = new Vertex(0L, 0L, new ArrayRealVector(new double[]{1, 1}), 0D);

        path.add(vertex1);
        path.add(vertex2);

        double orientInput;

        if(orient < 0) {
            orientInput = 2 * Math.PI - orient;
        } else {
            orientInput = orient;
        }


        Double res = executeTestCase(1, d_l, d_r, 0, path, new ArrayRealVector(new double[]{0, 1}), orientInput);

        //with these values, the angle towards the trajectory is
        // atan(-1/sqrt(2))
        //which is about -35,26 degree
        double angleToTrajectory = Math.atan(-1.0 / Math.sqrt(2) / 2.0);
        //however the trajectory has an angle of -45 degree from y-Axis and the car has an angle of orient
        angleToTrajectory += -45.0 / 180.0 * Math.PI - orient;

        //the angle to the street
        double angleToStreet = Math.atan(((d_l + d_r) / 2.0 - d_r) / 2.0);

        //transform into range
        if(angleToTrajectory > Math.PI)
            angleToTrajectory -= 2*Math.PI;
        else if(angleToTrajectory < -Math.PI)
            angleToTrajectory += 2*Math.PI;

        angleToStreet *= -1.0;
        angleToTrajectory *= -1.0;

        //transform all angles to degree
        angleToTrajectory = (180.0 * angleToTrajectory / Math.PI);
        angleToStreet = (180.0 * angleToStreet / Math.PI);
        res = (180.0 * res / Math.PI);

        System.out.println(res);

        //allow some error as FLOP are just pseudo arithmetics
        assertTrue(res > angleToTrajectory - 0.0000001);
        assertTrue(res < angleToTrajectory + 0.0000001);
    }


    /*****************************
     * helper functions          *
     *****************************/
    private Double executeTestCase(double velocity, double distanceLeft, double distanceRight, double vanishAngle, List<Vertex> path, RealVector coords, Double globalOrientation) {

        FunctionBlock steeringBlock = new SteeringLogic();

        Double res;

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        input.put(ConnectionEntry.STEERING_LOGIC_current_velocity.toString(), velocity);
        input.put(ConnectionEntry.STEERING_LOGIC_distance_to_left.toString(), distanceLeft);
        input.put(ConnectionEntry.STEERING_LOGIC_distance_to_right.toString(), distanceRight);
        input.put(ConnectionEntry.STEERING_LOGIC_vanishing_angle.toString(), vanishAngle);
        input.put(ConnectionEntry.STEERING_LOGIC_path.toString(), path);
        input.put(ConnectionEntry.STEERING_LOGIC_gps_coordinates.toString(), coords);
        input.put(ConnectionEntry.STEERING_LOGIC_global_orientation.toString(), globalOrientation);

        steeringBlock.setInputs(input);
        steeringBlock.execute(1);

        res = (Double) steeringBlock.getOutputs().get(ConnectionEntry.STEERING_LOGIC_steering_angle.toString());

        return res;
    }
}
