/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.trajectoryBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.PathGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockParameter;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Test for the FunctionBlock TrajectoryBlock
 *
 * Created by Christoph Grüne on 08.03.2017.
 */
public class FunctionBlockTestTrajectoryBlock extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestTrajectoryBlock(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestTrajectoryBlock.class);
    }

    /**
     * tests if all inputs (see below) are handled correctly:
     *      for all Surface.values()
     *      maxVelocity = 100
     *      currentVelocity in [0, maxVelocity]
     *      brakesMaxAcceleration in [0.5, 10.0]
     *      weather in [0, 1]
     *      wheelbase = 4
     *      various straight line and circular paths
     */
    public void testTrajectoryBlock() {

        PathGenerator pathGenerator = new PathGenerator();
        List<Vertex> path;

        RealVector gpsCoordinates;
        Double currentVelocity;
        Double brakesMaxAcceleration;

        Double distance;
        Double maxVelocity;
        Surface currentSurface = Surface.Asphalt;
        Double currentWeather;
        Double wheelbase = 4.0;

        double steeringAngle;
        double compareResult;

        for(double x = -10.0; x <= 10.0; x += 5.0) {
            for(double y = -10.0; y <= 10.0; y += 5.0) {
                for(distance = 0.5; distance <= 150.0; distance *= 5.0) {
                    for(int i = 0; i < 2; ++i) {
                        if(i == 0) {
                            path = pathGenerator.generateStraightLine(distance * 2, x, y, new ArrayRealVector(new Double[]{x * y, x + y, 0.0}), 0);
                            steeringAngle = 0.0;
                        } else {
                            distance = 5 + distance;
                            steeringAngle = (Double) (Math.asin(wheelbase / distance));
                            path = pathGenerator.generateCircle(distance, x, y, 0);
                        }
                        for(Vertex vertex : path) {
                            vertex.setMaximumSteeringAngle(steeringAngle);
                        }
                        for(brakesMaxAcceleration = 0.5; brakesMaxAcceleration <= 10.0; brakesMaxAcceleration += 1.1) {
                            for (maxVelocity = 100.0; maxVelocity <= 100.0; maxVelocity += 2.5) {
                                for (currentVelocity = 0.0; currentVelocity <= maxVelocity; currentVelocity += 2.5) {
                                    for (Surface s : Surface.values()) {
                                        for (currentWeather = 0.0; currentWeather <= 1.0; currentWeather += 0.1) {
                                            Iterator<Vertex> pathIterator = path.iterator();
                                            double result = executeTestCase(path,
                                                    path.get(0).getPosition(),
                                                    currentVelocity, brakesMaxAcceleration, maxVelocity,
                                                    currentSurface, currentWeather);

                                            compareResult = s.getParameterA() * Math.exp(s.getParameterB(currentWeather) * Math.abs(steeringAngle)) + s.getParameterC();
                                            compareResult = Math.min(maxVelocity, compareResult);
                                            boolean iteratorFlag = false;
                                            Vertex startVertex = path.get(0);
                                            Vertex cur = path.get(0);
                                            while(pathIterator.hasNext() && startVertex.getPosition().getDistance(cur.getPosition()) <= (currentVelocity * currentVelocity) / (2 * brakesMaxAcceleration) * FunctionBlockParameter.DISTANCE_ESTIMATION_multiplication.getParameter() + FunctionBlockParameter.DISTANCE_ESTIMATION_offset.getParameter()) {
                                                cur = pathIterator.next();
                                            }
                                            if (!pathIterator.hasNext()) {
                                                iteratorFlag = true;
                                            }
                                            if (iteratorFlag) {
                                                compareResult = 0.0;
                                            }
                                            if (result != compareResult) {
                                                assertFalse(true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/
    /**
     * this function sets inputs, executes the function block, and returns the outputs
     *
     * @param path the path
     * @param gps the current gps position
     * @param currentVelocity the current velocity
     * @param brakesMaxAcceleration the maximal brakes acceleration
     * @param maxVelocity the maximal velocity of the car
     * @param currentSurface the current surface the car drives on
     * @param currentWeather the current weather coefficient
     * @return the output(s) of the executed function block
     */
    private Double executeTestCase(List<Vertex> path,
                                   RealVector gps,
                                   Double currentVelocity,
                                   Double brakesMaxAcceleration,
                                   Double maxVelocity,
                                   Surface currentSurface,
                                   Double currentWeather) {

        Map<String, Object> inputs = new LinkedHashMap<>();
        inputs.put(ConnectionEntry.TRAJECTORY_BLOCK_path.toString(), path);
        inputs.put(ConnectionEntry.TRAJECTORY_BLOCK_gps_coordinates.toString(), gps);
        inputs.put(ConnectionEntry.TRAJECTORY_BLOCK_current_velocity.toString(), currentVelocity);
        inputs.put(ConnectionEntry.TRAJECTORY_BLOCK_brakes_max_acceleration.toString(), brakesMaxAcceleration);
        inputs.put(ConnectionEntry.TRAJECTORY_BLOCK_max_velocity.toString(), maxVelocity);
        inputs.put(ConnectionEntry.TRAJECTORY_BLOCK_current_surface.toString(), currentSurface);
        inputs.put(ConnectionEntry.TRAJECTORY_BLOCK_current_weather.toString(), currentWeather);

        FunctionBlock trajectoryBlock = new TrajectoryBlock();

        trajectoryBlock.setInputs(inputs);
        trajectoryBlock.execute(1);

        Double result = (Double) trajectoryBlock.getOutputs().get(ConnectionEntry.TRAJECTORY_BLOCK_velocity.toString());

        return result;
    }
}
