/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.innerControlBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * this is a test for the FunctionBlock VelocityLogic
 *
 * Created by Christoph on 08.03.2017.
 */
public class FunctionBlockTestVelocityLogic extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestVelocityLogic(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestVelocityLogic.class);
    }


    /**
     * tests if all inputs (see below) are handled correctly:
     *      rainCoefficient in [0, 1]
     *      newSteeringAngle in [-5, 5]
     *      maxVelocity in [0, 200]
     *      trajectoryVelocity in [0, maxVelocity]
     *      hazardVelocity in [0, maxVelocity]
     *      signVelocity in [0, maxVelocity]
     */
    public void testVelocityLogic() {
        Double rainCoefficient;
        Double newSteeringAngle;
        Double currentSteeringAngle;
        Double maxVelocity;
        Double trajectoryVelocity;
        Double hazardVelocity;
        Double signVelocity;
        Double vehicleMaxTemporaryAllowedVelocity;

        Double maxVelocityTest = 100.0;
        Double stepSizeTest = 25.0;

        for(Surface s : Surface.values()) {
            for (rainCoefficient = 0.0; rainCoefficient <= 1.0; rainCoefficient += 0.1) {
                for (newSteeringAngle = -5.0; newSteeringAngle <= 5.0; newSteeringAngle += 0.5) {
                    for (currentSteeringAngle = -5.0; currentSteeringAngle <= 5.0; currentSteeringAngle += 0.5) {
                        for (maxVelocity = 0.0; maxVelocity <= maxVelocityTest; maxVelocity += stepSizeTest) {
                            for (trajectoryVelocity = 0.0; trajectoryVelocity <= maxVelocity; trajectoryVelocity += stepSizeTest) {
                                for (hazardVelocity = 0.0; hazardVelocity <= maxVelocity; hazardVelocity += stepSizeTest) {
                                    for (signVelocity = 0.0; signVelocity <= maxVelocity; signVelocity += stepSizeTest) {
                                        for (vehicleMaxTemporaryAllowedVelocity = 0.0; vehicleMaxTemporaryAllowedVelocity <= maxVelocity; vehicleMaxTemporaryAllowedVelocity += stepSizeTest) {
                                            double result = executeTestCase(s, rainCoefficient, newSteeringAngle, currentSteeringAngle, maxVelocity, trajectoryVelocity, hazardVelocity, signVelocity, vehicleMaxTemporaryAllowedVelocity);

                                            double angle = Math.max(newSteeringAngle, currentSteeringAngle);
                                            double steeringVelocity = s.getParameterA() * Math.exp(Math.abs(angle) * s.getParameterB(rainCoefficient)) + s.getParameterC();
                                            double compareResult = Math.min(steeringVelocity, maxVelocity);
                                            compareResult = Math.min(compareResult, trajectoryVelocity);
                                            compareResult = Math.min(compareResult, hazardVelocity);
                                            compareResult = Math.min(compareResult, signVelocity);
                                            compareResult = Math.min(compareResult, vehicleMaxTemporaryAllowedVelocity);

                                            if (result != compareResult) {
                                                assertFalse(true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/
    /**
     * this function sets inputs, executes the function block, and returns the outputs
     *
     * @param s the current s
     * @param rainCoefficient the current weather parameter
     * @param newSteeringAngle the new steering angle calculated by the velocity logic
     * @param maxVelocity the maximal velocity
     * @param trajectoryVelocity the velocity calculated by the trajectoryBlock
     * @param hazardVelocity the velocity calculated by the hazardBlock
     * @param signVelocity the velocity calculated by the signBlock
     * @param vehicleMaxTemporaryAllowedVelocity the velocity taken from vehicle directly
     * @return the output(s) of the executed function block
     */
    private Double executeTestCase(Surface s, Double rainCoefficient, Double newSteeringAngle, Double currentSteeringAngle, Double maxVelocity, Double trajectoryVelocity, Double hazardVelocity, Double signVelocity, Double vehicleMaxTemporaryAllowedVelocity) {

        Map<String, Object> inputs = new LinkedHashMap<>();
        inputs.put(ConnectionEntry.VELOCITY_LOGIC_current_surface.toString(), s);
        inputs.put(ConnectionEntry.VELOCITY_LOGIC_current_weather.toString(), rainCoefficient);
        inputs.put(ConnectionEntry.VELOCITY_LOGIC_new_steering_angle.toString(), newSteeringAngle);
        inputs.put(ConnectionEntry.VELOCITY_LOGIC_current_steering_angle.toString(), currentSteeringAngle);
        inputs.put(ConnectionEntry.VELOCITY_LOGIC_max_velocity.toString(), maxVelocity);
        inputs.put(ConnectionEntry.VELOCITY_LOGIC_trajectory_velocity.toString(), trajectoryVelocity);
        inputs.put(ConnectionEntry.VELOCITY_LOGIC_vehicle_max_temporary_allowed_velocity.toString(), vehicleMaxTemporaryAllowedVelocity);
        inputs.put(ConnectionEntry.VELOCITY_LOGIC_hazard_velocity.toString(), hazardVelocity);
        inputs.put(ConnectionEntry.VELOCITY_LOGIC_sign_velocity.toString(), signVelocity);

        FunctionBlock velocityLogic = new VelocityLogic();

        velocityLogic.setInputs(inputs);
        velocityLogic.execute(1);

        Double result = (Double) velocityLogic.getOutputs().get(ConnectionEntry.VELOCITY_LOGIC_desired_velocity.toString());

        return result;
    }
}
