/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.testLogic;

/**
 * distance: m
 * velocity: m/s
 * steeringAngel: radius
 * Created by xiangwei wang on 1/2/2017.
 */
public class MockCar implements ControllerCar {

    private double distanceToLeft;
    private double distanceToRight;
    private double currentVelocity;
    private double currentSteeringAngel;

    public MockCar(){
        distanceToLeft = 6.0;
        distanceToRight = 8.0;
        currentVelocity = 20.0;
        currentSteeringAngel = 0.0;
    }

    public MockCar(double distancetoleft, double distancetoright, double currentvelocity, double currentsteeringangel){
        this.currentSteeringAngel = currentsteeringangel;
        this.currentVelocity = currentvelocity;
        this.distanceToLeft = distancetoleft;
        this.distanceToRight = distancetoright;
    }

    public double getDistanceToLeft(){return distanceToLeft;}
    public double getDistanceToRight(){return distanceToRight;}
    public double getCurrentVelocity(){return currentVelocity;}
    public double getCurrentSteeringAngel(){return currentSteeringAngel;}


    public void setDistanceToLeft(double distancetoleft){this.distanceToLeft = distancetoleft;}
    public void setDistanceToRight(double distancetoright){this.distanceToRight = distancetoright;}

    public void setCurrentVelocity(double velocity){this.currentVelocity = velocity;}
    public void setCurrentSteeringAngel(double steeringAngel){this.currentSteeringAngel = steeringAngel;}
}
