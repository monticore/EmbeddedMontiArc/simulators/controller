/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.testLogic;

import java.lang.Math;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by xiangwei wang on 2/2/2017.
 */
public class SteeringLogicTest extends TestCase {

    @Test
    public void execute() throws Exception {

        MockCar car = new MockCar();

        double angel = Math.atan2(( car.getDistanceToRight() + car.getDistanceToLeft())/2 -
                car.getDistanceToRight(), setDistanceInFront(car.getCurrentVelocity()));

        assertFalse(angel>0.0);
    }

    @Test
    public double setDistanceInFront(double currentvelocity) throws Exception{

        double distanceInFront = currentvelocity;

        assertEquals(distanceInFront, currentvelocity);

        return distanceInFront;
    }

    public void testMock() { assertTrue(true); }
}
