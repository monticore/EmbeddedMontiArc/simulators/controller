/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.tuning;

/**
 * Some mock functions for a car which can be used for tuning the controller.
 * speed in m/s, angle in radian
 *
 * Created by s.erlbeck on 19.01.2017.
 */
public interface ControllerTuningCar {

    public abstract double getCurrentSpeed();

    public abstract double getCurrentSteering();

    public abstract void setCurrentSpeed(double speed);

    public abstract void setCurrentSteering(double angle);
}
