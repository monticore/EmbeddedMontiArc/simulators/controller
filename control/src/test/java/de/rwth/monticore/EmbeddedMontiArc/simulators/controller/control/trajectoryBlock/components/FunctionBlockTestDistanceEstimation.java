/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.control.trajectoryBlock.components;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockParameter;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This is a test for the FunctionBlock DistanceEstimation
 *
 * Created by Christoph Grüne on 25.01.2017.
 */
public class FunctionBlockTestDistanceEstimation extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestDistanceEstimation(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestDistanceEstimation.class);
    }

    /**
     * tests if all inputs (see below) are handled correctly:
     *      velocity in [0, 500]
     *      constantBrakesMaxAcceleration in [0.1, 20.0]
     */
    public void testDistanceEstimation() {

        FunctionBlock distanceEstimation = new DistanceEstimation();

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        Double result, compareResult;

        for(double sensorVelocity = 0.0; sensorVelocity < 500.0; sensorVelocity += 0.16854546) {
            for(double constantBrakesMaxAcceleration = 0.1; constantBrakesMaxAcceleration < 20.0; constantBrakesMaxAcceleration += 0.0136836372) {
                input.put(ConnectionEntry.DISTANCE_ESTIMATION_current_velocity.toString(), sensorVelocity);
                input.put(ConnectionEntry.DISTANCE_ESTIMATION_brakes_max_acceleration.toString(), constantBrakesMaxAcceleration);

                distanceEstimation.setInputs(input);
                distanceEstimation.execute(1);
                result = (Double) distanceEstimation.getOutputs().get(ConnectionEntry.DISTANCE_ESTIMATION_estimated_distance.toString());

                compareResult = (sensorVelocity * sensorVelocity) / (2 * constantBrakesMaxAcceleration);
                compareResult = compareResult * FunctionBlockParameter.DISTANCE_ESTIMATION_multiplication.getParameter() + FunctionBlockParameter.DISTANCE_ESTIMATION_offset.getParameter();

                if (result.doubleValue() != compareResult.doubleValue()) {
                    assertFalse(true);
                }
            }
        }

        assertTrue(true);
    }


    /*****************************
     * helper functions          *
     *****************************/
}
