/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators.PathGenerator;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.math3.linear.ArrayRealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.*;

/**
 * This is a test for the FunctionBlock CreateIterator.
 *
 * Created by Christoph Grüne on 25.01.2017.
 */
public class FunctionBlockTestCreateIterator extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestCreateIterator(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestCreateIterator.class);
    }

    /**
     * tests the block for a List<Vertex> which is a straight line path with a length of 20.0
     */
    public void testCreateIterator1() {

        FunctionBlock createPathIterator = new CreateIterator();

        PathGenerator pathGenerator = new PathGenerator();

        List<Vertex> path = pathGenerator.generateStraightLine(20.0, 0.0, 0.0, new ArrayRealVector(new double[] {0.0, 1.0}), 0);
        Iterator<Vertex> compareResult = path.iterator();

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        input.put(ConnectionEntry.CREATE_ITERATOR_list.toString(), path);

        createPathIterator.setInputs(input);
        createPathIterator.execute(1);
        Iterator<Vertex> result = (Iterator<Vertex>) createPathIterator.getOutputs().get(ConnectionEntry.CREATE_ITERATOR_iterator.toString());

        for(Vertex vec : path) {
            if (!result.next().equals(compareResult.next())) {
                assertFalse(true);
            }
        }

        assertTrue(true);
    }

    /**
     * tests the block for the List<Integer> {0, 1, 2, 3, 4, 5}. It crates another Iterator and tests if all following Elmenents are equal.
     */
    public void testCreateIterator2() {

        FunctionBlock createPathIterator = new CreateIterator();

        PathGenerator pathGenerator = new PathGenerator();

        List<Integer> list = new LinkedList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        input.put(ConnectionEntry.CREATE_ITERATOR_list.toString(), list);

        for(int i = 0; i < 6; ++i) {

            input.put(ConnectionEntry.CREATE_ITERATOR_element_to_begin.toString(), list.get(i));

            createPathIterator.setInputs(input);
            createPathIterator.execute(1);
            Iterator<Integer> result = (Iterator<Integer>) createPathIterator.getOutputs().get(ConnectionEntry.CREATE_ITERATOR_iterator.toString());

            Iterator<Integer> compareIterator = list.listIterator(i);

            while(result.hasNext()) {
                if(!compareIterator.hasNext()) {
                    assertFalse(true);
                }
                Integer compareInteger = compareIterator.next();
                Integer resultInteger = result.next();
                if(compareInteger != resultInteger) {
                    assertFalse(true);
                }
            }
            if(compareIterator.hasNext()) {
                assertFalse(true);
            }
        }

        assertTrue(true);
    }


    /*****************************
     * helper functions          *
     *****************************/
}
