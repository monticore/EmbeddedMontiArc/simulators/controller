/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph on 11.03.2017.
 */
public class FunctionBlockTestMinimumDouble extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestMinimumDouble(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestMinimumDouble.class);
    }

    /**
     * tests for all combinations of the set {0, 1.4, 0, 2.45, 3.453343, 0.0000001} if the functionBlock finds the maximum every time
     */
    public void testMinimumDouble() {

        FunctionBlock minimumDouble = new MinimumDouble();

        List<Double> list = new LinkedList<>();
        list.add(0.0);
        list.add(1.4);
        list.add(0.0);
        list.add(2.45);
        list.add(3.453343);
        list.add(0.0000001);

        for(int i = 0; i < list.size(); ++i) {
            for(int j = 0; j < list.size(); ++j) {
                Map<String, Object> input = new LinkedHashMap<String, Object>();
                input.put(ConnectionEntry.MINIMUM_DOUBLE_double_1.toString(), list.get(i));
                input.put(ConnectionEntry.MINIMUM_DOUBLE_double_2.toString(), list.get(j));

                minimumDouble.setInputs(input);
                minimumDouble.execute(1);
                Double result = (Double) minimumDouble.getOutputs().get(ConnectionEntry.MINIMUM_DOUBLE_minimum.toString());

                double compareResult = list.get(i);
                if (list.get(j) < compareResult) {
                    compareResult = list.get(j);
                }

                if (compareResult != result) {
                    assertFalse(true);
                }
            }
        }
        assertTrue(true);
    }


    /*****************************
     * helper functions          *
     *****************************/

}
