/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph on 10.03.2017.
 */
public class FunctionBlockTestMinimumOfDoubleList extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestMinimumOfDoubleList(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestMinimumOfDoubleList.class);
    }

    /**
     * tests the function block for the list {0.0, 1.4, 0.0, 2.45, 3.453343, 0.0000001}
     */
    public void testMinimumOfDoubleList() {

        FunctionBlock minimumOfDoubleList = new MinimumOfDoubleList();

        List<Double> list = new LinkedList<>();
        list.add(0.0);
        list.add(1.4);
        list.add(0.0);
        list.add(2.45);
        list.add(3.453343);
        list.add(0.0000001);

        Map<String, Object> input = new LinkedHashMap<String, Object>();
        input.put(ConnectionEntry.MINIMUM_OF_DOUBLE_LIST_list.toString(), list);

        minimumOfDoubleList.setInputs(input);
        minimumOfDoubleList.execute(1);
        Double result = (Double) minimumOfDoubleList.getOutputs().get(ConnectionEntry.MINIMUM_OF_DOUBLE_LIST_minimum.toString());

        double compareResult = list.get(0);
        for(Double d : list) {
            if(d < compareResult) {
                compareResult = d;
            }
        }

        if(compareResult != result) {
            assertFalse(true);
        }
        assertTrue(true);
    }


    /*****************************
     * helper functions          *
     *****************************/
}
