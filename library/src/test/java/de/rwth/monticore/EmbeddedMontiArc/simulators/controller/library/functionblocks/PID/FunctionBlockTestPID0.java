/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.PID;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Unit test for PID. Test of a constant signal which is zero.
 *
 * Created on 06.11.2016
 */
public class FunctionBlockTestPID0 extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestPID0(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestPID0.class);
    }

    /**
     * test, if the input signal contains only zeros, the output is only zeros, too.
     */
    public void testPID_signalContainsZeros() {

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        PID pid = new PID(0.5, 0.3, 0.6);

        Double res;

        for(Double time_d = 0.1; time_d < 10.0; time_d += 0.1){

            input.put(ConnectionEntry.PID_desired_value.toString(), (Double) 0.0);
            input.put(ConnectionEntry.PID_current_value.toString(), (Double) 0.0);
            //input.put(ConnectionEntry.PID_delta_time.toString(), time_d);

            pid.setInputs(input);
            pid.execute(time_d);
            res = (Double) pid.getOutputs().get(ConnectionEntry.PID_new_controlled_value.toString());
            assertFalse(res != 0.0);
        }

        assertTrue(true);

    }
}
