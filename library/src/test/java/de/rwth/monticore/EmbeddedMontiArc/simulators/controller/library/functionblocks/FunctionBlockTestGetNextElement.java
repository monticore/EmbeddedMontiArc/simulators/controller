/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.*;

/**
 * Test for the FunctionBlock GetNextElement.
 *
 * Created by Christoph Grüne on 10.03.2017.
 */
public class FunctionBlockTestGetNextElement extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestGetNextElement(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestGetNextElement.class);
    }

    /**
     * this tests for the list, if all elements are get as well as if the listEnd flag is set at the end of the list.
     */
    public void testGetNextElement() {

        FunctionBlock getNextElement = new GetNextElement<Integer>();

        List<Integer> list = new LinkedList<>();
        list.add(0);
        list.add(1);
        list.add(2);

        Iterator<Integer> iterator = list.iterator();

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        input.put(ConnectionEntry.GET_NEXT_ELEMENT_iterator.toString(), iterator);

        for(int i = 0; i < list.size(); ++i) {
            getNextElement.setInputs(input);
            getNextElement.execute(1);
            Integer result = (Integer) getNextElement.getOutputs().get(ConnectionEntry.GET_NEXT_ELEMENT_next_element.toString());
            Boolean pathEnd = (Boolean) getNextElement.getOutputs().get(ConnectionEntry.GET_NEXT_ELEMENT_list_end.toString());

            if (i < list.size() - 1 && result != i) {
                assertFalse(true);
            }
            if(i == list.size() - 1 && !pathEnd) {
                assertFalse(true);
            }
        }

        assertTrue(true);
    }


    /*****************************
     * helper functions          *
     *****************************/
}
