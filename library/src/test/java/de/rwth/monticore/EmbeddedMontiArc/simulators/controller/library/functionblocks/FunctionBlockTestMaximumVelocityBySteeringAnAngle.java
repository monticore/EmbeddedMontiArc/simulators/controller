/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Christoph on 11.03.2017.
 */
public class FunctionBlockTestMaximumVelocityBySteeringAnAngle extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestMaximumVelocityBySteeringAnAngle(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestMaximumVelocityBySteeringAnAngle.class);
    }

    /**
     * tests if all inputs (see below) are handled correctly:
     *      for all Surface.values()
     *      steeringAngle in [-5, 5]
     *      weather in [0, 1]
     */
    public void testMaximumVelocityBySteeringAnAngle() {
        Double rainCoefficient;
        Double newSteeringAngle;

        for (Surface s : Surface.values()) {
            for (rainCoefficient = 0.0; rainCoefficient <= 1.0; rainCoefficient += 0.1) {
                for (newSteeringAngle = -5.0; newSteeringAngle <= 5.0; newSteeringAngle += 0.1) {
                    double result = executeTestCase(s, rainCoefficient, newSteeringAngle);

                    double compareResult = s.getParameterA() * Math.exp(Math.abs(newSteeringAngle) * s.getParameterB(rainCoefficient)) + s.getParameterC();

                    if (result != compareResult) {
                        assertFalse(true);
                    }
                }
            }
        }

        assertTrue(true);
    }

    /*****************************
     * helper functions          *
     *****************************/
    /**
     * this function sets inputs, executes the function block, and returns the outputs
     *
     * @param s the current surface the car drives on
     * @param rainCoefficient the current weather coefficient
     * @param newSteeringAngle the steering angle
     * @return the output(s) of the executed function block
     */
    private Double executeTestCase(Surface s, Double rainCoefficient, Double newSteeringAngle) {

        Map<String, Object> inputs = new LinkedHashMap<>();
        inputs.put(ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_surface.toString(), s);
        inputs.put(ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_weather.toString(), rainCoefficient);
        inputs.put(ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_steering_angle.toString(), newSteeringAngle);

        FunctionBlock maximumVelocityBySteeringAnAngle = new MaximumVelocityBySteeringAnAngle();

        maximumVelocityBySteeringAnAngle.setInputs(inputs);
        maximumVelocityBySteeringAnAngle.execute(1);

        Double result = (Double) maximumVelocityBySteeringAnAngle.getOutputs().get(ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_velocity.toString());

        return result;
    }
}
