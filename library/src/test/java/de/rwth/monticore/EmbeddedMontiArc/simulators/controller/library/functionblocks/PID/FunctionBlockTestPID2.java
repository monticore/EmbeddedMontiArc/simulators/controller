/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.PID;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Unit test for PID. Test of a monotonically decreasing signal.
 *
 * Created on 06.11.2016
 */
public class FunctionBlockTestPID2 extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FunctionBlockTestPID2(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FunctionBlockTestPID2.class);
    }

    /**
     * test, if the input signal is monotonically increasing, the output is monotonically increasing
     */
    public void testPID_signalDecreasesMonotonically() {

        Map<String, Object> input = new LinkedHashMap<String, Object>();

        PID pid = new PID(5, 3, 3);

        Double velocity = 100.0;
        Double res;
        Double d = 10.0;

        while(d > 2.0) {
            d -= 0.1;

            input.put(ConnectionEntry.PID_desired_value.toString(), d);
            input.put(ConnectionEntry.PID_current_value.toString(), velocity);
            //input.put(ConnectionEntry.PID_delta_time.toString(), 0.01);

            pid.setInputs(input);
            pid.execute(0.01);
            res = (Double) pid.getOutputs().get(ConnectionEntry.PID_new_controlled_value.toString());

            if(velocity > d) {
                assertFalse(res > 0.0);
            } else {
                assertFalse(res < 0.0);
            }
            velocity += res;
        }

        pid = new PID(5, 3, 3);
        velocity = 10.0;
        d = 10.0;

        while(d > 0.0) {
            d -= 0.1;

            input.put(ConnectionEntry.PID_desired_value.toString(), d);
            input.put(ConnectionEntry.PID_current_value.toString(), velocity);
            //input.put(ConnectionEntry.PID_delta_time.toString(), 0.01);

            pid.setInputs(input);
            pid.execute(0.01);
            res = (Double) pid.getOutputs().get(ConnectionEntry.PID_new_controlled_value.toString());

            if(velocity > 0.0) {
                assertFalse(res > 0.0);
            } else {
                assertFalse(res < 0.0);
            }
            velocity += res;
        }

        assertTrue(true);

    }
}
