/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */

package exceptions;

/**
 * This is an Exception for the Dijkstra algorithm in the FindPath block.
 * It is thrown if a vertex is not reachable.
 *
 * Created by Christoph Grüne on 08.03.2017.
 */
public class UnreachableVertexException extends RuntimeException {
    public UnreachableVertexException() {
        super();
    }
    public UnreachableVertexException(String s) {
        super(s);
    }
    public UnreachableVertexException(String s, Throwable throwable) {
        super(s, throwable);
    }
    public UnreachableVertexException(Throwable throwable) {
        super(throwable);
    }
}
