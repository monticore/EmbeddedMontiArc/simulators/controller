/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */

package exceptions;

/**
 * This is an Exception for the FindPath block.
 * It is thrown if the TargetNode is not present in the graph respectively adjacency list.
 *
 * Created by Christoph Grüne on 19.03.2017.
 */
public class IllegalTargetNodeException extends RuntimeException {
    public IllegalTargetNodeException() {
            super();
        }
    public IllegalTargetNodeException(String s) {
            super(s);
        }
    public IllegalTargetNodeException(String s, Throwable throwable) {
            super(s, throwable);
        }
    public IllegalTargetNodeException(Throwable throwable) {
            super(throwable);
        }
}
