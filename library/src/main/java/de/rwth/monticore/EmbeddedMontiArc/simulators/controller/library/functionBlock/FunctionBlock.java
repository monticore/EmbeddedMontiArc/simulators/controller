/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.interfaces.FunctionBlockInterface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.exceptions.EntryIsNullException;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * This abstract class provides the basic structures of a function block.
 * It is important to call super.setInputs(Map<>) in every sub block.
 * Otherwise, the FunctionBlockManagement does not work, because it needs the connectionMap, which is setted by setInputs(Map<>).
 *
 * Created by Christoph Grüne on 09.12.2016.
 */
public abstract class FunctionBlock implements FunctionBlockInterface {

    protected Map<String, Object> connectionMap;

    public FunctionBlock() {
        connectionMap = new LinkedHashMap<String, Object>();
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/
    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified key
     */
    public void setInputs(Map<String, Object> inputs) {
        //check for null entries -> fail fast
        Set<String> keySet = inputs.keySet();
        setConnectionMap(inputs, keySet);
    }

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified key
     * @param args all entries that are allowed to be null
     */
    public void setInputs(Map<String, Object> inputs, String... args) {
        //check for null entries -> fail fast
        Set<String> keySet = inputs.keySet();
        for(int i = 0; i < args.length; ++i) {
            if(keySet.contains(args[i])) {
                keySet.remove(args[i]);
            }
        }
        setConnectionMap(inputs, keySet);
    }

    /**
     * helper method for setInputs(Map<String, Object>) and setInputs(Map<String, Object>, String...).
     * It checks all entries in the map if they are null and sets the connectionMap.
     */
    private void setConnectionMap(Map<String, Object> inputs, Set<String> keySet) {
        for(String key : keySet) {
            if (inputs.get(key) == null) {
                throw new EntryIsNullException(key + " in " + this.toString() + " is null!");
            }
        }
        this.connectionMap.putAll(inputs);
        //WARNING TODO REMOVE THIS AFTER TESTING
        connectionMap.put(BusEntry.COMPUTERVISION_VANISHING_POINT.toString(), 0.0);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public abstract Map<String, Object> getOutputs();

    /**
     * returns the connectionMap
     *
     * @return connectionMap
     */
    public Map<String, Object> getConnectionMap() {
        return connectionMap;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public abstract String[] getImportNames();
}

