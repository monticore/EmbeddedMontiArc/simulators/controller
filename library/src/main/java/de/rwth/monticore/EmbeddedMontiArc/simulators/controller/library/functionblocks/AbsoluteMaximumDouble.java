/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */

package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This function block calculates the maximum of the to passed Doubles.
 *      used formulas: none
 *
 *      used sub blocks: none
 *
 * Created by Christoph Grüne on 22.03.2017.
 */
public class AbsoluteMaximumDouble extends FunctionBlock {

    //Input Variables
    Double double1;
    Double double2;

    //Output Variables
    Double maximum;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public AbsoluteMaximumDouble() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        if(Math.abs(double1) >= Math.abs(double2)) {
            maximum = double1.doubleValue();
        } else {
            maximum = double2.doubleValue();
        }
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        double1 = (Double) inputs.get(ConnectionEntry.ABSOLUTE_MAXIMUM_DOUBLE_double_1.toString());
        double2 = (Double) inputs.get(ConnectionEntry.ABSOLUTE_MAXIMUM_DOUBLE_double_2.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.ABSOLUTE_MAXIMUM_DOUBLE_maximum.toString(), maximum);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.MINIMUM_DOUBLE_double_1.toString(),
                ConnectionEntry.MINIMUM_DOUBLE_double_2.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
}
