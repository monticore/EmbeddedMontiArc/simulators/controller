/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * This function block calculates the minimum of a double list.
 *      used formulas: none
 *
 *      used sub blocks: none
 *
 * Created by Christoph Grüne on 10.03.2017.
 */
public class MinimumOfDoubleList extends FunctionBlock{

    //Input Variables
    List<Double> list;

    //Output Variables
    Double minimum;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public MinimumOfDoubleList() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        if(list.size() == 0) {
            minimum = null;
        } else {
            minimum = list.get(0);
        }
        for(Double d : list) {
            if(d < minimum) {
                minimum = d;
            }
        }
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        list = (List<Double>) inputs.get(ConnectionEntry.MINIMUM_OF_DOUBLE_LIST_list.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.MINIMUM_OF_DOUBLE_LIST_minimum.toString(), minimum);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.MINIMUM_OF_DOUBLE_LIST_list.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
}
