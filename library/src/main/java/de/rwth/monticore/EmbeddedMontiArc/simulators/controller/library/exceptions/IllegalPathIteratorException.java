/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.exceptions;

/**
 * This is an Exception for the blocks PartitionPath and Velocity Estimation.
 * It is thrown if an incorrect path iterator was passed.
 *
 * Created by Christoph Grüne on 08.03.2017.
 */
public class IllegalPathIteratorException extends RuntimeException {
    public IllegalPathIteratorException() {
        super();
    }
    public IllegalPathIteratorException(String s) {
        super(s);
    }
    public IllegalPathIteratorException(String s, Throwable throwable) {
        super(s, throwable);
    }
    public IllegalPathIteratorException(Throwable throwable) {
        super(throwable);
    }
}
