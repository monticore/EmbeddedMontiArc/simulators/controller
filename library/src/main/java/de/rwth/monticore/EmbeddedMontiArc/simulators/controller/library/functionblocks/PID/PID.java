/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.PID;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This function block implements a Propotional-Integral-Derivative (PID) controller
 *
 * Created by Christoph Grüne on 06.11.2016
 */
public class PID extends FunctionBlock {

    //Input Arguments
    private Double desiredValue;
    private Double currentValue;

    //Output Arguments
    private Double controlledValue;

    //Global Variables
    private double k_p;
    private double k_i;
    private double k_d;

    private Double previousValueDerivative = 0.0;
    private Double previousValueIntegral = 0.0;
    private Double oldVal = 0.0;


    /**
     * Constructs a PID controller object
     *
     * @param k_p proportional coefficient
     * @param k_i integral coefficient
     * @param k_d differential coefficient
     */
    public PID(double k_p, double k_i, double k_d) {
        this.k_p = k_p;
        this.k_i = k_i;
        this.k_d = k_d;
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/
    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        controlledValue = calculatePID(desiredValue, currentValue, timeDelta);
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        desiredValue = (Double) inputs.get(ConnectionEntry.PID_desired_value.toString());
        currentValue = (Double) inputs.get(ConnectionEntry.PID_current_value.toString());
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.PID_new_controlled_value.toString(), controlledValue);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.PID_desired_value.toString(),
                ConnectionEntry.PID_current_value.toString()//,
                //ConnectionEntry.PID_delta_time.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/

    /**
     * calculates u(t) = k_p * e(t) + k_i * integral_0_t(e(t)) + k_d * de(t)/dt with e(t) = r(t) - y(t)
     *
     * @param r the intended value in si unit
     * @param y the currently measured value in si unit (e.g. meters per second)
     * @param deltaTime the delta time in seconds
     * @return the value of u(t) (see above)
     */
    private double calculatePID(double r, double y, double deltaTime) {
        double u;
        double e;
        double derivative = 0;
        double integral = 0;

        e = r - y;

        //branch: a functionblocks.PID controller can be an PI and a PD controller as well, that is, these constants may be zero
        if(k_d != 0) {
            derivative = calculateDerivative(e, deltaTime);
        }
        if(k_i != 0) {
            integral = calculateIntegral(e, deltaTime);
        }

        u = k_p * e + k_i * integral + k_d * derivative;

        return u;
    }

    /**
     * differentiates by calculating the difference quotient using the default difference between two points in time
     *
     * @param val_i the current value of the function
     * @param deltaTime the delta time in seconds
     * @return      the derivative in microseconds
     * @throws IllegalStateException if the current default difference is not positive
     */
    public double calculateDerivative(double val_i, double deltaTime) {
        double eDerivative;

        if(deltaTime <= 0) {
            throw new IllegalStateException("This time step size is not positive: " + deltaTime);
        }

        //calculate the gradient
        eDerivative = val_i - previousValueDerivative;
        eDerivative /= (double)deltaTime;

        //update the previous values
        previousValueDerivative = val_i;

        return eDerivative;
    }

    /**
     * apply numerical integration by using the mean of current speed und previous speed
     *
     * @param val_i the current value of the function
     * @param deltaTime the delta time in seconds
     * @return the Integral
     * @throws IllegalArgumentException if the current point in time is earlier or equal to the previous one
     */
    public double calculateIntegral(double val_i, double deltaTime)
    {
        double eIntegral;

        if(deltaTime <= 0)
        {
            throw new IllegalArgumentException("This time step size is not positive: " + deltaTime);
        }

        //calculate the Integration
        //use the mean value to approximate, regard the speed function as a linear function
        eIntegral = oldVal + (deltaTime)*((val_i + previousValueIntegral)/2);

        //update the previous value
        oldVal = eIntegral;
        previousValueIntegral = val_i;

        return eIntegral;

    }
}
