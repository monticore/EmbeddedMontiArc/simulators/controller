/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This FunctionBlock get the next element of an Iterator<T>, where T is a generic type.
 *      used formulas: none
 *
 *      used sub blocks: none
 *
 * Created by Christoph Grüne on 10.03.2017.
 */
public class GetNextElement<T> extends FunctionBlock {

    //Input Variables
    private Iterator<T> iterator;

    //Output Variables
    private T nextElement;
    private Boolean listEnd = false;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public GetNextElement() {initComponents();}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        if(iterator.hasNext()) {
            nextElement = iterator.next();
            if(!iterator.hasNext()) {
                listEnd = true;
            }
        } else {
            listEnd = true;
        }
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        iterator = (Iterator<T>) inputs.get(ConnectionEntry.GET_NEXT_ELEMENT_iterator.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.GET_NEXT_ELEMENT_next_element.toString(), nextElement);
        outputs.put(ConnectionEntry.GET_NEXT_ELEMENT_list_end.toString(), listEnd);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.GET_NEXT_ELEMENT_iterator.toString(),
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * initialises all components such as sub function blocks
     * initialises the functionBlockManagement for this block
     */
    private void initComponents() {}
}
