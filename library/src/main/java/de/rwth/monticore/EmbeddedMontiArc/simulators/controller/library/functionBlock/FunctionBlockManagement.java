/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.exceptions.IllegalConnectionOfSubBlocksException;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.exceptions.IllegalFunctionBlockConnectionException;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * this class provides the functionality to connect blocks in a "super block".
 * One has to add one FunctionBlockManagement object per block to use the functionality.
 * The following operations are provided.
 *      add a FunctionBlock as a sub block
 *      add a connection between two ports of function blocks
 *      execute all added function blocks (future: with respect to the execution ordering specified by all connections)
 *
 * Created by Christoph Grüne on 18.01.2017.
 */
public class FunctionBlockManagement {

    private LinkedList<FunctionBlock> usedFunctionBlocks;

    private Map<FunctionBlock, AccessToFunctionBlock> accessMap;
    private Map<FunctionBlock, List<Connection>> connectionMap;
    private Map<FunctionBlock, List<Connection>> outputMap;

    private FunctionBlock superBlock;

    private boolean sorted = false;

    /**
     * constructor for FunctionBlockManagement
     * initialises all data structures to manage the FunctionBlock structure
     */
    public FunctionBlockManagement (FunctionBlock superBlock) {
        usedFunctionBlocks = new LinkedList<FunctionBlock>();

        accessMap = new LinkedHashMap<>();
        connectionMap = new LinkedHashMap<>();
        outputMap = new LinkedHashMap<>();

        this.superBlock = superBlock;
    }

    /**
     * connects port A of function block A to port B of function Block B
     *
     * @param superBlock is the super block of block A and B, note: A can be the super block of B and vice versa!
     * @param functionBlockA is the function block A
     * @param portA is the port name of function block A
     * @param functionBlockB is the function Block B
     * @param portB is the port name of function block B
     */
    public void connect(FunctionBlock superBlock, FunctionBlock functionBlockA, String portA, FunctionBlock functionBlockB, String portB) {
        helpConnection(superBlock, functionBlockA, portA, functionBlockB, portB, new Object(), false);
    }

    /**
     * connects port A of function block A to port B of function Block B
     *
     * @param superBlock is the super block of block A and B, note: A can be the super block of B and vice versa!
     * @param functionBlockA is the function block A
     * @param portA is the port name of function block A
     * @param functionBlockB is the function Block B
     * @param portB is the port name of function block B
     * @param initialValue the initial value of this connection
     */
    public void connect(FunctionBlock superBlock, FunctionBlock functionBlockA, String portA, FunctionBlock functionBlockB, String portB, Object initialValue) {
        helpConnection(superBlock, functionBlockA, portA, functionBlockB, portB, initialValue, true);
    }

    /**
     * helper for the connect() methods
     *
     * @param superBlock is the super block of block A and B, note: A can be the super block of B and vice versa!
     * @param functionBlockA is the function block A
     * @param portA is the port name of function block A
     * @param functionBlockB is the function Block B
     * @param portB is the port name of function block B
     * @param initialValue the initial value of this connection
     * @param hasInitialValue contains if the connection has an initial value
     */
    private void helpConnection(FunctionBlock superBlock,
                                FunctionBlock functionBlockA,
                                String portA,
                                FunctionBlock functionBlockB,
                                String portB,
                                Object initialValue,
                                boolean hasInitialValue) {

        if(superBlock != this.superBlock) {
            throw new IllegalFunctionBlockConnectionException(superBlock.toString() + " is not the correct super block, change it to " + this.superBlock.getClass().getName() + ". Call connect only in the superBlock and pass 'this' as argument!");
        }
        if(!usedFunctionBlocks.contains(functionBlockA) && functionBlockA != superBlock) {
            throw new IllegalFunctionBlockConnectionException(functionBlockA.toString() + " is not added to " + this.superBlock.toString() + ". Add this FunctionBlock before you connect it.");
        }
        if(!usedFunctionBlocks.contains(functionBlockB) && functionBlockB != superBlock) {
            throw new IllegalFunctionBlockConnectionException(functionBlockB.toString() + " is not added to " + this.superBlock.toString() + ". Add this FunctionBlock before you connect it.");
        }

        if(functionBlockB == superBlock) {
            if(!outputMap.containsKey(functionBlockA)) {
                outputMap.put(functionBlockA, new LinkedList<>());
            }
            Connection connection = new Connection(superBlock, functionBlockA, portA, functionBlockB, portB);
            outputMap.get(functionBlockA).add(connection);
        } else {
            if (!accessMap.containsKey(functionBlockB) && functionBlockB != superBlock) {
                accessMap.put(functionBlockB, new AccessToFunctionBlock());
            }
            if (!connectionMap.containsKey(functionBlockB)) {
                connectionMap.put(functionBlockB, new LinkedList<>());
            }
            if(hasInitialValue) {
                Connection connection = new Connection(superBlock, functionBlockA, portA, functionBlockB, portB, initialValue);
                connectionMap.get(functionBlockB).add(connection);
            } else {
                Connection connection = new Connection(superBlock, functionBlockA, portA, functionBlockB, portB);
                connectionMap.get(functionBlockB).add(connection);
            }
        }
    }

    /**
     * executes the given functionBlock and returns the outputs
     *
     * @param functionBlock the function block that will be executed
     * @return outputs of the executed Function Block
     */
    private Map<String, Object> executeFunctionBlock(FunctionBlock functionBlock, double timeDelta) {

        if(!usedFunctionBlocks.contains(functionBlock)) {
            throw new IllegalArgumentException(functionBlock.toString() + " is not added to the superBlock! Add this FunctionBlock before you execute it.");
        }
        accessMap.get(functionBlock).collectAllInputs(connectionMap.get(functionBlock));
        functionBlock.setInputs(accessMap.get(functionBlock).getMap());
        functionBlock.execute(timeDelta);
        Map<String, Object> outputs = new LinkedHashMap<>();
        if(outputMap.containsKey(functionBlock)) {
            for(Connection connection : outputMap.get(functionBlock)) {
                outputs.put(connection.getPortB(), functionBlock.getOutputs().get(connection.getPortA()));
            }
        }
        return outputs;
    }

    /**
     * executes several function blocks which are connected with each other
     */
    public Map<String, Object> executeAllFunctionBlocks(double timeDelta) {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();

        if(!sorted) {
            findExecutionOrder();
            sorted = true;
        }

        for(FunctionBlock functionBlock : usedFunctionBlocks) {
            outputs.putAll(executeFunctionBlock(functionBlock, timeDelta));
        }

        return outputs;
    }

    /**
     * sorts the function block by their execution order
     */
    private void findExecutionOrder() {
        LinkedList<FunctionBlock> notS = new LinkedList<>();
        LinkedList<FunctionBlock> S = new LinkedList<>();

        notS.addAll(usedFunctionBlocks);

        while(!notS.isEmpty()) {
            int sizeOfNotS = notS.size();
            for(FunctionBlock functionBlock : notS) {
                boolean addableFunctionBlock = true;
                for(Connection connection : connectionMap.get(functionBlock)) {
                    if(!S.contains(connection.getFunctionBlockA())
                            && connection.getFunctionBlockA() != connection.getSuperBlock()
                            && !connection.hasInitialValue()) {
                        addableFunctionBlock = false;
                    }
                }
                if(addableFunctionBlock) {
                    notS.remove(functionBlock);
                    S.add(functionBlock);
                    break;
                }
            }
            if(sizeOfNotS == notS.size()) {
                throw new IllegalConnectionOfSubBlocksException("The connections in " + this.superBlock.toString() + " are not correct.");
            }
        }
        usedFunctionBlocks = S;
    }

    /**
     * function to add a sub functionBlock
     *
     * @param functionBlock is the new sub function block
     */
    public void addFunctionBlock(FunctionBlock functionBlock) {
        usedFunctionBlocks.addLast(functionBlock);
    }
}
