/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IControllerNode;

/**
 * this enum contains all parameters that can and should be tuned for the current function block setting.
 *
 * Created by Christoph on 03.03.2017.
 */
public enum FunctionBlockParameter {

    //***************************************************************************
    //*  Velocity PID Controller (innerControlBlock)                            *
    //***************************************************************************
    /*
    with Ziegler-Nichols-Tuning, we estimate
    k_krit = 3
    t_krit = 2 seconds

    In order to decrease overshoot, the formula of wikipedia is used:
    k_p = 0.2 * k_krit
    k_i = k_p / (t_krit / 2)
    k_d = k_p * (t_krit / 3)
    */
    VELOCITY_PID_CONTROLLER_P(0.2 * 3),
    VELOCITY_PID_CONTROLLER_I(VELOCITY_PID_CONTROLLER_P.getParameter() / (2 / 2.0)),
    VELOCITY_PID_CONTROLLER_D(VELOCITY_PID_CONTROLLER_P.getParameter() * (2 / 3.0)),
    //***************************************************************************
    //*  Steering PID Controller (innerControlBlock)                            *
    //***************************************************************************
    /*
    Guessed values for steering PID
    tuning with Ziegler-Nichols would have been possible if there was more time
     */
    STEERING_PID_CONTROLLER_P(5.0),
    STEERING_PID_CONTROLLER_I(0.0),
    STEERING_PID_CONTROLLER_D(4.0),
    //***************************************************************************
    //*  MaximumSteeringAngleOfSection (trajectoryPlanningBlock)                *
    //***************************************************************************
    LENGTH_OF_SECTIONS(10.0),
    DISTANCE_OF_MEASURING_POINTS(IControllerNode.INTERPOLATION_DISTANCE),
    //***************************************************************************
    //*  DistanceEstimation (trajectoryBlock)                                   *
    //***************************************************************************
    DISTANCE_ESTIMATION_multiplication(1.5),
    DISTANCE_ESTIMATION_offset(4.0);

    private final Double parameter;

    private FunctionBlockParameter(Double parameter) {
        this.parameter = parameter;
    }

    public Double getParameter() {
        return this.parameter;
    }
}
