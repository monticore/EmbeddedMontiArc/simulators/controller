/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock;

/**
 * this class stores all necessary information for a connection in a block. It is widely used in the FunctionBlockManagement
 *
 * Created by Christoph on 16.01.2017.
 */
public class Connection {

    private FunctionBlock superBlock;
    private FunctionBlock functionBlockA;
    private String portA;
    private FunctionBlock functionBlockB;
    private String portB;

    private Object initialValue;
    private boolean hasInitialValue;

    /**
     * constructs a connection between function block A and function Block B from port portA to port portB in a super Block
     *
     * @param superBlock is the super block
     * @param functionBlockA is block A
     * @param portA is port A
     * @param functionBlockB is block B
     * @param portB is port B
     */
    public Connection(FunctionBlock superBlock, FunctionBlock functionBlockA, String portA, FunctionBlock functionBlockB, String portB) {
        this.superBlock = superBlock;
        this.functionBlockA = functionBlockA;
        this.portA = portA;
        this.functionBlockB = functionBlockB;
        this.portB = portB;
        this.hasInitialValue = false;
    }

    /**
     * constructs a connection between function block A and function Block B from port portA to port portB in a super Block
     * with an initail value
     *
     * @param superBlock is the super block
     * @param functionBlockA is block A
     * @param portA is port A
     * @param functionBlockB is block B
     * @param portB is port B
     */
    public Connection(FunctionBlock superBlock, FunctionBlock functionBlockA, String portA, FunctionBlock functionBlockB, String portB, Object initialValue) {
        this.superBlock = superBlock;
        this.functionBlockA = functionBlockA;
        this.portA = portA;
        this.functionBlockB = functionBlockB;
        this.portB = portB;
        this.initialValue = initialValue;
        this.hasInitialValue = true;
    }

    /**
     * getter for superBlock
     *
     * @return the super block of the connection
     */
    public FunctionBlock getSuperBlock() {
        return superBlock;
    }

    /**
     * getter for function block A
     *
     * @return start function block of the connection
     */
    public FunctionBlock getFunctionBlockA() {
        return functionBlockA;
    }

    /**
     * getter for port A
     *
     * @return start port of the connection
     */
    public String getPortA() {
        return portA;
    }

    /**
     * getter for function block B
     *
     * @return end function block of the connection
     */
    public FunctionBlock getFunctionBlockB() {
        return functionBlockB;
    }

    /**
     * getter for port B
     *
     * @return end port of the connection
     */
    public String getPortB() {
        return portB;
    }

    /**
     * Getter for the initial value of the connection
     *
     * @return the initial value
     */
    public Object getInitialValue() {
        return initialValue;
    }

    /**
     * returns whether the connection has an initial value
     *
     * @return true: if the connection has an initial value, false: otherwise
     */
    public boolean hasInitialValue() {
        if(hasInitialValue) {
            return true;
        }
        return false;
    }
}
