/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.databus;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.BusEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.interfaces.Bus;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This class implements a DataBus. This is a Map<String, Object> with specified operations.
 *
 * Created by Christoph Grüne on 20.12.2016.
 * Implemented by Christoph Grüne.
 */
public class DataBus implements Bus {

    //Map that contains all information
    private Map<String, Object> busMap;

    /**
     * constructor for a Databus
     */
    public DataBus() {
        busMap = new LinkedHashMap<String, Object>();
    }

    /**
     * puts a specific value to a key
     *
     * @param key is the key
     * @param object is the object
     */
    public void setData(String key, Object object) {
        busMap.put(key, object);
    }

    /**
     * integrates a existing map into this one
     *
     * @param map is the map
     */
    public void setAllData(Map<String, Object> map) {
        busMap.putAll(map);
    }

    /**
     * returns the object of a certain key
     *
     * @param key is the key of the object
     * @return the object with key key
     */
    public Object getData(String key) {
        return busMap.get(key);
    }

    /**
     * returns the connectionMap map
     *
     * @return busMap the Map that contains all information
     */
    public Map<String, Object> getAllData() {
        return busMap;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                BusEntry.NAVIGATION_DETAILED_PATH_WITH_MAX_STEERING_ANGLE.toString(),
                BusEntry.CONSTANT_MAXIMUM_TOTAL_VELOCITY.toString(),
                BusEntry.SIMULATION_DELTA_TIME.toString(),
                BusEntry.CONSTANT_NUMBER_OF_GEARS.toString(),
                BusEntry.CONSTANT_WHEELBASE.toString(),
                BusEntry.CONSTANT_MOTOR_MAX_ACCELERATION.toString(),
                BusEntry.CONSTANT_MOTOR_MIN_ACCELERATION.toString(),
                BusEntry.CONSTANT_BRAKES_MAX_ACCELERATION.toString(),
                BusEntry.CONSTANT_BRAKES_MIN_ACCELERATION.toString(),
                BusEntry.CONSTANT_STEERING_MAX_ANGLE.toString(),
                BusEntry.CONSTANT_STEERING_MIN_ANGLE.toString(),
                BusEntry.CONSTANT_TRAJECTORY_ERROR.toString(),
                BusEntry.VEHICLE_MAX_TEMPORARY_ALLOWED_VELOCITY.toString(),
                BusEntry.SENSOR_VELOCITY.toString(),
                BusEntry.SENSOR_STEERING.toString(),
                BusEntry.SENSOR_DISTANCE_TO_RIGHT.toString(),
                BusEntry.SENSOR_DISTANCE_TO_LEFT.toString(),
                BusEntry.SENSOR_GPS_COORDINATES.toString(),
                BusEntry.SENSOR_CURRENT_SURFACE.toString(),
                BusEntry.SENSOR_WEATHER.toString(),
                BusEntry.SENSOR_CAMERA.toString(),
                BusEntry.SENSOR_COMPASS.toString(),
                BusEntry.COMPUTERVISION_VANISHING_POINT.toString(),
                BusEntry.COMPUTERVISION_DETECTED_CARS.toString(),
                BusEntry.COMPUTERVISION_DETECTED_PEDESTRIANS.toString(),
                BusEntry.COMPUTERVISION_DETECTED_LANES.toString(),
                BusEntry.COMPUTERVISION_DETECTED_SIGNS.toString(),
        };
        return names;
    }
}
