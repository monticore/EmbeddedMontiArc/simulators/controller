/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.Adjacency;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.ControllerNode;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IAdjacency;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IControllerNode;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.utils.Point3D;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Edge;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures.Graph;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.*;

/**
 * This class generates several types of graphs.
 * Warning: this class depends on the ControllerNode class. Therefor, it makes use of the special mapping of the ControllerNode IDs.
 * In short, one has to call getAdjacencyList(i) before you can call getGraph(i). Otherwise, you get an incorrect graph.
 *
 * Created by Christoph Grüne on 01.03.2017.
 */
public class GraphGenerator {

    /**
     * this variable contains the number of examples
     */
    public static final Integer NUMBER_OF_EXAMPLES = 2;
    private static double radius = 15.0;
    private static double constantWheelbase = 4.0;

    private int counter_1 = 0;
    private int counter_2 = 0;

    /**
     * Setter for radius
     *
     * @param r the new value for radius
     */
    public void setRadius(double r) {radius = r;}

    /**
     * Getter for the i-th example graph
     * default: first graph
     *
     * @param i the index of the example
     * @return the i-th example graph, if NUMBER_OF_EXAMPLES < i: first example graph
     */
    public Graph getGraph(int i) {
        if(i == 2) {
            return getGraph_2();
        }
        return getGraph_1();
    }

    /**
     * Getter for the shortest path of the i-th example graph
     * default: shortest path of the first graph
     *
     * @param i the index of the example
     * @return the shortest path of the i-th example graph, if NUMBER_OF_EXAMPLES < i: first example graph
     */
    public List<Vertex> getShortestPath(int i, Vertex vStart, Vertex vEnd) {
        if(i == 2) {
            return getShortestPath_2(vStart, vEnd);
        }
        return getShortestPath_1(vStart, vEnd);
    }

    /**
     * Getter for the detailed shortest path with calculated steering angles of the i-th example graph
     * default: detailed shortest with calculated steering angles path of the first graph
     * a detailed path contains at least every two meters
     *
     * @param i the index of the example
     * @return the shortest path with calculated steering angles of the i-th example graph, if NUMBER_OF_EXAMPLES < i: first example graph
     */
    public List<Vertex> getDetailedPathWithMaximalSteeringAngle(int i, Vertex vStart, Vertex vEnd) {
        if(i == 2) {
            return getDetailedPathWithMaximalSteeringAngle_2(vStart, vEnd);
        }
        return getDetailedPathWithMaximalSteeringAngle_1(vStart, vEnd);
    }

    /**
     * Getter for the i-th example list of adjacencies
     * default: first list of adjacencies
     *
     * @param i the index of the example
     * @return the i-th example list of adjacencies, if NUMBER_OF_EXAMPLES < i: first example list of adjacencies
     */
    public List<IAdjacency> getIAdjacencyList(int i) {
        if(i == 2) {
            return getIAdjacencyList_2();
        }
        return getIAdjacencyList_1();
    }

    /**
     * Getter for the first example Graph for testing issues.
     * This is a "line" graph.
     *
     * @return a "line" graph
     */
    private Graph getGraph_1() {
        List<Vertex> vertices = new LinkedList<>();
        Map<Vertex, Map<Vertex, Edge>> edges = new LinkedHashMap<>();

        RealVector position1 = new ArrayRealVector(new Double[] {0.0, 0.0, 0.0});
        Vertex vertex1 = new Vertex(counter_1 + 1L, counter_1 + 0L, position1, 0.0);
        vertices.add(vertex1);
        RealVector position2 = new ArrayRealVector(new Double[] {1.0, 0.0, 0.0});
        Vertex vertex2 = new Vertex(counter_1 + 2L, counter_1 + 1L, position2, 0.0);
        vertices.add(vertex2);
        RealVector position3 = new ArrayRealVector(new Double[] {3.0, 0.0, 0.0});
        Vertex vertex3 = new Vertex(counter_1 + 3L, counter_1 + 2L, position3, 0.0);
        vertices.add(vertex3);
        RealVector position4 = new ArrayRealVector(new Double[] {6.0, 0.0, 0.0});
        Vertex vertex4 = new Vertex(counter_1 + 4L, counter_1 + 3L, position4, 0.0);
        vertices.add(vertex4);
        RealVector position5 = new ArrayRealVector(new Double[] {10.0, 0.0, 0.0});
        Vertex vertex5 = new Vertex(counter_1 + 5L, counter_1 + 4L, position5, 0.0);
        vertices.add(vertex5);

        Edge edge1 = new Edge(1.0, 0.0);
        Edge edge2 = new Edge(2.0, 0.0);
        Edge edge3 = new Edge(3.0, 0.0);
        Edge edge4 = new Edge(4.0, 0.0);

        edges.put(vertex1, new LinkedHashMap<>());
        edges.put(vertex2, new LinkedHashMap<>());
        edges.put(vertex3, new LinkedHashMap<>());
        edges.put(vertex4, new LinkedHashMap<>());
        edges.put(vertex5, new LinkedHashMap<>());

        edges.get(vertex1).put(vertex2, edge1);
        edges.get(vertex2).put(vertex3, edge2);
        edges.get(vertex3).put(vertex4, edge3);
        edges.get(vertex4).put(vertex5, edge4);

        edges.get(vertex2).put(vertex1, edge1);
        edges.get(vertex3).put(vertex2, edge2);
        edges.get(vertex4).put(vertex3, edge3);
        edges.get(vertex5).put(vertex4, edge4);


        return new Graph(vertices, edges, false);
    }

    /**
     * returns the shortest path from vStart to vEnd in the first example graph.
     *
     * @param vStart the start vertex
     * @param vEnd the target vertex
     * @return the shortest path form vStart to vEnd in the first example graph
     */
    private List<Vertex> getShortestPath_1(Vertex vStart, Vertex vEnd) {
        List<Vertex> list = new LinkedList<>();
        if(getGraph_1().getVertex(vStart).getId() <= getGraph_1().getVertex(vEnd).getId()) {
            for (long i = vStart.getId(); i <= vEnd.getId(); ++i) {
                list.add(getGraph(1).getVertex(i));
            }
        } else {
            for (long i = vStart.getId(); i >= vEnd.getId(); --i) {
                list.add(getGraph(1).getVertex(i));
            }
        }
        return list;
    }

    /**
     * returns the shortest detailed path with maximal steering angle from vStart to vEnd in the first example graph.
     * A detailed path contains a vertex at least every two meters
     *
     * @param vStart the start vertex
     * @param vEnd the target vertex
     * @return the shortest detailed path with maximal steering angle form vStart to vEnd in the first example graph
     */
    private List<Vertex> getDetailedPathWithMaximalSteeringAngle_1(Vertex vStart, Vertex vEnd) {
        List<Vertex> list = new LinkedList<>();
        if(vStart.getId() <= vEnd.getId()) {
            RealVector position1 = new ArrayRealVector(new Double[] {0.0, 0.0, 0.0});
            Vertex vertex1 = new Vertex(counter_1 + 1L, counter_1 + 0L, position1, 0.0);
            list.add(vertex1);
            RealVector position2 = new ArrayRealVector(new Double[] {1.0, 0.0, 0.0});
            Vertex vertex2 = new Vertex(counter_1 + 2L, counter_1 + 1L, position2, 0.0);
            list.add(vertex2);
            RealVector position3 = new ArrayRealVector(new Double[] {3.0, 0.0, 0.0});
            Vertex vertex3 = new Vertex(counter_1 + 3L, counter_1 + 2L, position3, 0.0);
            list.add(vertex3);
            RealVector position_3_1 = new ArrayRealVector(new Double[] {5.0, 0.0, 0.0});
            Vertex vertex_3_1 = new Vertex(-1L, -1L, position_3_1, 0.0);
            list.add(vertex_3_1);
            RealVector position4 = new ArrayRealVector(new Double[] {6.0, 0.0, 0.0});
            Vertex vertex4 = new Vertex(counter_1 + 4L, counter_1 + 3L, position4, 0.0);
            list.add(vertex4);
            RealVector position_4_1 = new ArrayRealVector(new Double[] {8.0, 0.0, 0.0});
            Vertex vertex_4_1 = new Vertex(-1L, -1L, position_4_1, 0.0);
            list.add(vertex_4_1);
            RealVector position5 = new ArrayRealVector(new Double[] {10.0, 0.0, 0.0});
            Vertex vertex5 = new Vertex(counter_1 + 5L, counter_1 + 4L, position5, 0.0);
            list.add(vertex5);

            int start = vStart.getId().intValue() - counter_1;
            if(start > 4) {
                start++;
            }
            if(start > 3) {
                start++;
            }
            int end = vEnd.getId().intValue() - counter_1;
            if(end > 4) {
                end++;
            }
            if(end > 3) {
                end++;
            }

            return list.subList(start - 1, end);
        }

        RealVector position5 = new ArrayRealVector(new Double[] {10.0, 0.0, 0.0});
        Vertex vertex5 = new Vertex(counter_1 + 5L, counter_1 + 4L, position5, 0.0);
        list.add(vertex5);
        RealVector position_4_1 = new ArrayRealVector(new Double[] {8.0, 0.0, 0.0});
        Vertex vertex_4_1 = new Vertex(-1L, -1L, position_4_1, 0.0);
        list.add(vertex_4_1);
        RealVector position4 = new ArrayRealVector(new Double[] {6.0, 0.0, 0.0});
        Vertex vertex4 = new Vertex(counter_1 + 4L, counter_1 + 3L, position4, 0.0);
        list.add(vertex4);
        RealVector position_3_1 = new ArrayRealVector(new Double[] {4.0, 0.0, 0.0});
        Vertex vertex_3_1 = new Vertex(-1L, -1L, position_3_1, 0.0);
        list.add(vertex_3_1);
        RealVector position3 = new ArrayRealVector(new Double[] {3.0, 0.0, 0.0});
        Vertex vertex3 = new Vertex(counter_1 + 3L, counter_1 + 2L, position3, 0.0);
        list.add(vertex3);
        RealVector position2 = new ArrayRealVector(new Double[] {1.0, 0.0, 0.0});
        Vertex vertex2 = new Vertex(counter_1 + 2L, counter_1 + 1L, position2, 0.0);
        list.add(vertex2);
        RealVector position1 = new ArrayRealVector(new Double[] {0.0, 0.0, 0.0});
        Vertex vertex1 = new Vertex(counter_1 + 1L, counter_1 + 0L, position1, 0.0);
        list.add(vertex1);

        int start = vStart.getId().intValue() - counter_1;
        if(start > 4) {
            start++;
        }
        if(start > 3) {
            start++;
        }
        int end = vEnd.getId().intValue() - counter_1;
        if(end > 4) {
            end++;
        }
        if(end > 3) {
            end++;
        }

        return list.subList(list.size() - start, list.size() - end + 1);
    }

    /**
     * Getter for a example IAdjacency list for testing issues.
     * This is a "line" graph
     *
     * @return a "line" graph
     */
    private List<IAdjacency> getIAdjacencyList_1() {
        List<IAdjacency> list = new LinkedList<>();

        //store the ControllerNode counter to be able to generate a correct graph
        counter_1 = ControllerNode.getCounter();

        Point3D point1 = new Point3D(0.0, 0.0, 0.0);
        Point3D point2 = new Point3D(1.0, 0.0, 0.0);
        Point3D point3 = new Point3D(3.0, 0.0, 0.0);
        Point3D point4 = new Point3D(6.0, 0.0, 0.0);
        Point3D point5 = new Point3D(10.0, 0.0, 0.0);

        IControllerNode node1 = new ControllerNode(point1, counter_1 + 0L);
        if(node1.getId() != counter_1) {
            counter_1 = (int) node1.getId() - 1;
            node1 = new ControllerNode(point1, counter_1);
        }
        IControllerNode node2 = new ControllerNode(point2, counter_1 + 1L);
        IControllerNode node3 = new ControllerNode(point3, counter_1 + 2L);
        IControllerNode node4 = new ControllerNode(point4, counter_1 + 3L);
        IControllerNode node5 = new ControllerNode(point5, counter_1 + 4L);

        IAdjacency adjacency1 = new Adjacency(node1, node2);
        list.add(adjacency1);
        IAdjacency adjacency2 = new Adjacency(node2, node3);
        list.add(adjacency2);
        IAdjacency adjacency3 = new Adjacency(node3, node4);
        list.add(adjacency3);
        IAdjacency adjacency4 = new Adjacency(node4, node5);
        list.add(adjacency4);

        return list;
    }

    /**
     * Getter for the second example Graph for testing issues.
     * This is a "curved line" graph.
     *
     * @return a "curved line" graph
     */
    private Graph getGraph_2() {
        List<Vertex> vertices = new LinkedList<>();
        Map<Vertex, Map<Vertex, Edge>> edges = new LinkedHashMap<>();

        PathGenerator pathGenerator = new PathGenerator();

        vertices = pathGenerator.generateCircle(radius, 0.0, 0.0, counter_2);

        for(int i = 0; i < vertices.size() - 1; ++i) {
            Edge edge = new Edge(vertices.get(i).getPosition().getDistance(vertices.get(i + 1).getPosition()), 0.0);
            if(i == 0) {
                edges.put(vertices.get(i), new LinkedHashMap<>());
            }
            edges.get(vertices.get(i)).put(vertices.get(i + 1), edge);
            edges.put(vertices.get(i + 1), new LinkedHashMap<>());
            edges.get(vertices.get(i + 1)).put(vertices.get(i), edge);
        }

        return new Graph(vertices, edges, false);
    }

    /**
     * returns the shortest path from vStart to vEnd in the second example graph
     *
     * @param vStart the start vertex
     * @param vEnd the target vertex
     * @return the shortest path form vStart to vEnd in the first example graph
     */
    private List<Vertex> getShortestPath_2(Vertex vStart, Vertex vEnd) {
        List<Vertex> list = new LinkedList<>();
        if(getGraph_2().getVertex(vStart).getId() <= getGraph_2().getVertex(vEnd).getId()) {
            for (long i = vStart.getId(); i <= vEnd.getId(); ++i) {
                list.add(getGraph_2().getVertex(i));
            }
        } else {
            for (long i = vStart.getId(); i >= vEnd.getId(); --i) {
                list.add(getGraph_2().getVertex(i));
            }
        }
        return list;
    }

    /**
     * returns the shortest detailed path with maximal steering angle from vStart to vEnd in the second example graph
     *
     * @param vStart the start vertex
     * @param vEnd the target vertex
     * @return the shortest detailed path with maximal steering angle form vStart to vEnd in the second example graph
     */
    private List<Vertex> getDetailedPathWithMaximalSteeringAngle_2(Vertex vStart, Vertex vEnd) {
        LinkedList<Vertex> list = new LinkedList<>();
        if(vStart.getId() <= vEnd.getId()) {

            for(int i = 0; i < getGraph(2).getNumberOfVertices(); ++i) {
                list.add(getGraph(2).getVertices().get(i));
                list.get(i).setMaximumSteeringAngle(calculateAngle(radius, constantWheelbase));
            }

            int start = vStart.getId().intValue() - counter_2;
            int end = vEnd.getId().intValue() - counter_2;

            return list.subList(start - 1, end);
        }

        for(int i = 0; i < getGraph(2).getNumberOfVertices(); ++i) {
            list.addFirst(getGraph(2).getVertices().get(i));
            list.get(0).setMaximumSteeringAngle(calculateAngle(radius, constantWheelbase));
        }

        int start = vStart.getId().intValue() - counter_2;
        int end = vEnd.getId().intValue() - counter_2;

        return list.subList(list.size() - start, list.size() - end + 1);
    }

    /**
     * Getter for a example IAdjacency list for testing issues.
     * This is a "curved line" graph
     *
     * @return a "curved line" graph
     */
    private List<IAdjacency> getIAdjacencyList_2() {
        List<IAdjacency> list = new LinkedList<>();

        counter_2 = ControllerNode.getCounter();

        PathGenerator pathGenerator = new PathGenerator();

        Point3D point1 = new Point3D(radius, 0.0, 0.0);
        IControllerNode node1 = new ControllerNode(point1, counter_2);
        if(node1.getId() != counter_2) {
            counter_2 = (int) node1.getId() - 1;
            node1 = new ControllerNode(point1, counter_2);
        }
        List<Vertex> vertices = pathGenerator.generateCircle(radius, 0.0, 0.0, counter_2);

        IControllerNode cur = convertVertexToNode(vertices.get(0));
        for(int i = 0; i < vertices.size() - 1; ++i) {
            Edge edge = new Edge(1.0, 0.0);
            IControllerNode next = convertVertexToNode(vertices.get(i + 1));
            IAdjacency adjacency = new Adjacency(cur, next);
            cur = next;
            list.add(adjacency);
        }

        return list;
    }

    /**
     * calculates the steering angle for a car dependent on the wheelcase of the car and the radius of the curve to drive.
     *
     * @param radius the radius of the curve
     * @param constantWheelbase the wheelbase of the car
     * @return the steering angle
     */
    private Double calculateAngle(Double radius, Double constantWheelbase) {
        if (radius == 0.0) {
            return 0.0;
        }
        return (Double) (Math.asin(constantWheelbase / radius));
    }

    /**
     * converts a Vertex to a Node
     *
     * @param vertex the vertex to convert
     * @return the node
     */
    private IControllerNode convertVertexToNode(Vertex vertex) {
        Point3D p = new Point3D(
                vertex.getPosition().getEntry(0),
                vertex.getPosition().getEntry(1),
                vertex.getPosition().getEntry(2)
        );
        IControllerNode node = new ControllerNode(p, vertex.getOsmId());
        return node;
    }
}
