/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures;

/**
 * This class provides all information for an edge data structure in a general graph.
 *
 * Created by Christoph Grüne on 22.02.2017.
 */
public class Edge {
    private Double weight;
    private Double capacity;

    /**
     * constructor for an edge object
     *
     * @param weight the weight of the edge
     * @param capacity the capacity of the edge
     */
    public Edge(Double weight, Double capacity) {
        this.weight = weight;
        this.capacity = capacity;
    }

    public Edge copy() {
        return new Edge(this.weight.doubleValue(), this.capacity.doubleValue());
    }

    /**
     * Getter for weight
     *
     * @return the weight of this edge
     */
    public Double getWeight() {
        return new Double(weight.doubleValue());
    }

    /**
     * Getter for capacity
     *
     * @return the capacity of this edge
     */
    public Double getCapacity() {
        return new Double(capacity.doubleValue());
    }

    /**
     * Test for the equality of two edges. If all attributes are the same the edges are considered to be equal.
     *
     * @param obj the object to test
     * @return if the objects are equal
     */
    @Override
    public boolean equals(Object obj) {
        if(obj.getClass() != this.getClass()) {
            return false;
        }

        Edge otherEdge = (Edge) obj;
        if (!otherEdge.getWeight().equals(this.getWeight())) {
            return false;
        } else if(!otherEdge.getCapacity().equals(this.getCapacity())) {
            return false;
        }
        return true;
    }
}
