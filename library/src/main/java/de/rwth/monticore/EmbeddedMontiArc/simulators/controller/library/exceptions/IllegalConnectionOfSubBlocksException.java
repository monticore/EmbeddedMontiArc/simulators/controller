/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.exceptions;

/**
 * This exception is used in findExecutionOrder() in FunctionBlockManagement.
 * It is thrown, if no execution order can be found because of not assignable ports.
 *
 * Created by Christoph Grüne on 19.03.2017.
 */
public class IllegalConnectionOfSubBlocksException extends RuntimeException {
    public IllegalConnectionOfSubBlocksException() {
        super();
    }
    public IllegalConnectionOfSubBlocksException(String s) {
        super(s);
    }
    public IllegalConnectionOfSubBlocksException(String s, Throwable throwable) {
        super(s, throwable);
    }
    public IllegalConnectionOfSubBlocksException(Throwable throwable) {
        super(throwable);
    }
}
