/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IControllerNode;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedList;
import java.util.List;

/**
 * This class generates several types of paths. A path is a List<Vertex>
 *
 * Created by Christoph Grüne on 25.01.2017.
 */
public class PathGenerator {

    /**
     * this variable contains the resolution of the path. The unit is vertex/meter
     */
    public static double DISTANCE_OF_POINTS = IControllerNode.INTERPOLATION_DISTANCE;

    /**
     * constructs a PathGenerator object
     */
    public PathGenerator(){

    }

    /**
     * Setter for DISTANCE_OF_POINTS
     *
     * @param distanceOfPoints thr new value for DISTANCE_OF_POINTS
     */
    public void setDISTANCE_OF_POINTS(double distanceOfPoints) {
        DISTANCE_OF_POINTS = distanceOfPoints;
    }

    /**
     * generates a path in the form of a circle in the x-y-plain
     *
     * @param radius the radius of the circle
     * @param xMiddle the x coordinate of the middle point
     * @param yMiddle the y coordinate of the miidle point
     * @param startId the start ID of the vertices of the path
     * @return the path
     */
    public List<Vertex> generateCircle(double radius, double xMiddle, double yMiddle, int startId) {
        List<Vertex> path = new LinkedList<>();

        long i = startId;
        double phi = 0;
        double x, y;

        while(phi <= 2 * Math.PI) {
            x = xMiddle + radius * Math.cos(phi);
            y = yMiddle + radius * Math.sin(phi);

            RealVector pos = new ArrayRealVector(new double[] {x, y, 0});

            path.add(new Vertex(i + 1, i, pos, 0.0));
            i++;
            phi += (DISTANCE_OF_POINTS) / radius;
        }

        return path;
    }

    /**
     * generates a path in the form of a line
     *
     * @param length the length of the line
     * @param xStart the x-coordinate of the start position
     * @param yStart the y-coordinate of the start position
     * @param direction the direction of the line
     * @param startId the start ID of the vertices of the path
     * @return the path
     */
    public List<Vertex> generateStraightLine(double length, double xStart, double yStart, RealVector direction, int startId) {
        List<Vertex> path = new LinkedList<>();

        long i = startId;
        double t = 0;
        double x, y;

        if(direction.getNorm() == 0) {
            direction = new ArrayRealVector(new ArrayRealVector(new Double[] {1.0, 0.0, 0.0}));
        }
        direction.unitize();

        while(t <= length) {
            x = xStart + t * direction.getEntry(0);
            y = yStart + t * direction.getEntry(1);

            RealVector pos = new ArrayRealVector(new double[] {x, y, 0});

            i++;
            path.add(new Vertex(i + 1, i, pos, 0.0));
            t += DISTANCE_OF_POINTS;
        }

        return path;
    }
}
