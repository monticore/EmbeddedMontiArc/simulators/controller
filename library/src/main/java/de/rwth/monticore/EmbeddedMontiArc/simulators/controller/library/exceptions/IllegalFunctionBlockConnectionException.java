/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.exceptions;

/**
 * This exception is used in the function connect() in FunctionBlockManagement.
 * It is thrown if non-added function blocks are connected.
 *
 * Created by Christoph Grüne on 19.03.2017.
 */
public class IllegalFunctionBlockConnectionException extends RuntimeException {
    public IllegalFunctionBlockConnectionException() {
        super();
    }
    public IllegalFunctionBlockConnectionException(String s) {
        super(s);
    }
    public IllegalFunctionBlockConnectionException(String s, Throwable throwable) {
        super(s, throwable);
    }
    public IllegalFunctionBlockConnectionException(Throwable throwable) {
        super(throwable);
    }
}
