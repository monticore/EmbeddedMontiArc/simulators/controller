/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock;

/**
 * this enum administrates all ports of all function blocks which are used in this project.
 * Every entry stands for one port of a function block.
 * The signature of the entries is as follows:
 *      The name of the block is in uppercase letters and separated by underscores.
 *      The name of the variable the port assigned to is in lowercase letters and separated per underscore as well.
 *      Furthermore:
 *      non-indented entries are inputs
 *          tab-indented entries are outputs
 *              double tab-indented entries are global variables
 *
 *
 * Created by Christoph on 17.01.2017.
 */
public enum ConnectionEntry {
    //***************************************************************************
    //*  Library Communication Entries                         *
    //***************************************************************************
    //GetNextElement
    GET_NEXT_ELEMENT_iterator("GET_NEXT_ELEMENT_iterator"),
        GET_NEXT_ELEMENT_next_element("GET_NEXT_ELEMENT_next_element"),
        GET_NEXT_ELEMENT_list_end("GET_NEXT_ELEMENT_list_end"),
    //MoveIterator
    MOVE_ITERATOR_iterator_old("MOVE_ITERATOR_iterator_old"),
    MOVE_ITERATOR_object("MOVE_ITERATOR_object"),
        MOVE_ITERATOR_iterator_new("MOVE_ITERATOR_iterator_new"),
    //MinimumOfDoubleList
    MINIMUM_OF_DOUBLE_LIST_list("MINIMUM_OF_DOUBLE_LIST_list"),
        MINIMUM_OF_DOUBLE_LIST_minimum("MINIMUM_OF_DOUBLE_LIST_minimum"),
    //CreateIterator
    CREATE_ITERATOR_list("CREATE_ITERATOR_list"),
    CREATE_ITERATOR_element_to_begin("CREATE_ITERATOR_element_to_begin"),
        CREATE_ITERATOR_iterator("CREATE_ITERATOR_iterator"),
    //MinimumDouble
    MINIMUM_DOUBLE_double_1("MINIMUM_DOUBLE_double_1"),
    MINIMUM_DOUBLE_double_2("MINIMUM_DOUBLE_double_2"),
        MINIMUM_DOUBLE_minimum("MINIMUM_DOUBLE_minimum"),
    //MinimumDouble
    ABSOLUTE_MAXIMUM_DOUBLE_double_1("ABSOLUTE_MAXIMUM_DOUBLE_double_1"),
    ABSOLUTE_MAXIMUM_DOUBLE_double_2("ABSOLUTE_MAXIMUM_DOUBLE_double_2"),
        ABSOLUTE_MAXIMUM_DOUBLE_maximum("ABSOLUTE_MAXIMUM_DOUBLE_maximum"),
    //***************************************************************************
    //*  Inner Communication Entries (mainControlBlock)                         *
    //***************************************************************************
    MAIN_CONTROL_BLOCK_actuator_brake("MAIN_CONTROL_BLOCK_actuator_brake"),
    MAIN_CONTROL_BLOCK_actuator_engine("MAIN_CONTROL_BLOCK_actuator_engine"),
    MAIN_CONTROL_BLOCK_actuator_gear("MAIN_CONTROL_BLOCK_actuator_gear"),
    MAIN_CONTROL_BLOCK_actuator_steering("MAIN_CONTROL_BLOCK_actuator_steering"),
    //***************************************************************************
    //*  Inner Communication Entries (InnerControlBlock)                        *
    //***************************************************************************
    //InnerControlBlock
    INNER_CONTROL_BLOCK_sensor_distance_to_left("INNER_CONTROL_BLOCK_sensor_distance_to_left"),
    INNER_CONTROL_BLOCK_sensor_distance_to_right("INNER_CONTROL_BLOCK_sensor_distance_to_right"),
    INNER_CONTROL_BLOCK_sensor_steering("INNER_CONTROL_BLOCK_sensor_steering"),
    INNER_CONTROL_BLOCK_sensor_current_surface("INNER_CONTROL_BLOCK_sensor_current_surface"),
    INNER_CONTROL_BLOCK_sensor_weather("INNER_CONTROL_BLOCK_sensor_weather"),
    INNER_CONTROL_BLOCK_sensor_velocity("INNER_CONTROL_BLOCK_sensor_velocity"),
    INNER_CONTROL_BLOCK_sensor_orientation("INNER_CONTROL_BLOCK_sensor_orientation"),
    INNER_CONTROL_BLOCK_constant_maximum_velocity("INNER_CONTROL_BLOCK_constant_maximum_velocity"),
    INNER_CONTROL_BLOCK_constant_number_of_gears("INNER_CONTROL_BLOCK_constant_number_of_gears"),
    //INNER_CONTROL_BLOCK_constant_delta_time("INNER_CONTROL_BLOCK_constant_delta_time"),
    INNER_CONTROL_BLOCK_constant_min_steering_angle("INNER_CONTROL_BLOCK_constant_min_steering_angle"),
    INNER_CONTROL_BLOCK_constant_max_steering_angle("INNER_CONTROL_BLOCK_constant_max_steering_angle"),
    INNER_CONTROL_BLOCK_constant_motor_max_acceleration("INNER_CONTROL_BLOCK_constant_motor_max_acceleration"),
    INNER_CONTROL_BLOCK_constant_brake_max_acceleration("INNER_CONTROL_BLOCK_constant_brake_max_acceleration"),
    INNER_CONTROL_BLOCK_constant_trajectory_error("INNER_CONTROL_BLOCK_constant_trajectory_error"),
    INNER_CONTROL_BLOCK_vehicle_max_temporary_allowed_velocity("INNER_CONTROL_BLOCK_vehicle_max_temporary_allowed_velocity"),
    INNER_CONTROL_BLOCK_sensor_gps_coordinates("INNER_CONTROL_BLOCK_sensor_gps_coordinates"),
    INNER_CONTROL_BLOCK_path("INNER_CONTROL_BLOCK_path"),
    INNER_CONTROL_BLOCK_angle_to_vanishing_point("INNER_CONTROL_BLOCK_angle_to_vanishing_point"),
    INNER_CONTROL_BLOCK_hazard_velocity("INNER_CONTROL_BLOCK_hazard_velocity"),
    INNER_CONTROL_BLOCK_sign_velocity("INNER_CONTROL_BLOCK_sign_velocity"),
    INNER_CONTROL_BLOCK_trajectory_velocity("INNER_CONTROL_BLOCK_trajectory_velocity"),
        INNER_CONTROL_BLOCK_actuator_steering("INNER_CONTROL_BLOCK_actuator_steering"),
        INNER_CONTROL_BLOCK_actuator_brake("INNER_CONTROL_BLOCK_actuator_brake"),
        INNER_CONTROL_BLOCK_actuator_engine("INNER_CONTROL_BLOCK_actuator_engine"),
        INNER_CONTROL_BLOCK_actuator_gear("INNER_CONTROL_BLOCK_actuator_gear"),
    //SteeringLogic
    STEERING_LOGIC_current_velocity("STEERING_LOGIC_current_velocity"),
    STEERING_LOGIC_distance_to_left("STEERING_LOGIC_distance_to_left"),
    STEERING_LOGIC_distance_to_right("STEERING_LOGIC_distance_to_right"),
    STEERING_LOGIC_vanishing_angle("STEERING_LOGIC_vanishing_angle"),
    STEERING_LOGIC_path("STEERING_LOGIC_path"),
    STEERING_LOGIC_gps_coordinates("STEERING_LOGIC_gps_coordinates"),
    STEERING_LOGIC_global_orientation("STEERING_LOGIC_global_orientation"),
        STEERING_LOGIC_steering_angle("STEERING_LOGIC_steering_angle"),
    //SteeringLogicHelper
    STEERING_LOGIC_HELPER_current_velocity("STEERING_LOGIC_HELPER_current_velocity"),
    STEERING_LOGIC_HELPER_distance_to_left("STEERING_LOGIC_HELPER_distance_to_left"),
    STEERING_LOGIC_HELPER_distance_to_right("STEERING_LOGIC_HELPER_distance_to_right"),
    STEERING_LOGIC_HELPER_vanishing_angle("STEERING_LOGIC_HELPER_vanishing_angle"),
    STEERING_LOGIC_HELPER_path("STEERING_LOGIC_HELPER_path"),
    STEERING_LOGIC_HELPER_gps_coordinates("STEERING_LOGIC_HELPER_gps_coordinates"),
    STEERING_LOGIC_HELPER_global_orientation("STEERING_LOGIC_HELPER_global_orientation"),
        STEERING_LOGIC_HELPER_steering_angle("STEERING_LOGIC_HELPER_steering_angle"),
    //SteeringDistributor
    STEERING_DISTRIBUTOR_angle_pid("STEERING_DISTRIBUTOR_angle_pid"),
    STEERING_DISTRIBUTOR_sensor_steering("STEERING_DISTRIBUTOR_sensor_steering"),
    STEERING_DISTRIBUTOR_min_steering_angle("STEERING_DISTRIBUTOR_min_steering_angle"),
    STEERING_DISTRIBUTOR_max_steering_angle("STEERING_DISTRIBUTOR_max_steering_angle"),
        STEERING_DISTRIBUTOR_actuator_steering_value("STEERING_DISTRIBUTOR_actuator_steering_value"),
    //VelocityLogic
    VELOCITY_LOGIC_current_weather("VELOCITY_LOGIC_current_weather"),
    VELOCITY_LOGIC_current_surface("VELOCITY_LOGIC_current_surface"),
    VELOCITY_LOGIC_max_velocity("VELOCITY_LOGIC_max_velocity"),
    VELOCITY_LOGIC_new_steering_angle("VELOCITY_LOGIC_new_steering_angle"),
    VELOCITY_LOGIC_current_steering_angle("VELOCITY_LOGIC_current_steering_angle"),
    VELOCITY_LOGIC_hazard_velocity("VELOCITY_LOGIC_hazard_velocity"),
    VELOCITY_LOGIC_sign_velocity("VELOCITY_LOGIC_sign_velocity"),
    VELOCITY_LOGIC_trajectory_velocity("VELOCITY_LOGIC_trajectory_velocity"),
    VELOCITY_LOGIC_vehicle_max_temporary_allowed_velocity("VELOCITY_LOGIC_vehicle_max_temporary_allowed_velocity"),
        VELOCITY_LOGIC_desired_velocity("VELOCITY_LOGIC_desired_velocity"),
    //VelocityDistributor
    VELOCITY_DISTRIBUTOR_velocity_pid("VELOCITY_DISTRIBUTOR_velocity_pid"),
    VELOCITY_DISTRIBUTOR_current_velocity("VELOCITY_DISTRIBUTOR_current_velocity"),
    VELOCITY_DISTRIBUTOR_maximum_velocity("VELOCITY_DISTRIBUTOR_maximum_velocity"),
    VELOCITY_DISTRIBUTOR_number_of_gears("VELOCITY_DISTRIBUTOR_number_of_gears"),
    //VELOCITY_DISTRIBUTOR_delta_time("VELOCITY_DISTRIBUTOR_delta_time"),
    VELOCITY_DISTRIBUTOR_motor_max_acceleration("VELOCITY_DISTRIBUTOR_motor_max_acceleration"),
    VELOCITY_DISTRIBUTOR_brake_max_acceleration("VELOCITY_DISTRIBUTOR_brake_max_acceleration"),
        VELOCITY_DISTRIBUTOR_actuator_gear_value("VELOCITY_DISTRIBUTOR_actuator_gear_value"),
        VELOCITY_DISTRIBUTOR_actuator_engine_value("VELOCITY_DISTRIBUTOR_actuator_engine_value"),
        VELOCITY_DISTRIBUTOR_actuator_brake_value("VELOCITY_DISTRIBUTOR_actuator_brake_value"),
    //***************************************************************************
    //*  Inner Communication Entries (functionblocks.PID)                                      *
    //***************************************************************************
    PID_desired_value("PID_desired_value"),
    PID_current_value("PID_current_value"),
    //PID_delta_time("PID_delta_time"),
        PID_new_controlled_value("PID_new_controlled_value"),
    //***************************************************************************
    //*  Inner Communication Entries (trajectoryBlock)                          *
    //***************************************************************************
    //TrajectoryBlock
    TRAJECTORY_BLOCK_path("TRAJECTORY_BLOCK_path"),
    TRAJECTORY_BLOCK_current_velocity("TRAJECTORY_BLOCK_current_velocity"),
    TRAJECTORY_BLOCK_current_surface("TRAJECTORY_BLOCK_current_surface"),
    TRAJECTORY_BLOCK_current_weather("TRAJECTORY_BLOCK_current_weather"),
    TRAJECTORY_BLOCK_gps_coordinates("TRAJECTORY_BLOCK_gps_coordinates"),
    TRAJECTORY_BLOCK_max_velocity("TRAJECTORY_BLOCK_max_velocity"),
    TRAJECTORY_BLOCK_brakes_max_acceleration("TRAJECTORY_BLOCK_brakes_max_acceleration"),
        TRAJECTORY_BLOCK_velocity("TRAJECTORY_BLOCK_velocity"),
    //DistanceEstimation
    DISTANCE_ESTIMATION_current_velocity("DISTANCE_ESTIMATION_current_velocity"),
    DISTANCE_ESTIMATION_brakes_max_acceleration("DISTANCE_ESTIMATION_brakes_max_acceleration"),
        DISTANCE_ESTIMATION_estimated_distance("DISTANCE_ESTIMATION_estimated_distance"),
    //LocateNearestVertexInPath
    LOCATE_NEAREST_VERTEX_IN_PATH_path("LOCATE_NEAREST_VERTEX_IN_PATH_path"),
    LOCATE_NEAREST_VERTEX_IN_PATH_gps_coordinates("LOCATE_NEAREST_VERTEX_IN_PATH_gps_coordinates"),
        LOCATE_NEAREST_VERTEX_IN_PATH_nearest_vertex("LOCATE_NEAREST_VERTEX_IN_PATH_nearest_vertex"),
    //MaximumVelocityBySteeringAnAngle
    MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_steering_angle("MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_steering_angle"),
    MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_surface("MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_surface"),
    MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_weather("MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_weather"),
        MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_velocity("MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_velocity"),
    //VelocityEstimation
    VELOCITY_ESTIMATION_path_iterator("VELOCITY_ESTIMATION_path_iterator"),
    VELOCITY_ESTIMATION_start_vertex("VELOCITY_ESTIMATION_start_vertex"),
    VELOCITY_ESTIMATION_distance("VELOCITY_ESTIMATION_distance"),
    VELOCITY_ESTIMATION_current_surface("VELOCITY_ESTIMATION_current_surface"),
    VELOCITY_ESTIMATION_current_weather("VELOCITY_ESTIMATION_current_weather"),
        VELOCITY_ESTIMATION_velocity("VELOCITY_ESTIMATION_velocity"),
            VELOCITY_ESTIMATION_path_end("VELOCITY_ESTIMATION_path_end"),
            VELOCITY_ESTIMATION_next_vertex("VELOCITY_ESTIMATION_next_vertex"),
    //ExtractMaxSteeringAngleFromVertex
    EXTRACT_MAX_STEERING_ANGLE_FROM_VERTEX_vertex("EXTRACT_MAX_STEERING_ANGLE_FROM_VERTEX_vertex"),
        EXTRACT_MAX_STEERING_ANGLE_FROM_VERTEX_steering_angle("EXTRACT_MAX_STEERING_ANGLE_FROM_VERTEX_steering_angle"),
    //***************************************************************************
    //*  Inner Communication Entries (hazardBlock)                              *
    //***************************************************************************
    //HazardBlock
    HAZARD_BLOCK_current_steering("HAZARD_BLOCK_current_steering"),
    HAZARD_BLOCK_current_surface("HAZARD_BLOCK_current_surface"),
    HAZARD_BLOCK_current_velocity("HAZARD_BLOCK_current_velocity"),
    HAZARD_BLOCK_brakes_max_acceleration("HAZARD_BLOCK_brakes_max_acceleration"),
    HAZARD_BLOCK_cars("HAZARD_BLOCK_cars"),
    HAZARD_BLOCK_pedestrians("HAZARD_BLOCK_pedestrians"),
    HAZARD_BLOCK_distance_to_left("HAZARD_BLOCK_distance_to_left"),
    HAZARD_BLOCK_distance_to_right("HAZARD_BLOCK_distance_to_right"),
    HAZARD_BLOCK_maximum_velocity("HAZARD_BLOCK_maximum_velocity"),
        HAZARD_BLOCK_hazard_velocity("HAZARD_BLOCK_hazard_velocity"),
    //***************************************************************************
    //*  Inner Communication Entries (hazardBlock)                              *
    //***************************************************************************
    //TrafficSignBlock
    TRAFFIC_SIGN_BLOCK_signs("TRAFFIC_SIGN_BLOCK_signs"),
    TRAFFIC_SIGN_BLOCK_max_velocity("TRAFFIC_SIGN_BLOCK_max_velocity"),
        TRAFFIC_SIGN_BLOCK_velocity("TRAFFIC_SIGN_BLOCK_velocity"),
    //***************************************************************************
    //*  Navigation Communication Entries (navigationBlock)                     *
    //***************************************************************************
    //NavigationBlock
        NAVIGATION_BLOCK_path("NAVIGATION_BLOCK_path"),
    //ExtractDetailedPath
    EXTRACT_DETAILED_PATH_non_detailed_path("EXTRACT_DETAILED_PATH_non_detailed_path"),
        EXTRACT_DETAILED_PATH_detailed_path("EXTRACT_DETAILED_PATH_detailed_path"),
    //extractGraph
    EXTRACT_GRAPH_adjacency_list("EXTRACT_GRAPH_adjacency_list"),
        EXTRACT_GRAPH_graph("EXTRACT_GRAPH_graph"),
    //extractTargetVertex
    EXTRACT_TARGET_VERTEX_target_node("EXTRACT_TARGET_VERTEX_target_node"),
    EXTRACT_TARGET_VERTEX_graph("EXTRACT_TARGET_VERTEX_graph"),
        EXTRACT_TARGET_VERTEX_target_vertex("EXTRACT_TARGET_VERTEX_target_vertex"),
    //FindPath
    FIND_PATH_graph("FIND_PATH_graph"),
    FIND_PATH_start_vertex("FIND_PATH_start_vertex"),
    FIND_PATH_target_vertex("FIND_PATH_target_vertex"),
        FIND_PATH_path("FIND_PATH_path"),
    //LocateNearestVertex
    LOCATE_NEAREST_VERTEX_IN_GRAPH_graph("LOCATE_NEAREST_VERTEX_IN_GRAPH_graph"),
    LOCATE_NEAREST_VERTEX_IN_GRAPH_gps_coordinates("LOCATE_NEAREST_VERTEX_IN_GRAPH_gps_coordinates"),
        LOCATE_NEAREST_VERTEX_IN_GRAPH_nearest_vertex("LOCATE_NEAREST_VERTEX_IN_GRAPH_nearest_vertex"),
    //***************************************************************************
    //*  Navigation Communication Entries (trajectoryPlanningBlock)             *
    //***************************************************************************
    //TrajectoryPlanningBlock
    TRAJECTORY_PLANNING_BLOCK_path("TRAJECTORY_PLANNING_BLOCK_path"),
    TRAJECTORY_PLANNING_BLOCK_wheelbase("TRAJECTORY_PLANNING_BLOCK_wheelbase"),
    TRAJECTORY_PLANNING_BLOCK_detailed_path("TRAJECTORY_PLANNING_BLOCK_detailed_path"),
    //MaximumSteeringAngleTotal
    MAXIMUM_STEERING_ANGLE_TOTAL_path_iterator("MAXIMUM_STEERING_ANGLE_TOTAL_path_iterator"),
    MAXIMUM_STEERING_ANGLE_TOTAL_path("MAXIMUM_STEERING_ANGLE_TOTAL_path"),
    MAXIMUM_STEERING_ANGLE_TOTAL_wheelbase("MAXIMUM_STEERING_ANGLE_TOTAL_wheelbase"),
        MAXIMUM_STEERING_ANGLE_TOTAL_detailed_path("MAXIMUM_STEERING_ANGLE_TOTAL_detailed_path"),
            MAXIMUM_STEERING_ANGLE_TOTAL_path_end("MAXIMUM_STEERING_ANGLE_TOTAL_path_end"),
    //MaximumSteeringAngleOfSection
    MAXIMUM_STEERING_ANGLE_OF_SECTION_wheelbase("MAXIMUM_STEERING_ANGLE_OF_SECTION_wheelbase"),
    MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning("MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning"),
    MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_in_front("MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_in_front"),
    MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_behind("MAXIMUM_STEERING_ANGLE_OF_SECTION_beginning_behind"),
    MAXIMUM_STEERING_ANGLE_OF_SECTION_end("MAXIMUM_STEERING_ANGLE_OF_SECTION_end"),
    MAXIMUM_STEERING_ANGLE_OF_SECTION_end_in_front("MAXIMUM_STEERING_ANGLE_OF_SECTION_end_in_front"),
    MAXIMUM_STEERING_ANGLE_OF_SECTION_end_behind("MAXIMUM_STEERING_ANGLE_OF_SECTION_end_behind"),
        MAXIMUM_STEERING_ANGLE_OF_SECTION_steering_angle("MAXIMUM_STEERING_ANGLE_OF_SECTION_steering_angle"),
            MAXIMUM_STEERING_ANGLE_OF_SECTION_no_intersection_point_of_lines("MAXIMUM_STEERING_ANGLE_OF_SECTION_no_intersection_point_of_lines"),
    //PartitionPath
    PARTITION_PATH_path_iterator("PARTITION_PATH_path_iterator"),
    PARTITION_PATH_last_end("PARTITION_PATH_last_end"),
    PARTITION_PATH_last_end_in_front("PARTITION_PATH_last_end_in_front"),
    PARTITION_PATH_last_end_behind("PARTITION_PATH_last_end_behind"),
        PARTITION_PATH_beginning("PARTITION_PATH_beginning"),
        PARTITION_PATH_beginning_in_front("PARTITION_PATH_beginning_in_front"),
        PARTITION_PATH_beginning_behind("PARTITION_PATH_beginning_behind"),
        PARTITION_PATH_end("PARTITION_PATH_end"),
        PARTITION_PATH_end_in_front("PARTITION_PATH_end_in_front"),
        PARTITION_PATH_end_behind("PARTITION_PATH_end_behind"),
        PARTITION_PATH_path_end("PARTITION_PATH_path_end"),
    //SetMaximumSteeringAngleInVertex
    SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_path("SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_path"),
    SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_beginning("SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_beginning"),
    SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_end("SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_end"),
    SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_steering_angle("SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_steering_angle"),
        SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_path_with_angles("SET_MAXIMUM_STEERING_ANGLE_IN_VERTEX_path_with_angles"),
    //IntersectionPointOfLines
    INTERSECTION_POINT_OF_LINES_position_vector_1("INTERSECTION_POINT_OF_LINES_position_vector_1"),
    INTERSECTION_POINT_OF_LINES_direction_vector_1("INTERSECTION_POINT_OF_LINES_direction_vector_1"),
    INTERSECTION_POINT_OF_LINES_position_vector_2("INTERSECTION_POINT_OF_LINES_position_vector_2"),
    INTERSECTION_POINT_OF_LINES_direction_vector_2("INTERSECTION_POINT_OF_LINES_direction_vector_2"),
        INTERSECTION_POINT_OF_LINES_intersection_point("INTERSECTION_POINT_OF_LINES_intersection_point"),
        INTERSECTION_POINT_OF_LINES_no_intersection_point("INTERSECTION_POINT_OF_LINES_no_intersection_point"),
    //NormalOfDirectionVector2D
    NORMAL_OF_DIRECTION_VECTOR_2D_direction_vector("NORMAL_OF_DIRECTION_VECTOR_2D_direction_vector"),
        NORMAL_OF_DIRECTION_VECTOR_2D_normal_vector("NORMAL_OF_DIRECTION_VECTOR_2D_normal_vector"),
    //MaximumSteeringAngleForRadius
    MAXIMUM_STEERING_ANGLE_FOR_RADIUS_radius("MAXIMUM_STEERING_ANGLE_FOR_RADIUS_radius"),
        MAXIMUM_STEERING_ANGLE_FOR_RADIUS_steering_angle("MAXIMUM_STEERING_ANGLE_FOR_RADIUS_steering_angle"),
    //VertexToVectorXY
    VERTEX_TO_VECTOR_XYZ_vertex("VERTEX_TO_VECTOR_XYZ_vertex"),
        VERTEX_TO_VECTOR_XYZ_vector_xy("VERTEX_TO_VECTOR_XYZ_vector_xy"),
    //SubtractVectors
    SUBTRACT_VECTORS_minuend_vector("SUBTRACT_VECTORS_minuend_vector"),
    SUBTRACT_VECTORS_subtrahend_vector("SUBTRACT_VECTORS_subtrahend_vector"),
        SUBTRACT_VECTORS_subtracted_vector("SUBTRACT_VECTORS_subtracted_vector"),
    //MinimumDistanceOfVectors
    MINIMUM_DISTANCE_OF_VECTORS_vector_1_1("MINIMUM_DISTANCE_OF_VECTORS_vector_1_1"),
    MINIMUM_DISTANCE_OF_VECTORS_vector_1_2("MINIMUM_DISTANCE_OF_VECTORS_vector_1_2"),
    MINIMUM_DISTANCE_OF_VECTORS_vector_2_1("MINIMUM_DISTANCE_OF_VECTORS_vector_2_1"),
    MINIMUM_DISTANCE_OF_VECTORS_vector_2_2("MINIMUM_DISTANCE_OF_VECTORS_vector_2_2"),
        MINIMUM_DISTANCE_OF_VECTORS_minimum_distance("MINIMUM_DISTANCE_OF_VECTORS_minimum_distance");

    private final String name;

    private ConnectionEntry(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }
}
