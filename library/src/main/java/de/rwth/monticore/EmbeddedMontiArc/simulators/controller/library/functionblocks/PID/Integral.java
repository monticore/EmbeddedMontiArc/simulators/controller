/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.PID;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 06.11.2016
 * First modification on 12.11.2016
 * Sencond modification on 07.01.2017
 */
@Deprecated
public class Integral extends FunctionBlock {

    //Input Arguments
    private Double currentValue;

    //Output Arguments
    private Double eIntegral;

    //Global Arguments
    private Double previousValue;
    private Double oldVal;

    /**
     *constructs a Integral for a function starting in (t_0, val_0) with default difference d_t
     *@param val_0 the starting value of the function
     */
    public Integral(double val_0){
        this.previousValue = val_0;
        this.oldVal = 0.0;
    }


    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/
    /**
     * Main function of this function block
     */
    @Override
    public void execute(double timeDelta) {
        double r;
        double u;

        r = currentValue;
        u = calculate(r, timeDelta);
        eIntegral = u;
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/
    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        currentValue = (Double) inputs.get(ConnectionEntry.PID_current_value.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.PID_new_controlled_value.toString(), eIntegral);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.PID_current_value.toString()//,
                //ConnectionEntry.PID_delta_time.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * apply nunerical integral by using the mean of current speed und previous speed
     * @param val_i the current value of the function
     * @param deltaTime the delta time in seconds
     * @return      the Integral
     * @throws IllegalArgumentException if the current point in time is earlier or equal to the previous one
     */
    public double calculate(double val_i, double deltaTime)
    {
        double eIntegral;

        if(deltaTime <= 0)
        {
            throw new IllegalArgumentException("This time step size is not positive: " + deltaTime);
        }

        //calculate the Integration
        //use the mean value to approximate, regard the speed function as a linear function
        eIntegral = oldVal + (deltaTime)*((val_i + previousValue)/2);

        //update the previous value
        oldVal = eIntegral;
        previousValue = val_i;

        return eIntegral;

    }

    
}
