/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.TestExampleGenerators;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IControllerNode;
import org.apache.commons.math3.linear.ArrayRealVector;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;

import java.util.LinkedList;
import java.util.List;

/**
 * This class generates several types of detailed paths.
 * A detailed path is a List<Vertex> that has a Vertex at least every two meters.
 *
 * Created by Christoph on 05.03.2017.
 */
public class DetailedPathGenerator {

    /**
     * this variable contains the number of examples that are implemented in this generator.
     */
    public static final Integer NUMBER_OF_EXAMPLES = 1;

    /**
     * Getter for the i-th example path
     * default: first graph
     *
     * @param i the index of the example
     * @return the i-th example path, if NUMBER_OF_EXAMPLES < i: first example path
     */
    public List<Vertex> getPath(int i) {
        switch(i) {
            case 1: return getPath_1();
            default: return getPath_1();
        }
    }

    /**
     * Getter for the i-th example path
     * default: first graph
     *
     * @param i the index of the example
     * @return the i-th example path, if NUMBER_OF_EXAMPLES < i: first example path
     */
    public List<Vertex> getDetailedPath(int i) {
        switch(i) {
            case 1: return getDetailedPath_1();
            default: return getDetailedPath_1();
        }
    }

    /**
     * Getter for the first non-detailed example path
     * this is just a list of two vertices
     *
     * @return the first non-detailed example path
     */
    private List<Vertex> getPath_1() {
        List<Vertex> list = new LinkedList<>();
        Vertex vertex1 = new Vertex(1L, 1L, new ArrayRealVector(new Double[] {0.0, 0.0, 0.0}), 0.0);
        list.add(vertex1);
        Vertex vertex2 = new Vertex(2L, 2L, new ArrayRealVector(new Double[] {50.0, 0.0, 0.0}), 0.0);
        list.add(vertex2);

        return list;
    }

    /**
     * Getter for the corresponding detailed example path to the first example path
     * this is just a list of two vertices interpolated linearly in IControllerNode.INTERPOLATION_DISTANCE meter distance
     *
     * @return the first detailed example path
     */
    private List<Vertex> getDetailedPath_1() {
        List<Vertex> list = new LinkedList<>();
        Vertex vertex1 = new Vertex(1L, 1L, new ArrayRealVector(new Double[] {0.0, 0.0, 0.0}), 0.0);
        list.add(vertex1);
        for(double d = IControllerNode.INTERPOLATION_DISTANCE; d < 50.0; d += IControllerNode.INTERPOLATION_DISTANCE) {
            Vertex vertex = new Vertex(-1L, -1L, new ArrayRealVector(new Double[] {d, 0.0, 0.0}), 0.0);
            list.add(vertex);
        }
        Vertex vertex2 = new Vertex(2L, 2L, new ArrayRealVector(new Double[] {50.0, 0.0, 0.0}), 0.0);
        list.add(vertex2);

        return list;
    }
}
