/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks.PID;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 10.11.2016
 * Modified on 06.01.2017
 */
@Deprecated
public class Derivative extends FunctionBlock {

    //Input Arguments
    private Double currentValue;

    //Output Argument
    private Double eDerivative;

    //Global Arguments
    private Double previousValue;


    /**
     * constructs a Derivative for a function starting in (t_0, val_0) with default difference d_t
     * @param val_0 the starting value of the function
     */
    public Derivative(double k_d, double val_0) {
        this.previousValue = val_0;
    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/
    /**
     * Main function of this function block
     */
    public void execute(double timeDelta){
        eDerivative = calculate(currentValue, timeDelta);
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/
    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        currentValue = (Double) inputs.get(ConnectionEntry.PID_current_value.toString());
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.PID_new_controlled_value.toString(), eDerivative);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.PID_current_value.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * differentiates by calculating the difference quotient using the default difference between two points in time
     * @param val_i the current value of the function
     * @param deltaTime the delta time in seconds
     * @return      the derivative in microseconds
     * @throws IllegalStateException if the current default difference is not positive
     */
    public double calculate(double val_i, double deltaTime) {
        double eDerivative;

        if(deltaTime <= 0) {
            throw new IllegalStateException("This time step size is not positive: " + deltaTime);
        }

        //calculate the gradient
        eDerivative = val_i - previousValue;
        eDerivative /= (double)deltaTime;

        //update the previous values
        previousValue = val_i;

        return eDerivative;
    }
}
