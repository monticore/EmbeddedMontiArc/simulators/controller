/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks;

import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlockManagement;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * This FunctionBlock creates an iterator for List<T> whereby T is a generic type.
 *      used formulas: none
 *
 *      used sub blocks: none
 *
 * Created by Christoph Grüne on 24.01.2017.
 */
public class CreateIterator<T> extends FunctionBlock {

    //Input Variables
    List<T> list;
    T elementToBegin;

    //Output Variables
    Iterator<T> iterator;

    //Global Variables

    //FunctionBlockManagement
    FunctionBlockManagement functionBlockManagement;

    /**
     * Constructor for a InnerControlBlock object
     */
    public CreateIterator() {}

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        if(elementToBegin == null) {
            iterator = list.iterator();
        } else {
            iterator = list.listIterator(list.indexOf(elementToBegin));
        }
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        list = (List<T>) inputs.get(ConnectionEntry.CREATE_ITERATOR_list.toString());
        try {
            elementToBegin = (T) inputs.get(ConnectionEntry.CREATE_ITERATOR_element_to_begin.toString());
        } catch (Exception e) {}
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.CREATE_ITERATOR_iterator.toString(), iterator);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {ConnectionEntry.CREATE_ITERATOR_list.toString()};
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
}
