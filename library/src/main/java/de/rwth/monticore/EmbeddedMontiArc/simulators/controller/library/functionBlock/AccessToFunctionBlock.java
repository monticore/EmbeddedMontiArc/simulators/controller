/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is used to link the different components of the controller which implement FunctionBlock.
 * Its purpose is to collect all the relevant data needed for the components calculation, then put these into a hashmap
 * which can then be given to the according FunctionBlock.
 * Consequently, this class has to be used for every component of type FunctionBlock.
 * The components of type FunctionBlock, whose data is collected, shall be passed as parameters to the constructor.
 * Created by s.erlbeck on 27.12.2016.
 */
public class AccessToFunctionBlock {

    private Map<String, Object> inputs;

    public AccessToFunctionBlock() {
        inputs = new LinkedHashMap<String, Object>();
    }

    /**
     * collecting all the data from the components of type FunctionBlock
     */
    public void collectAllInputs(List<Connection> connections) {
        for(Connection connection : connections) {
            Object input;
            if(connection.getSuperBlock() == connection.getFunctionBlockA()) {
                if(connection.getSuperBlock().getConnectionMap().get(connection.getPortA()) == null && connection.hasInitialValue()) {
                    input = connection.getInitialValue();
                } else {
                    input = connection.getSuperBlock().getConnectionMap().get(connection.getPortA());
                }
                inputs.put(connection.getPortB(), input);
            } else {
                if(connection.getFunctionBlockA().getOutputs().get(connection.getPortA()) == null && connection.hasInitialValue()) {
                    input = connection.getInitialValue();
                } else {
                    input = connection.getFunctionBlockA().getOutputs().get(connection.getPortA());
                }
                inputs.put(connection.getPortB(), input);
            }
        }
    }

    /**
     * getter for the Hashmap which can be passed directly to the according FunctionBlock
     * @return the Hashmap with the relevant Objects
     */
    public Map<String, Object> getMap() {
        return inputs;
    }
}
