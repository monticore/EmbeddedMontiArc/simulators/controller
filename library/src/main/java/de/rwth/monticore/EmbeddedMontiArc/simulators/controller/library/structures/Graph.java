/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.structures;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Vertex;
import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.map.IAdjacency;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This function implements the data structure of a graph without multi-edges.
 * It stores a list of the vertices and an adjacency list implemented with a Map<Vertex, Map<Vertex, Edge>>,
 * so one can implement a general edge data structure with more than one variable e.g. weight, capacity and velocity.
 *
 * Created by Christoph Grüne on 22.12.2016.
 */
public class Graph {

    private List<Vertex> vertices;
    private Map<Vertex, Map<Vertex, Edge>> edges;

    private Integer numberOfVertices = 0;
    private boolean directed;

    /**
     * constructor for a graph object
     *
     * @param vertices is the list of all vertices contained in that graph
     * @param edges is the adjacencyList
     */
    public Graph(List<Vertex> vertices, Map<Vertex, Map<Vertex, Edge>> edges, boolean directed) {
        this.numberOfVertices = vertices.size();
        this.directed = directed;

        this.vertices = vertices;
        this.edges = edges;
    }

    /**
     * constructor for a graph object
     *
     * @param adjacencies the list of adjacencies provided with the interface IAdjacency
     */
    public Graph(List<IAdjacency> adjacencies, boolean directed) {
        this.numberOfVertices = 0;
        this.directed = directed;
        this.edges = new LinkedHashMap<>();
        this.vertices = new LinkedList<>();

        for(IAdjacency iAdjacency : adjacencies) {

            Long id1 = iAdjacency.getNode1().getId();
            Long osmId1 = iAdjacency.getNode1().getOsmId();

            RealVector position1 = new ArrayRealVector(new Double[] {iAdjacency.getNode1().getPoint().getX(),
                    iAdjacency.getNode1().getPoint().getY(),
                    iAdjacency.getNode1().getPoint().getZ()});

            Long id2 = iAdjacency.getNode2().getId();
            Long osmId2 = iAdjacency.getNode2().getOsmId();

            RealVector position2 = new ArrayRealVector(new Double[] {iAdjacency.getNode2().getPoint().getX(),
                    iAdjacency.getNode2().getPoint().getY(),
                    iAdjacency.getNode2().getPoint().getZ()});

            Vertex vertex1 = new Vertex(id1, osmId1, position1, 0.0);
            Vertex vertex2 = new Vertex(id2, osmId2, position2, 0.0);

            if(!hasVertex(vertex1)) {
                addVertex(vertex1);
            } else {
                vertex1 = getVertex(vertex1);
            }
            if(!hasVertex(vertex2)) {
                addVertex(vertex2);
            } else {
                vertex2 = getVertex(vertex2);
            }

            Edge edge = new Edge(iAdjacency.getDistance(), 0.0);
            addEdge(vertex1, vertex2, edge);
        }
    }

    /**
     * Getter for vertices
     *
     * @return all vertices
     */
    public List<Vertex> getVertices() {
        return vertices;
    }

    /**
     * Getter for edges
     *
     * @return
     */
    public Map<Vertex, Map<Vertex, Edge>> getEdges() {
        return edges;
    }

    /**
     * Getter for numberOfVertices
     *
     * @return the number of vertices
     */
    public Integer getNumberOfVertices() {
        return numberOfVertices;
    }

    /**
     * adds a vertex to that graph
     *
     * @param vertex the graph that shall be added
     */
    public void addVertex(Vertex vertex) {
        vertices.add(vertex);
        edges.put(vertex, new LinkedHashMap<>());
        numberOfVertices++;
    }

    /**
     * removes a vertex
     *
     * @param vertex the Vertex to be removed
     */
    public void removeVertex(Vertex vertex) {
        vertices.remove(vertex);
        edges.remove(vertex);
        numberOfVertices--;
    }

    /**
     * Getter for vertex
     *
     * @param id the ID of the vertex
     * @return the vertex
     */
    public Vertex getVertex(Long id) {
        for(Vertex vertex : vertices) {
           if(vertex.getId().equals(id)) {
               return vertex;
           }
        }
        throw new IllegalArgumentException("There is not such a vertex in this graph.");
    }

    /**
     * returns the first equal vertex in the graph
     *
     * @param vertex the vertex to check
     * @return the first equal vertex in the graph
     */
    public Vertex getVertex(Vertex vertex) {
        for(Vertex v : vertices) {
            if(v.equals(vertex)) {
                return v;
            }
        }
        throw new IllegalArgumentException("There is not such a vertex in this graph.");
    }

    /**
     * returns if the graph has the vertex
     *
     * @param id the id of the vertex to check
     * @return if the vertex is in the graph
     */
    public boolean hasVertex(Integer id) {
        for(Vertex vertex : vertices) {
            if(vertex.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    /**
     * returns if the graph has an equal vertex
     *
     * @param vertex the vertex to check
     * @return if an equal vertex is in the graph
     */
    public boolean hasVertex(Vertex vertex) {
        for(Vertex v : vertices) {
            if(v.equals(vertex)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds an edge to the graph if vertex1 and vertex2 are in the graph
     *
     * @param vertex1 the first vertex (where the edges comes form)
     * @param vertex2 the second vertex (where the edges goes to)
     * @param edge the edge itself
     */
    public void addEdge(Vertex vertex1, Vertex vertex2, Edge edge) {

        if(!hasVertex(vertex1)) {
            throw new IllegalArgumentException("This graph does not contain vertex1");
        }
        if(!hasVertex(vertex2)) {
            throw new IllegalArgumentException("This graph does not contain vertex2");
        }

        if(!hasEdge(vertex1, vertex2, edge)) {
            edges.get(vertex1).put(vertex2, edge);
            if (!directed) {
                edges.get(vertex2).put(vertex1, edge);
            }
        }
    }

    public void removeEdge(Vertex vertex1, Vertex vertex2) {
        edges.get(vertex1).remove(vertex2);
    }

    /**
     * Getter for edge
     *
     * @param vertex1 the outgoing vertex of the edge
     * @param vertex2 the ingoing vertex of the edge
     * @return the edge
     */
    public Edge getEdge(Vertex vertex1, Vertex vertex2) {
        return edges.get(vertex1).get(vertex2);
    }

    /**
     * returns if the graph has an edge between vertex1 and vertex2
     *
     * @param vertex1 the outgoing vertex of the edge
     * @param vertex2 the ingoing vertex of the edge
     * @return if the edge is in the graph
     */
    public boolean hasEdge(Vertex vertex1, Vertex vertex2) {
        if(edges.containsKey(vertex1)) {
            if(edges.get(vertex1).containsKey(vertex2)) {
                return edges.get(vertex1).get(vertex2) != null;
            }
        }
        return false;
    }

    /**
     * returns if the graph has the edge where the edge is equal to the passed argument "edge"
     *
     * @param vertex1 the outgoing vertex of the edge
     * @param vertex2 the ingoing vertex of the edge
     * @param edge the edge to be tested
     * @return if the edge is in the graph
     */
    public boolean hasEdge(Vertex vertex1, Vertex vertex2, Edge edge) {
        if(edges.containsKey(vertex1)) {
            if(edges.get(vertex1).containsKey(vertex2)) {
                return edge.equals(edges.get(vertex1).get(vertex2));
            }
        }
        return false;
    }
}
