/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionblocks;

import de.rwth.monticore.EmbeddedMontiArc.simulators.commons.controller.commons.Surface;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.ConnectionEntry;
import de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.functionBlock.FunctionBlock;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This FunctionBlock calculates the maximal steering angle the car should drive when certain weather and surface conditions are fulfilled.
 *
 *      used formulas: a * exp(b * lambda), where a is the "A" coefficient of the surface (the maximal velocity of the car on that ground)
 *                                          and b ist the "B" coefficient of the surface
 *                                          and lambda the steering angle
 *
 *      used sub blocks: none
 *
 * Created by Christoph Grüne on 14.01.2017.
 */
public class MaximumVelocityBySteeringAnAngle extends FunctionBlock {

    //Input Variables
    private Double steeringAngle;
    private Surface currentSurface;
    private Double currentWeather;

    //Output Variables
    private Double velocity;

    //Global Variables

    //Used Function Blocks

    /**
     * Constructor for a InnerControlBlock object
     */
    public MaximumVelocityBySteeringAnAngle() {

    }

    /***************************************************************************
     *  Main function for this function block                                  *
     ***************************************************************************/

    /**
     * Main function of this function block
     */
    public void execute(double timeDelta) {
        velocity = calculateMaximumVelocity(steeringAngle, currentSurface, currentWeather);
    }

    /***************************************************************************
     *  Getter and Setter to emulate the Input and Output of a function block  *
     ***************************************************************************/

    /**
     * set connectionMap from extern
     *
     * @param inputs map that contains all connectionMap with in getImportNames specified keys
     */
    public void setInputs(Map<String, Object> inputs) {
        steeringAngle = (Double) inputs.get(ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_steering_angle.toString());
        currentSurface = (Surface) inputs.get(ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_surface.toString());
        currentWeather = (Double) inputs.get(ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_weather.toString());
        super.setInputs(inputs);
    }

    /**
     * output method
     *
     * @return all outputs in a map
     */
    public Map<String, Object> getOutputs() {
        Map<String, Object> outputs = new LinkedHashMap<String, Object>();
        outputs.put(ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_velocity.toString(), velocity);
        return outputs;
    }

    /**
     * returns all import names for maps
     *
     * @return all import names
     */
    public String[] getImportNames() {
        String[] names = new String[] {
                ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_steering_angle.toString(),
                ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_surface.toString(),
                ConnectionEntry.MAXIMUM_VELOCITY_BY_STEERING_AN_ANGLE_current_weather.toString()
        };
        return names;
    }

    /***************************************************************************
     *  Helper functions for this function block                               *
     ***************************************************************************/
    /**
     * Function to compute the maximal velocity by steering at a certain angle
     * f(x) = a * exp(b * x)
     *
     * @param lambda desired steering angle
     * @param surface i is the ground on that the cars is driving
     */
    private Double calculateMaximumVelocity(Double lambda, Surface surface, Double rainCoefficient) {
        return surface.getParameterA() * Math.exp(surface.getParameterB(rainCoefficient) * Math.abs(lambda)) + surface.getParameterC();
    }
}
