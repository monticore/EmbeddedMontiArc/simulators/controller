/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.controller.library.exceptions;

/**
 * This exception is used to denote that an entry of the input map of a FunctionBlock is null.
 * Therefor, it is thrown if an entry is null and is not marked that it can be null.
 *
 * Created by Christoph Grüne on 19.03.2017.
 */
public class EntryIsNullException extends RuntimeException {
    public EntryIsNullException() {
        super();
    }
    public EntryIsNullException(String s) {
        super(s);
    }
    public EntryIsNullException(String s, Throwable throwable) {
        super(s, throwable);
    }
    public EntryIsNullException(Throwable throwable) {
        super(throwable);
    }
}
